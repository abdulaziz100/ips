<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRecurringInvoiceLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('recurring_invoice_logs', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('recurring_invoice_id');
            $table->unsignedInteger('generated_invoice_id');
            $table->date('generated_date');
            $table->string('generated_by', 20)->default('command');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('recurring_invoice_logs');
    }
}
