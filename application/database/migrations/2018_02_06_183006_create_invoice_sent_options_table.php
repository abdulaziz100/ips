<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoiceSentOptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoice_sent_options', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('invoice_id');
            $table->unsignedInteger('owner_id');
            $table->unsignedInteger('customer_profile_id');
            $table->string('sent_media', 50);
            $table->string('from')->nullable();
            $table->string('to')->nullable();
            $table->text('ccs')->nullable();
            $table->text('message')->nullable();
            $table->boolean('is_attached')->default(false);
            $table->boolean('copy_myself')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoice_sent_options');
    }
}
