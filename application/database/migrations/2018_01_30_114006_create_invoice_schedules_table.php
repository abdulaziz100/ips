<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoiceSchedulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoice_schedules', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('invoice_id');
            $table->time('trigger_time');
            $table->string('frequency', 20);
            $table->unsignedInteger('day_of_week')->nullable();
            $table->unsignedInteger('day_of_month')->nullable();
            $table->unsignedInteger('month')->nullable();
            $table->date('start');
            $table->date('until')->nullable();
            $table->string('timezone');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoice_schedules');
    }
}
