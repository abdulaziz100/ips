<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoices', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('customer_profile_id');
            $table->unsignedInteger('owner_id');
            $table->unsignedInteger('subscription_id')->default(0);
            $table->string('title')->default('Invoice');
            $table->string('invoice_summary', 500)->nullable();
            $table->string('identifier', 50)->unique();
            $table->string('po_number', 50)->nullable();
            $table->double('sub_total', 15, 2)->default(0);
            $table->double('vat_total', 15, 2)->default(0);
            $table->double('total', 15, 2)->default(0);
            $table->date('invoice_date');
            $table->date('payment_due');
            $table->text('notes')->nullable();
            $table->text('invoice_footer')->nullable();
            $table->string('status')->default('draft');
            $table->softDeletes();
            $table->integer('created_by')->unsigned()->default(0);
            $table->integer('updated_by')->unsigned()->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoices');
    }
}
