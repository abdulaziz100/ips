<div class="form-group">
	<label for="title" class="control-label"> Title {!! validation_error($errors->first('title'),'title') !!} </label>
	{!! Form::text('title',old('title'),['class'=>'form-control','placeholder' =>'Title', 'id' => 'title']) !!}
</div>
<div class="form-group">
	{!! Form::label('description','Description') !!} 
	{!! Form::textarea('description',old('description'),['class'=>'form-control','placeholder' =>'Description', 'size' => '30x2']) !!}
</div>
<div class="form-group">
	<label for="category_id" class="control-label"> Category {!! validation_error($errors->first('category_id'),'category_id') !!} </label>
	{!! Form::select('category_id',$categories, null,['class'=>'form-control chosen-select','id' => 'category_id']) !!}
</div>
<div class="form-group">
	{!! Form::label('regular_price','Regular Price') !!} 
	{!! Form::number('regular_price',old('regular_price'),['class'=>'form-control','placeholder' =>'Regular Price']) !!}
</div>
<div class="form-group">
	{!! Form::label('promotional_price','Promotional Price') !!} 
	{!! Form::number('promotional_price',old('promotional_price'),['class'=>'form-control','placeholder' =>'Promotional Price']) !!}
</div>
