@extends('layouts.master')

@section('title') List of Services @endsection

@section('content')
<div class="row">
	<div class="col-sm-12">
		<div class="card m-b-30">
			<div class="card-header">
				<h4 class="card-title">
					List of Services
					<a href="{{ url('services/create')}}" class="btn btn-danger btn-xs pull-right"><i class="fa fa-plus-circle"></i> Add New</a>
				</h4>
			</div>
			
			<div class="card-body">
				<div class="table-responsive">
					<table class="table table-bordered">
						<thead>
							<th>Title</th>
							<th>Category</th>
							<th width="12%">Regular Price</th>
							<th width="15%">Promotional Price</th>
							<th width="12%">Created at</th>
							<th width="9%">Action</th>
						</thead>
						<tbody>
						@forelse($services as $service)
							<tr>
								<td>{{ $service->title }}</td>
								<td>{{ $service->category->title }}</td>
								<td>{{ $service->regular_price }}</td>
								<td>{{ $service->promotional_price }}</td>
								<td>{{ $service->created_at->format('d M, Y')}}</td>
								<td>
					                {{-- Edit --}}
	                                <a href="{{url('services/'.$service->id.'/edit')}}" class="btn btn-default btn-xs" title="Edit this information"><i class="fa fa-pencil"></i></a>
	                                {{-- Delete --}}
									<a href="#" data-id="{{$service->id}}" data-action="{{ url('services/delete') }}" data-message="Are you sure, You want to delete this service?" class="btn btn-danger btn-xs alert-dialog" title="Delete service"><i class="fa fa-trash white"></i></a>
								</td>
							</tr>
						@empty
							<tr>
								<td colspan="6" align="center">No Records Found</td>
							</tr>
						@endforelse
						</tbody>
					</table>
				</div>
			</div><!-- /.card-body -->

			@if($services->total() > 10)
			<div class="card-footer">
				<div class="row">
					<div class="col-md-4">
						{{ $services->paginationSummery }}
					</div>
					<div class="col-md-8 text-right">
						<div class="float-right">
							{!! $services->links() !!}
						</div>
					</div>
				</div>
			</div>
			@endif
		</div>
	</div>
</div>
	
@endsection

@section('custom-style')

@endsection

@section('custom-script')

@endsection