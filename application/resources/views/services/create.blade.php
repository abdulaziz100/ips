@extends('layouts.master')

@section('title') Create Service @endsection

@section('content')
<div class="row">
	<div class="col-sm-12">
		<div class="card m-b-30"> 
			<div class="card-header">
				<h4 class="card-title">Create Service</h4> 
			</div>

			{!! Form::open(array('url' => 'services', 'role' => 'form')) !!}
			<div class="card-body">
				<div class="row">
					<div class="col-sm-6">
						@include('services.form')
					</div>
				</div>
			</div><!-- /.card-body -->
			<div class="card-footer">
				<button class="btn btn-info">
					<i class="fa fa-save"></i> Save
				</button>
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
	
@endsection