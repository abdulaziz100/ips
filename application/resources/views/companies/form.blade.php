<div class="row">
	<div class="col-sm-6">
		<fieldset>
			<legend>Basic Info</legend>
		
			<div class="form-group">
				<label for="name" class="control-label"> Name {!! validation_error($errors->first('name'),'name') !!} </label>
				{!! Form::text('name',old('name'),['class'=>'form-control','placeholder' =>'Name', 'id' => 'name']) !!}
			</div>
			<div class="form-group">
				{!! Form::label('type_of_business','Type of Business') !!} 
				{!! Form::select('type_of_business',config('constants.type_of_business'), old('type_of_business'),['class'=>'form-control chosen-select']) !!}
			</div>
			<div class="form-group">
				<label for="country" class="control-label"> Country {!! validation_error($errors->first('country'),'country') !!} </label>
				{!! Form::select('country',$countries,old('country') ? old('country'):(!empty($company->country) ? $company->country : 'BD'),['class'=>'form-control chosen-select','placeholder'=>'Select a Country', 'id' => 'country']) !!}
			</div>
			<div class="form-group">
				{!! Form::label('currency','Currency') !!} 
				{!! Form::select('currency',config('currencies'), old('currency') ? old('currency') : (!empty($company->currency) ? $company->currency : 'BDT'),['class'=>'form-control chosen-select']) !!}
			</div>
			<div class="form-group">
				{!! Form::label('type_of_organization','Type of Organization') !!} 
				{!! Form::select('type_of_organization',config('constants.type_of_organization'), old('type_of_organization'),['class'=>'form-control chosen-select']) !!}
			</div>
		</fieldset>
	</div>
	<div class="col-sm-6">
		<fieldset>
			<legend>Other Info</legend>
		
			<div class="form-group">
				{!! Form::label('address','Address') !!} 
				{!! Form::textarea('address',old('address'),['class'=>'form-control','placeholder' =>'Address', 'size' => '30x2']) !!}
			</div>
			<div class="form-group">
				{!! Form::label('city','City') !!} 
				{!! Form::text('city',old('city'),['class'=>'form-control','placeholder' =>'City']) !!}
			</div>
			<div class="form-group">
				{!! Form::label('state','State/Province') !!} 
				{!! Form::text('state',old('state'),['class'=>'form-control','placeholder' =>'State/Province']) !!}
			</div>
			<div class="form-group">
				{!! Form::label('zip_code','Postal/Zip Code') !!} 
				{!! Form::text('zip_code',old('zip_code'),['class'=>'form-control','placeholder' =>'Postal/Zip Code']) !!}
			</div>
			<div class="form-group">
			    {!! Form::label('timezone','Timezone') !!}
			    {!! Form::select('timezone', getTimezones(), old('timezone') ? old('timezone') : (!empty($company->timezone) ? $company->timezone : 'Asia/Dhaka'),['class'=>'form-control chosen-select']) !!}
			</div>
			<div class="form-group">
				{!! Form::label('primary_phone','Primary Phone') !!} 
				{!! Form::text('primary_phone',old('primary_phone'),['class'=>'form-control','placeholder' =>'Primary Phone']) !!}
			</div>
			<div class="form-group">
				{!! Form::label('alt_phone','Alternate Phone') !!} 
				{!! Form::text('alt_phone',old('alt_phone'),['class'=>'form-control','placeholder' =>'Alternate Phone']) !!}
			</div>
			<div class="form-group">
				{!! Form::label('website','Website') !!} 
				{!! Form::text('website',old('website'),['class'=>'form-control','placeholder' =>'Website']) !!}
			</div>
		</fieldset>
	</div>
</div>



