@extends('layouts.master')

@section('title') {{ $company->name }} @endsection

@section('content')
<div class="row">
	<div class="col-sm-12">
		<div class="card m-b-30">
			<div class="card-header">
				<h4 class="card-title">{{ $company->name }}</h4> 
			</div>
			
			<div class="card-body">
				<div class="row">
					<div class="col-sm-6 details-info">
						<strong>Type of Business: </strong>{{ config('constants.type_of_business.'.$company->type_of_business) }}
						<br>
						<strong>Country: </strong> {{ $company->country_obj->name }}
						<br>
						<strong>Business Currency: </strong>{{ config('currencies.'.$company->currency) }}
						<br>
						<strong>Type of Organization</strong>{{ config('constants.type_of_organization.'.$company->type_of_organization) }}
					</div>
					<div class="col-sm-6 details-info">
						<strong>City: </strong>{{ $company->city }}
						<br>
						<strong>State/Province:</strong> {{ $company->state }}
						<br>
						<strong>Timezone: </strong>{{ $company->timezone }}
						<br>
						<strong>Primary Phone: </strong>{{ $company->primary_phone }}
						<br>
						<strong>Alternate Phone: </strong>{{ $company->alt_phone }}
						<br>
						<strong>Website: </strong>{{ $company->website }}
						<br>
					</div>
				</div>
			</div><!-- /.card-body -->

			<div class="card-footer">
				<a href="{{ url('companies/'.$company->id.'/edit') }}" class="btn btn-primary"><i class="fa fa-edit"></i> Edit</a>
				<a href="{{ url('companies') }}" class="btn btn-success"><i class="fa fa-backward"></i> Go Back</a>
			</div>
		</div>
	</div>
</div>
	
@endsection

@section('custom-style')

@endsection

@section('custom-script')

@endsection