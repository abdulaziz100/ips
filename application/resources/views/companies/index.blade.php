@extends('layouts.master')

@section('title') List of Companies @endsection

@section('content')
<div class="row">
	<div class="col-sm-12">
		<div class="card m-b-30">
			<div class="card-header">
				<h4 class="card-title">List of Companies 
					<a href="{{ url('companies/create')}}" class="btn btn-danger btn-xs pull-right"><i class="fa fa-plus-circle"></i> Add New</a>
				</h4>
			</div>
			
			<div class="card-body">
				<div class="table-responsive">
					<table class="table table-responsive table-bordered training-table">
						<thead>
							<th>Name</th>
							<th>Type of Business</th>
							<th width="12%">Currency</th>
							<th width="17%">Type of Organization</th>
							<th width="10%">Country</th>
							<th width="12%">Created at</th>
							<th width="12%">Action</th>
						</thead>
						<tbody>
						@forelse($companies as $company)
							<tr>
								<td>{{ $company->name }}</td>
								<td>{{ config('constants.type_of_business.'.$company->type_of_business) }}</td>
								<td>{{ config('currencies.'.$company->currency) }}</td>
								<td>{{ config('constants.type_of_organization.'.$company->type_of_organization) }}</td>
								<td>{{ $company->country_obj->name }}</td>
								<td>{{ $company->created_at->format('d M, Y')}}</td>
								<td>
									{{-- View --}}
									<a class="btn btn-xs btn-success" href="{{ url('companies/' . $company->id) }}" title="View Company"><i class="fa fa-eye"></i></a>
					                {{-- Edit --}}
	                                <a href="{{url('companies/'.$company->id.'/edit')}}" class="btn btn-default btn-xs" title="Edit this information"><i class="fa fa-pencil"></i></a>
	                                {{-- Delete --}}
									<a href="#" data-id="{{$company->id}}" data-action="{{ url('companies/delete') }}" data-message="Are you sure, You want to delete this company?" class="btn btn-danger btn-xs alert-dialog" title="Delete company"><i class="fa fa-trash white"></i></a>
								</td>
							</tr>
						@empty
							<tr>
								<td colspan="7" align="center">No Records Found</td>
							</tr>
						@endforelse
						</tbody>
					</table>
				</div>
			</div><!-- /.card-body -->

			@if($companies->total() > 10)
			<div class="card-footer">
				<div class="row">
					<div class="col-md-4">
						{{ $companies->paginationSummery }}
					</div>
					<div class="col-md-8 text-right">
						<div class="float-right">
							{!! $companies->links() !!}
						</div>
					</div>
				</div>
			</div>
			@endif
		</div>
	</div>
</div>
	
@endsection

@section('custom-style')

@endsection

@section('custom-script')

@endsection