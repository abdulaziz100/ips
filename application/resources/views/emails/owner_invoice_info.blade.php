@extends('emails.master')

@section('title')  List of Invoices of Customers @endsection

@section('content')
<div style="border:1px solid #dfdfdf;margin:0 auto 20px;max-width:570px;text-align:center;" class="card">
    <div class="card-body">

        <div style="margin-top:20px;padding:20px;text-align:center">
            
            <table style="width:100%;border: 1px solid black;border-collapse: collapse;">
                <thead>
                    <tr>
                        <th width="7%" style="border: 1px solid black;border-collapse: collapse;padding:10px;text-align: left;background-color: black;color: white;">ID</th>
                        <th style="border: 1px solid black;border-collapse: collapse;padding:10px;text-align: left;background-color: black;color: white;">Customer Name</th>
                        <th width="20%" style="border: 1px solid black;border-collapse: collapse;padding:10px;text-align: left;background-color: black;color: white;text-align:right">Amount</th>
                        <th width="8%" style="border: 1px solid black;border-collapse: collapse;padding:10px;text-align: left;background-color: black;color: white;">Action</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($invoices as $invoice)
                    <tr style="background-color: {{ $loop->index % 2 == 0 ? '#eee;':'#fff;' }}">
                        <td style="border: 1px solid black;border-collapse: collapse;padding: 5px;text-align: left;"><a style="text-decoration: none;" href="{{ url('invoice-details/'.$invoice->identifier) }}" target="_blank">{{ strtoupper($invoice->identifier) }}</a></td>
                        <td style="border: 1px solid black;border-collapse: collapse;padding: 5px;text-align: left;">{{ $invoice->customer_profile->name }}</td>
                        <td style="border: 1px solid black;border-collapse: collapse;padding: 5px;text-align: right;">৳{{ number_format(round($invoice->total,2),2) }}</td>
                        <td style="border: 1px solid black;border-collapse: collapse;padding: 5px;text-align: left;"><a href="{{ url('invoice-details/'.$invoice->identifier) }}" target="_blank" style="color: #fff;background-color: #5cb85c;border-color: #4cae4c;display: inline-block;padding: 6px 12px;margin-bottom: 0;font-size: 14px;font-weight: 400;line-height: 1.42857143;text-align: center;white-space: nowrap;vertical-align: middle;border-radius: 4px;text-decoration: none;">View</a></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
