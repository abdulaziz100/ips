@extends('emails.master')

@section('title') Reminder for Invoice # {{ strtoupper($invoice->identifier) }} @endsection

@section('content')

    @include('preview.invoice_reminder_content', ['is_email_view' => true])
    
@endsection