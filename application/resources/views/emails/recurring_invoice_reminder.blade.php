@extends('emails.master')

@section('title') Reminder for Recurring Invoice # {{ strtoupper($invoice->identifier) }} @endsection

@section('content')
<div style="border:1px solid #dfdfdf;margin:0 auto 20px;max-width:570px;text-align:center;" class="card">
    <div style="font-size:18px;padding:10px 0 30px;text-align:center" class="card-body">
        <div style="margin-top:20px" class="invoice-logo-holder text-center">
            <img src="{{ isset($message) ? $message->embed( getCompanyLogoPath($company->id)) : url(getCompanyLogoPath($company->id)) }}" alt="Invoice Logo" width="180">
        </div>

        <div style="margin-top:20px;padding:20px">
        	<h1>Reminder for 
                <strong>Recurring Invoice #{{ strtoupper($invoice->identifier) }}</strong>
            </h1>
            <span style="color:#bcbcbc;size:14px">due on {{ \Carbon::parse($invoice->payment_due)->format('F d, Y') }}</span>
        </div>

        <hr style="background-color:#bcbcbc;margin:20px 0 0;">

        <div style="color:#bcbcbc;line-height:27px;padding:20px;text-align:center">
            <p>{{ $owner->name }} <br> <br>
            Just a friendly reminder, You will receive an invoice in the next week and this is due on {{ \Carbon::parse($invoice->payment_due)->format('F d, Y') }}. <br>
            The invoice was issued on {{ \Carbon::parse($invoice->invoice_date)->format('F d, Y') }}. <br>
            </p>
        </div>

        <div style="border-top:1px solid #dfdfdf;margin:20px;text-align:center">
            <h3 style="font-weight:normal;line-height:30px;">
                Amount Due: <strong>৳{{ number_format(round($invoice->total,2),2) }} BDT</strong> <br>
                Due: <strong>{{ \Carbon::parse($invoice->payment_due)->format('F d, Y') }}</strong> 
            </h3>
        </div>
    </div>
</div>
@endsection