@extends('emails.master')

@section('title') Invoice #{{ $invoice->identifier }} from {{ $company->name }} @endsection

@section('content')
<div style="border:1px solid #dfdfdf;margin:0 auto 20px;max-width:570px;text-align:center;" class="card">
    <div style="padding:10px 0 30px;" class="card-body">
        <div style="margin-top:20px" class="invoice-logo-holder text-center">
            <img src="{{ isset($message) ? $message->embed( getCompanyLogoPath($company->id)) : url(getCompanyLogoPath($company->id)) }}" alt="Invoice Logo" width="180">
        </div>

        <div style="background-color:#dfdfdf;margin-top:20px;padding:20px">
        	<h3 style="margin:0;"><strong>{{ $company->name }}</strong> has sent you an invoice for</h3>
			<h2 style="margin-top:20px">৳{{ number_format($invoice->total, 2) }}</h2>
            <span>Due on {{ \Carbon::parse($invoice->payment_due)->format('F d,Y') }}</span>
        </div>

        <div style="margin-top:20px" class="view-online">
            <a style="background-color: #337ab7; color:#ffffff;display:inline-block;font-size:16px;padding:10px 30px;text-decoration:none;" href="{{ url('invoice-details/'.$invoice->identifier) }}" class="btn btn-primary">View Online</a>
            <hr>
        </div>
        
        @if(!empty($email_message))
            <div style="font-size:16px;padding:20px 10px;text-align:left;">{!! nl2br(htmlspecialchars($email_message)) !!}</div>
            <hr>
        @endif

        @if(!empty($is_recurring_invoice))
            <table cellpadding="10">
                <tr>
                    <td align="center" width="15%"><img src="{{ $message->embed(url('assets/images/recurring_invoice_icon.png')) }}" alt="Recurring invoice icon" width="50"></td>
                    <td style="text-align:justify">
                        You’re busy. Set up automatic payments so you don’t have to remember to pay every time. When you pay online, save your credit card and you’ll get an email receipt for future payments. 
                    </td>
                </tr>
            </table>
            <hr>
        @endif

        <div style="margin-top:20px;font-size:16px;" class="invoice-contact-email margin-top-20">
            For questions about this invoice, please contact <br>
            <a href="mailto:{{ $owner->email }}">{{ $owner->email }}</a>
        </div>

        <div style="margin-top:20px;font-size:16px;" class="invoice-contact-location margin-top-20">
            <strong>{{ $company->name }}</strong> <br>
            @if($company->address)
                <span>{{ $company->address }} <br></span>
            @endif
            @if($company->state || $company->city || $company->zip_code)
                <span>{{ ($company->state ? $company->state .', ' : '') . ($company->city ? $company->city .' ' : '') .($company->zip_code ? $company->zip_code : '') }}</span> <br>
            @endif
            {{ $company->country_obj->name }}
        </div>

        <div style="margin-top:20px;font-size:16px;" class="invoice-contact-phone margin-top-20">
            <span>Phone: </span>{{ $company->primary_phone }}
            <br>
            <span>Alt Phone: </span>{{ $company->alt_phone }}
            <br>
            {{ $company->website }}
        </div>
    </div>
</div>
@endsection