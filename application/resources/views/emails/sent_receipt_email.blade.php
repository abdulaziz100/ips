@extends('emails.master')

@section('title')  Payment Receipt for Invoice#{{ $invoice->identifier }} @endsection

@section('content')
<div style="border:1px solid #dfdfdf;margin:0 auto 20px;max-width:570px;text-align:center;" class="card">
    <div class="card-body">
        <div style="margin-top:20px" class="invoice-logo-holder text-center">
            <img src="{{ isset($message) ? $message->embed( getCompanyLogoPath($company->id)) : url(getCompanyLogoPath($company->id)) }}" alt="Invoice Logo" width="180">
        </div>

        <div style="margin-top:20px;text-align:center">
        	<h2 style="margin:0;font-size:32px;">Payment Receipt</h2>
            <h5 style="font-size:15px;">Invoice #{{ $invoice->identifier }}</h5>
			<span>for {{ $invoice->customer_profile->name }}</span>

			<div style="margin-top:20px;" class="invoice-contact-location margin-top-20">
                <strong>{{ $company->name }}</strong> <br>
                @if($company->address)
                    <span>{{ $company->address }} <br></span>
                @endif
                @if($company->state || $company->city || $company->zip_code)
                    <span>{{ ($company->state ? $company->state .', ' : '') . ($company->city ? $company->city .' ' : '') .($company->zip_code ? $company->zip_code : '') }}</span> <br>
                @endif
                {{ $company->country_obj->name }}
            </div>

            <div style="margin-top:20px" class="invoice-contact-phone margin-top-20">
                <span>Phone: </span>{{ $company->primary_phone }}
                @if($company->alt_phone)
                    <br>
                    <span>Alt. Phone: </span>{{ $company->alt_phone }}
                @endif
                @if($company->website)
                    <br>
                    <a href="{{ str_contains($company->website, 'http://') ? $company->website : 'http://'.$company->website }}">{{ $company->website }}</a>
                @endif
            </div>
        </div>

        <hr>
        
        @if(!empty($email_message))
            <div style="margin-top:20px; padding:0 10px;text-align:justify;">{!! nl2br(htmlspecialchars($email_message)) !!}</div>
            <hr>
        @endif

        <div style="padding:0 10px 10px">
            <h4>Payment Amount: ৳{{ number_format($payment->amount,2) }} BDT</h4>
            <hr>
        </div>

        <div style="margin-top:20px;font-size: 14px;">
            <strong>PAYMENT METHOD:</strong> {{ strtoupper(config('constants.payment_mood.'.$payment->payment_mood)) }}
        </div>

        <div style="margin-top:20px">
            <a href="{{ url('invoice-details/'.$invoice->identifier) }}" style="background-color:#1967be;border-color:#1862b5;color:#fff;padding:5px 10px;text-decoration:none;">View Online</a> <br>
            or <a style="display:inline-block;margin-top:10px;" href="{{ url('payment-receipt/'.encrypt($payment->id)) }}">View receipt on web</a>
        </div>

        <div style="background-color:#dfdfdf;margin-top:20px;padding:15px">Thanks for your business. If this invoice was sent in error, please contact {{ $owner->email }}</div>
        
    </div>
</div>
@endsection