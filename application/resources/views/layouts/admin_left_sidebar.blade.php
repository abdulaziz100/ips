<!-- ========== Left Sidebar Start ========== -->
<div class="left side-menu">
    <button type="button" class="button-menu-mobile button-menu-mobile-topbar open-left waves-effect">
        <i class="ion-close"></i>
    </button>

    <!-- LOGO -->
    <div class="topbar-left">
        <div class="text-center">
            <a href="{{ url('/home') }}" class="logo">{{ $app_name }}</a>
            <!-- <a href="index.html" class="logo"><img src="{{ $assets }}/images/logo.png" height="24" alt="logo"></a> -->
        </div>
    </div>

    <div class="sidebar-inner slimscrollleft">

        
        <div class="user-details">
            <div class="text-center">
                <img src="{{ $assets }}/images/users/avatar-6.jpg" alt="" class="rounded-circle">
            </div>
            <div class="user-info">
                <h4 class="font-16">{{ Auth::user()->name }} <small>({{ config('constants.user_type.'.Auth::user()->user_type) }})</small></h4>
                <span class="text-muted user-status"><i class="fa fa-dot-circle-o text-success"></i> Online</span>
            </div>
        </div>

        <div id="sidebar-menu">
            <ul>
                <li class="menu-title">Main</li>

                <li>
                    <a href="{{ url('home') }}" class="waves-effect">
                        <i class="ti-home"></i>
                        <span> Dashboard <span class="badge badge-primary pull-right d-none">3</span></span>
                    </a>
                </li>

                @if(isSuperAdmin())
                    <li>
                        <a href="{{url('owners')}}" class="waves-effect">
                            <i class="fa fa-users"></i>
                            <span>Owners</span>
                        </a>
                    </li>
                @endif

                @if(isSuperAdmin() || isOwner())
                    <li>
                        <a href="{{url('companies')}}" class="waves-effect">
                            <i class="fa fa-building"></i>
                            <span>Companies</span>
                        </a>
                    </li>
                @endif

                @if(isSuperAdmin() || isOwner() || isAgent())
                    <li>
                        <a href="{{url('categories/list')}}" class="waves-effect">
                            <i class="fa fa-sitemap"></i>
                            <span>Categories</span>
                        </a>
                    </li>

                    <li class="has_sub">
                        <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-product-hunt"></i> <span> Services </span> <span class="pull-right"><i class="mdi mdi-chevron-right"></i></span></a>
                        <ul class="list-unstyled">
                            <li><a href="{{ url('services/create') }}">Create Service</a></li>
                            <li><a href="{{url('services')}}">Service List</a></li>
                        </ul>
                    </li>

                    <li class="has_sub">
                        <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-user-o"></i> <span> Customer Profiles </span> <span class="pull-right"><i class="mdi mdi-chevron-right"></i></span></a>
                        <ul class="list-unstyled">
                            <li><a href="{{ url('customer-profiles/create') }}">Create Customer Profile</a></li>
                            <li><a href="{{url('customer-profiles/list')}}">Customer Profile List</a></li>
                        </ul>
                    </li>
                @endif

                @if(isSuperAdmin() || isOwner())
                    <li class="has_sub">
                        <a href="javascript:void(0);" class="waves-effect"><i class="mdi mdi-account-multiple"></i> <span> Agents </span> <span class="pull-right"><i class="mdi mdi-chevron-right"></i></span></a>
                        <ul class="list-unstyled">
                            <li><a href="{{ url('create-agent') }}">Create Agent</a></li>
                            <li><a href="{{url('agents')}}">Agent List</a></li>
                        </ul>
                    </li>
                @endif

                @if(isOwner() || isAgent())
                    <li class="has_sub">
                        <a href="javascript:void(0);" class="waves-effect"><i class="mdi mdi-file-document"></i> <span> Invoices </span> <span class="pull-right"><i class="mdi mdi-chevron-right"></i></span></a>
                        <ul class="list-unstyled">
                            <li><a href="{{ url('invoices/create') }}">Create Invoice</a></li>
                            <li><a href="{{url('invoices/list')}}">Invoice List</a></li>
                        </ul>
                    </li>

                    <li class="has_sub">
                        <a href="javascript:void(0);" class="waves-effect"><i class="mdi mdi-file-document-box"></i> <span> Recurring Invoices </span> <span class="pull-right"><i class="mdi mdi-chevron-right"></i></span></a>
                        <ul class="list-unstyled">
                            <li><a href="{{ url('recurring-invoices/create') }}">Create Recurring Invoice</a></li>
                            <li><a href="{{url('recurring-invoices/list')}}">Recurring Invoice List</a></li>
                        </ul>
                    </li>
                @endif

                <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-newspaper-o"></i> <span> Reports </span> <span class="pull-right"><i class="mdi mdi-chevron-right"></i></span></a>
                    <ul class="list-unstyled">
                        <li><a href="{{ url('reports/customerwise-income') }}">Customerwise Income</a></li>
                        <li><a href="{{ url('reports/invoice') }}">Invoice</a></li>
                        <li><a href="{{ url('reports/income') }}">Income</a></li>
                        <li><a href="{{ url('reports/popular-services') }}">Popular Services</a></li>
                    </ul>
                </li>

                @if(isOwner())
                    <li>
                        <a href="{{url('settings')}}" class="waves-effect">
                            <i class="mdi mdi-settings"></i>
                            <span> Settings <span class="badge badge-primary pull-right d-none">3</span></span>
                        </a>
                    </li>
                @endif

            </ul>
        </div>
        <div class="clearfix"></div>
    </div> <!-- end sidebarinner -->
</div>
<!-- Left Sidebar End -->