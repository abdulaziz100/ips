<!DOCTYPE html>
<html>
    <head>
        @include('layouts.public_head')
    </head>


    <body>

        <!-- Begin page -->
        <div class="wrapper-page">

            <div class="card">
                <div class="card-body">

                    <h3 class="text-center mt-0 m-b-15">
                        <a href="#/" class="logo logo-admin">{{ config('constants.app_name') }}</a>
                    </h3>

                    @yield('content')

                </div>
            </div>
        </div>

        @include('layouts.public_foot')
        
    </body>
</html>