<!-- jQuery  -->
<script src="{{ $assets }}/js/jquery.min.js"></script>
<script src="{{ $assets }}/js/bootstrap.min.js"></script>
<script src="{{$assets}}/plugins/toaster/jquery.toast.js"></script>

@if(session()->has('toast'))
    
    @php
        $toast = session()->get('toast');
        $message = $toast['message'];
        $type = $toast['type'];
    @endphp

    <script>
    	function toastMsg(message, type) {
		    $.toast({
		        //heading: type,
		        text: message,
		        showHideTransition: 'slide',
		        icon: type,
		        allowToastClose: true,
		        hideAfter: 6000,   // in milli seconds
		        position: 'top-right'
		    });
		}
        toastMsg("{!! $message !!}","{{ $type }}");
    </script>
@endif
