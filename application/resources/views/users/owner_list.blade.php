@extends('layouts.master')

@section('title') List of Owners @endsection 

@section('content')	

<div class="row">
	<div class="col-md-12">
		<div class="card m-b-30">
			<div class="card-header">
                <h4 class="card-title"> List of {{ Request::is('owners') ? 'Owners':'Agents' }}
                @if(Request::is('agents'))
                    <a href="{{ url('create-agent')}}" class="btn btn-danger btn-xs pull-right"><i class="fa fa-plus-circle"></i> Add New</a>
                @endif
                </h4>
            </div>
			<div class="card-body">
				{!! Form::open(['url'=>Request::url(), 'role' => 'form']) !!}
                <div class="row">
                    <div class="col-sm-11 padding-right-0">
                        <div class="form-group">
                            {!! Form::text('search_item',Request::get('search_item') ? Request::get('search_item'):'',['class'=>'form-control','placeholder'=>'Search the users by their name or email and hit Enter']) !!}
                        </div>
                    </div>
                    <div class="col-sm-1">
                        <div class="form-group">
                            <a href="{{Request::url()}}" class="btn btn-default float-right" title="Refresh"><i class="fa fa-refresh" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}

                <div class="table-responsive">
					<table class="table table-striped table-bordered">
					    <thead>
					        <tr>
					            <td>Name</td>
					            <td>Email</td>
					            <td width="6%">Status</td>
					            <td width="18%">Created at</td>
					            <td width="7%">Actions</td>
					        </tr>
					    </thead>
					    <tbody>
					    @forelse($users as $user)
					        <tr>
					            <td>{{ $user->name }}</td>
					            <td>{{ $user->email }}</td>
								<td><span class="label label-{{ $user->active ? 'success' : 'danger' }}">{{ $user->active ? 'Active' : 'Inactive' }}</span></td>
								<td>{{ $user->created_at->format('d M,Y h:i A') }}</td>
					            <td class="action-column">

					                {{-- Reset Password --}}
					                <a href="javascript:showPasswordResetForm({{ $user->id }});" class="btn btn-default btn-xs hide" title="Reset Password"><i class="fa fa-undo"></i></a>

					                {{-- Resend Verification Link --}}
					                <a class="btn btn-xs btn-primary hide" href="{{ URL::to('users/resend-verification/' . $user->id ) }}" title="Resend verification Link"><i class="fa fa-send"></i></a>

					                {{-- Activate/Deactivate --}}
                                    <a href="#" data-id="{{$user->id}}" data-action="{{ url('users/change-active') }}" data-message="Are you sure, You want to {{$user->active ? 'deactivate' : 'activate' }} this user?" class="btn btn-xs btn-{{$user->active ? 'danger':'info'}} alert-dialog" data-toggle="tooltip" title="{{$user->active ? 'Deactivate User':'Activate User'}}"><i class="fa fa-{{$user->active ? 'ban':'check'}}"></i></a>

					                {{-- Delete --}}
                                    <a href="#" data-id="{{$user->id}}" data-action="{{ url('users/delete') }}" data-message="Are you sure, You want to delete this user?" class="btn btn-danger btn-xs alert-dialog" title="Delete User"><i class="fa fa-trash white"></i></a>
					            </td>
					        </tr>
					    @empty
					    	<tr>
					    		<td colspan="5" align="center">No record found!</td>
					    	</tr>
					    @endforelse
					    </tbody>
					</table>
				</div>
			</div><!-- end card-body -->
			@if($users->total() > 10)
			<div class="card-footer">
				<div class="row">
					<div class="col-sm-4">{{ $users->paginationSummery }}</div>
					<div class="col-sm-8 text-right">{!! $users->links() !!}</div>
				</div>
			</div>
			@endif
		</div><!-- end card -->
	</div>
</div>

<!-- Modal -->
<div id="passwordResetModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Reset Password</h4>
      </div>
      {!! Form::open(['url' => 'admindashboard/reset-password', 'role' => 'form', 'id' => 'password-reset-form','autocomplete' => 'off']) !!}
      <div class="modal-body">
        <div id="password-reset-message"></div>
            <div class="form-group">
                {!! Form::label('password','Password') !!} <small class="text-danger">(required)</small>
                {!! Form::password('password',['class'=>'form-control','placeholder'=>'Password']) !!}
            </div>

            <div class="form-group">
                {!! Form::label('password_confirmation','Confirm Password') !!} <small class="text-danger">(required)</small>
                {!! Form::password('password_confirmation',['class'=>'form-control','placeholder'=>'Confirm Password']) !!}
            </div>
      </div>
      <div class="modal-footer">
        {!! Form::hidden('user_id', '') !!}
        <button class="btn btn-info" type="button" onclick="processPasswordResetData();">Reset Password</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
      {!! Form::close() !!}
    </div>

  </div>
</div>
@endsection

@push('custom_style')
	
@endpush

@push('custom_script')
<script>
{{-- Open Password Reset form --}}
function showPasswordResetForm(id)
{
    if(!id) {
        alert('User id missing!');

        return false;
    }else {
        $('input[name=user_id]').val(id);
    }

    $('#passwordResetModal').modal();
}

{{-- Process password reset data --}}
function processPasswordResetData()
{
	$('#preloader').removeClass('hide');
    $.ajax({
        url:'{{ url('users/resetpassword') }}',
        method:'POST',
        data:$('#password-reset-form').serialize(),
        // dataType:'JSON',
        success:function(data) {
            $('#password-reset-message').html(data);
        },
        error:function(xhr, ajaxOptions, thrownError) {
            alert( xhr.status + ' ' + thrownError );
        },
        complete:function(data, status) {
            if(data.responseText.indexOf('alert-success') > -1) {
            	setTimeout(function() {
            		location.reload();
            	}, 2000);
            }
            $('#preloader').addClass('hide');
        }
    });
}
</script>	
@endpush
