<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            <label for="email" class="control-label"> Name {!! validation_error($errors->first('name'),'name') !!} </label>
            {!! Form::text('name',old('name'),['class'=>'form-control','placeholder'=>'Enter your name']) !!}
        </div>
    </div>
</div>
<div class="row">
	<div class="col-sm-6">
		<div class="form-group">
    		<label for="email" class="control-label"> Username {!! validation_error($errors->first('username'),'username') !!} </label>
    		{!! Form::text('username',old('username'),['class'=>'form-control','placeholder'=>'Enter your username']) !!}
		</div>
	</div>
	<div class="col-sm-6">
		<div class="form-group">
			<label for="email" class="control-label"> Email {!! validation_error($errors->first('email'),'email') !!} </label>
			{!! Form::text('email',old('email'),['class'=>'form-control','placeholder'=>'Enter your email']) !!}
		</div>
	</div>
</div>
<div class="row">
	<div class="col-sm-6">
		<div class="form-group">
    		<label for="password" class="control-label"> Password {!! validation_error($errors->first('password'),'password') !!} </label>
    		{!! Form::password('password',['class'=>'form-control','placeholder'=>'Enter your password', 'id' => 'password', 'autocomplete' => 'off']) !!}
		</div>
	</div>
	<div class="col-sm-6">
		<div class="form-group">
			{!! Form::label('password_confirmation','Confirm Password') !!}
    		{!! Form::password('password_confirmation',['class'=>'form-control','placeholder'=>'Enter your password again']) !!}
		</div>
	</div>
</div>