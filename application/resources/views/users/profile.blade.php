@extends('layouts.master')

@section('title') Profile @endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="card m-b-30">
                <div class="card-header">
                   <h4 class="card-title">Profile</h4> 
                </div>

				{!! Form::model($user, ['url' => 'profile', 'role' => 'form', 'autocomplete' => 'off']) !!}
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                {!! Form::label('name','Name') !!}
                                {!! Form::text('name',old('name'),['class'=>'form-control','placeholder'=>'Enter your name']) !!}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                    	<div class="col-sm-6">
                    		<div class="form-group">
	                    		{!! Form::label('username','Username') !!}
	                    		{!! Form::text('username',old('username'),['class'=>'form-control','placeholder'=>'Enter your username', 'disabled' => 'disabled']) !!}
                    		</div>
                    	</div>
                    	<div class="col-sm-6">
                    		<div class="form-group">
                    			{!! Form::label('email','Email') !!}
                    			{!! Form::text('email',old('email'),['class'=>'form-control','placeholder'=>'Enter your email', 'disabled' => 'disabled']) !!}
                    		</div>
                    	</div>
                    </div>
                    <div class="row">
                    	<div class="col-sm-6">
                    		<div class="form-group">
	                    		<label for="password" class="control-label"> Password {!! validation_error($errors->first('password'),'password', true) !!} </label>
	                    		{!! Form::password('password',['class'=>'form-control','placeholder'=>'Enter your password', 'id' => 'password', 'autocomplete' => 'off']) !!}
                    		</div>
                    	</div>
                    	<div class="col-sm-6">
                    		<div class="form-group">
                    			{!! Form::label('password_confirmation','Confirm Password') !!}
	                    		{!! Form::password('password_confirmation',['class'=>'form-control','placeholder'=>'Enter your password again']) !!}
                    		</div>
                    	</div>
                    </div>
                </div>
				<div class="card-footer">
					<button class="btn btn-info"><i class="fa fa-save" aria-hidden="true"></i> Save</button>
				</div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
@endsection
