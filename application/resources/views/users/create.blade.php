@extends('layouts.master')

@section('title') Create Agent @endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="card m-b-30">
                <div class="card-header">
                    <h4 class="card-title">Create Agent</h4>
                </div>

				{!! Form::open(['url' => 'store-agent', 'role' => 'form', 'autocomplete' => 'off']) !!}
                    <div class="card-body">
                        @include('users.form')
                    </div>
    				<div class="card-footer">
    					<button class="btn btn-info"><i class="fa fa-save" aria-hidden="true"></i> Save</button>
    				</div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
@endsection
