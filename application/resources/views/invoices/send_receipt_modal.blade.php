{{-- Invoice sent receipt modal --}}
<div id="sentReceiptModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Send a Receipt</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      {!! Form::open(['url' => '', 'role' => 'form', 'id' => 'sent-receipt-form']) !!}
      <div class="modal-body">
        <div class="row">
            <div class="col-sm-11">
                <div class="form-group">
                    <strong>From: </strong>
                    <span id="owner-email">{{ Auth::user()->email }}</span>
                </div>
            </div>
        </div>
        
        <div class="row">
            <div class="col-sm-11">
                <div class="form-group">
                    <label for="email">To: {!! validation_error($errors->first('to'),'to') !!}</label>
                    @if(!empty($invoice))
                        {!! Form::email('to',$invoice->customer_profile->email,['class'=>'form-control','placeholder'=>'To', 'id' => 'to']) !!}
                    @else
                        {!! Form::email('to','',['class'=>'form-control','placeholder'=>'To', 'id' => 'to']) !!}
                    @endif
                </div>
            </div>
            <div class="col-sm-1">
                <a class="btn btn-primary btn-xs margin-top-30" href="javascript:openCC();" title="Add CC"><i class="fa fa-plus-circle" aria-hidden="true"></i></a>
            </div>
        </div>

        <div class="row cc-row hide">
            <div class="col-sm-11">
                <div class="form-group">
                    <label for="email">CC: </label>
                    {!! Form::email('ccs',null,['class'=>'form-control','placeholder'=>'CC', 'data-role' => 'tagsinput']) !!}
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-11">
                <div class="form-group">
                    <strong>Subject</strong>
                    <span id="email-subject">
                    @if(!empty($invoice))
                        Payment Receipt for Invoice #{{ $invoice->identifier }}
                    @endif
                    </span>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-11">
                <div class="form-group">
                    {!! Form::label('message', 'Message') !!}
                    @if(!empty($invoice))
                        {!! Form::textarea('message','',['class'=>'form-control','placeholder'=>'Enter your message to '.$invoice->customer_profile->name, 'size' => '30x4']) !!}
                    @else
                        {!! Form::textarea('message','',['class'=>'form-control','placeholder'=>'Enter your message to customer', 'size' => '30x4']) !!}
                    @endif
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-11">
                <div class="checkbox">
                    <strong>Delivery</strong> <br>
                    <label class="padding-left-0">{!! Form::checkbox('bcc',1,false) !!} Send a copy to myself at {{ Auth::user()->email }}</label>
                </div>
            </div>
        </div>

      </div>
      <div class="modal-footer">
        {!! Form::hidden('payment_id', '') !!}
        <button type="button" class="btn btn-primary" onclick="sendReceipt();"><i class="fa fa-send" aria-hidden="true"></i> Send</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
      {!! Form::close() !!}
    </div>

  </div>
</div>
{{-- /Invoice sent receipt modal --}}