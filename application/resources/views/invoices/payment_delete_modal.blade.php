{{-- Payment Delete Modal --}}
<div id="paymentDeleteModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Remove Invoice Payment</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <div class="jumbotron text-center"><i class="fa fa-trash fa-5x" aria-hidden="true"></i>
            <h4 class="bold">Payment for ৳<span class="payment-amount"></span> using cash</h4>
            <span>Are you sure you want to remove this invoice payment?</span>
        </div>
      </div>
      <div class="modal-footer">
        {!! Form::hidden('payment_id', '') !!}
        <button type="button" class="btn btn-danger btn-delete-payment" onclick="removePayment({{ $invoice->id }});"><i class="fa fa-trash" aria-hidden="true"></i> Remove Payment</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
