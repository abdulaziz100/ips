@extends('layouts.master')

@section('title') Preview Email @endsection

@section('content')
<div class="card">
    <div class="card-body">
        @php 
            $owner = $invoice->owner;
            $company = $owner->companies()->first();
        @endphp
        <div class="invoice-logo-holder text-center">
            <img src="{{ $assets. '/images/company_logo_default.png' }}" alt="Invoice Logo" width="300" height="250">
        </div>
        <div class="well well-lg invoice-amount-info">
            <div>
                <strong>{{ $company->name }}</strong> has sent you an invoice for <br>
                <span class="invoice-amount">৳{{ number_format($invoice->total,2) }}</span> <br>
                <span class="invoice-due-date text-muted">
                    Due on {{ \Carbon::parse($invoice->payment_due)->format('F d,Y') }}
                </span>
            </div>
        </div>
        
        <div class="view-online">
            <a href="#" class="btn btn-primary">View Online</a>
        </div>
        
        @if(!empty($email_message))
            <div class="email-message">{{ $email_message }}</div>
        @endif

        <div class="invoice-contact-email margin-top-20">
            For questions about this invoice, please contact <br>
            <a href="mailto:{{ $owner->email }}">{{ $owner->email }}</a>
        </div>

        <div class="invoice-contact-location margin-top-20">
            <strong>{{ $company->name }}</strong> <br>
            @if($company->address)
                <span>{{ $company->address }} <br></span>
            @endif
            @if($company->state || $company->city || $company->zip_code)
                <span>{{ ($company->state ? $company->state .', ' : '') . ($company->city ? $company->city .' ' : '') .($company->zip_code ? $company->zip_code : '') }}</span> <br>
            @endif
            {{ $company->country_obj->name }}
        </div>

        <div class="invoice-contact-phone margin-top-20">
            <span>Phone: </span>{{ $company->primary_phone }}
            <br>
            <span>Alt Phone: </span>{{ $company->alt_phone }}
            <br>
            {{ $company->website }}
        </div>
    </div>
</div>

<footer class="footer margin-top-30">
	<span class="powered-by">Powered by {{ config('constants.app_name') }}</span> <br>
	<div class="margin-top-10">
	  <small>&copy; {{ config('constants.app_name') }}. All right reserved. Privacy Policy. Term of Use</small>
	</div>
</footer>
@endsection

@section('custom-style')
<style>
    .container {
        margin: 40px auto;
        text-align: center;
        width: 600px;
    }
    .invoice-amount-info {
        line-height: 40px;
        margin-top: 20px;
        text-align: center;
    }
    .invoice-amount {
        font-size: 30px;
    } 
    .view-online {
        border-bottom: 1px solid #dfdfdf;
        padding: 0 10px 15px;
        text-align: center;
    }
    .email-message {
        border-bottom: 1px solid #dfdfdf;
        padding: 15px 10px;
        text-align: justify;
    }
</style>
@endsection
 
