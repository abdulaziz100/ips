@extends('layouts.master')

@section('title') Edit Invoice @endsection

@section('content')
<div class="row">
	<div class="col-sm-12">
		<div class="card m-b-30"> 
			<div class="card-header">
				<h4 class="card-title">Edit Invoice</h4>
			</div>

			{!! Form::model($invoice,array('url' => 'invoices', 'role' => 'form')) !!}
			<div class="card-body">
				@include('invoices.form')
			</div><!-- /.card-body -->
			<div class="card-footer text-right">
				{!! Form::hidden('invoice_id',$invoice->id) !!}
				<button type="button" name="button_value" value="preview" class="btn btn-primary hide">
					<i class="fa fa-eye"></i> Preview
				</button>
				<button class="btn btn-info">
					<i class="fa fa-save"></i> Save & continue
				</button>
			</div>
			{!! Form::close() !!}
		</div>

		@include('invoices.customer_form_modal')
	</div>
</div>
	
@endsection
