{{-- Invoice email modal --}}
<div id="invoiceEmailModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Send the Invoice</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      {!! Form::open(['url' => '', 'role' => 'form', 'id' => 'invoice-email-form']) !!}
      <div class="modal-body">
        <div class="row">
            <div class="col-sm-11">
                <div class="form-group">
                    <label for="email">From: {!! validation_error($errors->first('from'),'from') !!}</label>

                    @if(!empty($invoice))
                        {!! Form::email('from',$invoice->owner->email,['class'=>'form-control','placeholder'=>'From']) !!} 
                    @else
                        {!! Form::email('from','',['class'=>'form-control','placeholder'=>'From']) !!}
                    @endif
                </div>
            </div>
            <div class="col-sm-1">
                <a href="#" class="btn btn-primary btn-xs margin-top-30" title="You can only select this verified email"><i class="fa fa-warning" aria-hidden="true"></i></a>
            </div>
        </div>
        
        <div class="row">
            <div class="col-sm-11">
                <div class="form-group">
                    <label for="email">To: {!! validation_error($errors->first('to'),'to') !!}</label>

                    @if(!empty($invoice))
                        {!! Form::email('to',$invoice->customer_profile->email,['class'=>'form-control','placeholder'=>'To']) !!}
                    @else
                        {!! Form::email('to','',['class'=>'form-control','placeholder'=>'To']) !!}
                    @endif
                </div>
            </div>
            <div class="col-sm-1">
                <a class="btn btn-primary btn-xs margin-top-30" href="javascript:openCC();" title="Add CC"><i class="fa fa-plus-circle" aria-hidden="true"></i></a>
            </div>
        </div>

        <div class="row cc-row hide">
            <div class="col-sm-11">
                <div class="form-group">
                    <label for="email">CC: </label>
                    {!! Form::email('ccs',null,['class'=>'form-control','placeholder'=>'CC', 'data-role' => 'tagsinput']) !!}
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-11">
                <div class="form-group">
                    {!! Form::label('subject', 'Subject') !!}
                    @if(!empty($invoice) && !empty($company))
                        @php 
                            $subject = 'Invoice #' . $invoice->identifier .' from '.$company->name 
                        @endphp
                        <div class="email-subject">{{ $subject }} <a href="javascript:openEmailSubject();">Edit</a></div>
                        {!! Form::text('subject',$subject,['class'=>'form-control hide','placeholder'=>'Subject']) !!}
                    @else
                        <div class="email-subject"><span class="email-subject-name">Subject</span> <a href="javascript:openEmailSubject();">Edit</a></div>
                        {!! Form::text('subject','Subject',['class'=>'form-control hide','placeholder'=>'Subject']) !!}
                    @endif
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-11">
                <div class="form-group">
                    <label for="email">Message: </label>
                    @if(!empty($invoice))
                        {!! Form::textarea('message','',['class'=>'form-control','placeholder'=>'Enter your message to '.$invoice->customer_profile->name, 'size' => '30x4']) !!}
                    @else
                        {!! Form::textarea('message','',['class'=>'form-control','placeholder'=>'Enter your message to customer', 'size' => '30x4']) !!}
                    @endif
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-11">
                <a class="btn-collapsed collapsed bold" data-toggle="collapse" href="#demo">Show Addtional Options</a>
                <div id="demo" class="collapse">
                    <div class="checkbox">
                        <label>{!! Form::checkbox('bcc',1,false) !!} Send a copy to myself at {{ Auth::user()->email }}</label>
                    </div>
                    <div class="checkbox">
                        <label>{!! Form::checkbox('attachment',1,false) !!} Attach the invoice as a PDF </label>
                    </div>
                </div> 
            </div>
        </div>

      </div>
      <div class="modal-footer">
        @if(!empty($invoice))
            <button type="button" class="btn btn-primary btn-send" onclick="sendEmail({{ $invoice->id }});"><i class="fa fa-send" aria-hidden="true"></i> Send</button>
        @else
            <button type="button" class="btn btn-primary btn-send" onclick="sendEmail('');"><i class="fa fa-send" aria-hidden="true"></i> Send</button>
        @endif
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
      {!! Form::close() !!}
    </div>

  </div>
</div>
{{-- /Invoice email modal --}}