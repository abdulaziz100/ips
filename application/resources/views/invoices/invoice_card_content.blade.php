<div class="card-header">
	<h4 class="card-title">List of Invoices 
		<a href="{{ url('invoices/create')}}" class="btn btn-danger btn-xs pull-right"><i class="fa fa-plus-circle"></i> Add New</a>
	</h4>
</div>

<div class="card-body">

	{!! Form::open(['url' => Request::url(), 'role'=>'form', 'id' => 'invoice-filter-form']) !!}
	<div class="row">
		<div class="col-lg-3">
			<div class="form-group">
				{!! Form::select('customer_profile_id',$customer_profiles, Request::input('customer_profile_id') ? Request::input('customer_profile_id') :'',['class'=>'form-control chosen-select','data-placeholder'=>'Select a customer', 'onchange' => 'this.form.submit();']) !!}
			</div>
		</div>
		<div class="col-lg-2">
			<div class="form-group">
				{!! Form::select('status',$invoice_statuses, Request::input('status') ? Request::input('status') :'',['class'=>'form-control chosen-select', 'onchange' => 'this.form.submit();']) !!}
			</div>
		</div>

		<div class="col-lg-4">
			<div class="form-group">
				<div id="reportrange" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
					<i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
					<span></span> <b class="caret"></b>
				</div>
			</div>
			{!! Form::hidden('date_range', Request::input('date_range')) !!}
		</div>

		<div class="col-lg-2">
			<div class="form-group">
				<div class="input-group mb-2">
					{!! Form::text('identifier',Request::input('identifier') ? Request::input('identifier') :'',['class'=>'form-control','placeholder' => 'Identifier', 'aria-label' => 'Identifier', 'aria-describedby' => 'basic-addon2']) !!}
					<div class="input-group-append">
						<button class="btn btn-outline-secondary" type="submit"  data-toggle="tooltip" title="Search"><i class="fa fa-search"></i></button>
					</div>
				</div>
			</div>
		</div>

		<div class="col-lg-1">
			<a href="{{ url()->current() }}" class="btn btn-default pull-right" data-toggle="tooltip" title="Refresh"><i class="fa fa-refresh"></i></a>
		</div>
	</div>
	{!! Form::close() !!}

	<div class="table-responsive">
		<table class="table table-responsive table-bordered training-table">
			<thead>
				<th width="7%">Identifier</th>
				<th>Customer</th>
				<th width="12%" class="text-right">Total</th>
				<th width="12%">Invoice Date</th>
				<th width="12%">Due Date</th>
				<th width="7%">Status</th>
				<th width="18%">Action</th>
			</thead>
			<tbody>
				@forelse($invoices as $invoice)
				<tr>
					<td>{{ strtoupper($invoice->identifier) }}{!! $invoice->ril_id ? '<br/><small class="text-muted">Recurring</small>' : '' !!}</td>
					<td>{{ $invoice->customer_profile->name }}</td>
					<td class="text-right">৳{{ number_format($invoice->total, 2) }}</td>
					<td>{{ Carbon::parse($invoice->invoice_date)->format('d M, Y') }}</td>
					<td>{{ Carbon::parse($invoice->payment_due)->format('d M, Y') }}</td>
					<td>
						{!! $invoice->getStatus() !!}
					</td>
					<td>
						@php
						$invoice_status = $invoice->getStatus(false);
						@endphp

						@if($invoice_status == 'draft')
						<a href="javascript:approveInvoice({{ $invoice->id }});" class="btn btn-info btn-xs">Approve</a>
						@elseif($invoice_status == 'unsent')
						<a href="javascript:openInvoiceEmailModal({{ $invoice->id }});" class="btn btn-info btn-xs">Send</a>
						@elseif(in_array($invoice_status, ['sent', 'partial', 'viewed']))
						<a href="javascript:openPaymentModal({{ $invoice->id}});" class="btn btn-info btn-xs">Record a payment</a>
						@elseif($invoice_status == 'overdue')
						<a href="javascript:openRemindInvoiceModal({{ $invoice->id }});" class="btn btn-info btn-xs">Send a Reminder</a>
						@else
						<a href="{{ url('invoices/'.$invoice->id) }}" class="btn btn-info btn-xs">View</a>
						@endif

						<div class="btn-group">
							<button class="btn btn-secondary btn-xs dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">

							</button>
							<div class="dropdown-menu">
								<a class="dropdown-item" href="{{url('invoices/'.$invoice->id)}}" title="View this information"><i class="fa fa-eye"></i> View</a>
								<a class="dropdown-item" href="{{url('invoices/'.$invoice->id.'/edit')}}" title="Edit this information"><i class="fa fa-pencil"></i> Edit</a>
								<div class="dropdown-divider"></div>
								<a class="dropdown-item" href="{{ url('invoice-details/'. $invoice->identifier) }}"><i class="fa fa-eye-slash"></i>  Preview as Customer</a>
								<a class="dropdown-item" href="{{ url('invoice-details/'. $invoice->identifier) }}?view=pdf" target="_blank"><i class="fa fa-file-pdf-o"></i>  Export as PDF</a>
								<a class="dropdown-item" href="{{ url('invoice-details/'. $invoice->identifier) }}?view=print" target="_blank"><i class="fa fa-print"></i>  Print</a>
								<a class="dropdown-item alert-dialog" href="#" data-id="{{$invoice->id}}" data-action="{{ url('invoices/delete') }}" data-message="Are you sure, You want to delete this invoice?" title="Delete invoice btn-danger"><i class="fa fa-trash white"></i> Delete</a>
							</div>
						</div>
					</td>
				</tr>
				@empty
				<tr>
					<td colspan="7" align="center">No Records Found</td>
				</tr>
				@endforelse
			</tbody>
		</table>
	</div>
</div><!-- /.card-body -->

@if($invoices->total() > 10)
<div class="card-footer">
	<div class="row">
		<div class="col-md-4">
			{{ $invoices->paginationSummery }}
		</div>
		<div class="col-md-8 text-right">
			<div class="float-right">
				{!! $invoices->links() !!}
			</div>
		</div>
	</div>
</div>
@endif