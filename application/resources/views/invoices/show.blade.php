@extends('layouts.master')

@section('title') Invoice #{{ strtoupper($invoice->identifier) }} @endsection

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="card m-b-30 panel-invoice">
			<div class="card-header">
				<h4 class="card-title">
					Invoice #{{ strtoupper($invoice->identifier) }}

					<div class="pull-right">
						<div class="btn-group">
						  <button class="btn btn-secondary btn-lg dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						    More Action
						  </button>
						  <div class="dropdown-menu">
						  	<a class="dropdown-item" href="{{ url('invoice-details/'. $invoice->identifier) }}?view=pdf" target="_blank"><i class="fa fa-file-pdf-o"></i>  Export as PDF</a>
						    <a class="dropdown-item" href="{{ url('invoice-details/'. $invoice->identifier) }}?view=print" target="_blank"><i class="fa fa-print"></i>  Print</a>
						    <div class="dropdown-divider"></div>
						    <a class="dropdown-item" href="{{ url('settings') }}"><i class="mdi mdi-settings"></i>  Customize</a>
						    <a class="dropdown-item" href="{{ url('invoice-details/'. $invoice->identifier) }}"><i class="fa fa-eye-slash"></i>  Preview as Customer</a>
						    <div class="dropdown-divider"></div>
						    <a class="dropdown-item alert-dialog" href="#" data-id="{{$invoice->id}}" data-action="{{ url('invoices/delete') }}" data-message="Are you sure, You want to delete this invoice?" title="Delete invoice btn-danger"><i class="fa fa-trash white"></i> Delete</a>
						  </div>
						</div>

						<a href="{{ url('invoices/create') }}" class="btn btn-secondary btn-lg">Create Another Invoice</a>
					</div>
				</h4>
			</div>
			
			<div class="card-body">
				<div class="row details-info">
					<div class="col-md-2">
						<strong class="text-muted">STATUS</strong>
						<div class="invoice-status-wrapper">
							@if(in_array($invoice->status, ['unsent', 'sent', 'viewed']) && (Carbon::now()->diffInDays(Carbon::parse($invoice->payment_due), false) < 0))
								<span class="label label-danger invoice-status"> OVERDUE</span>
							@else
								<span class="label label-{{ config('constants.invoice_status_class.'.$invoice->status) }} invoice-status"> {{ strtoupper($invoice->status) }}</span>
							@endif
						</div>
					</div>
					<div class="col-md-6">
						<strong class="text-muted">CUSTOMER</strong> <br>
						<h4 class="text-primary no-margin inline-block"><a href="javascript:void(0);" class="customer-profile bold">{{ $customer_profile->name }} <i class="fa fa-exclamation-circle" aria-hidden="true"></i></a></h4>
					</div>
					<div class="col-md-2">
						<strong class="text-muted">AMOUNT DUE</strong> <br>
						<span class="amount-due">৳{{ number_format($amount_due,2) }}</span>
					</div>
					<div class="col-md-2">
						<strong class="text-muted">DUE</strong> <br>
						<span class="due-time">{{ $invoice->status == 'paid' ? '__' : Carbon::parse($invoice->payment_due)->diffForHumans(Carbon::now()) }}</span>
					</div>
				</div>

				{{-- Panel One --}}
				<div class="panel panel-{{ $invoice->status == 'draft' ? 'primary' : 'default' }} panel-one margin-top-20">
					<div class="panel-body">
						
						@if($invoice->status == 'draft')
							<div class="draft-message">
								<i class="fa fa-exclamation-circle" aria-hidden="true"></i> This is a <strong>DRAFT</strong> invoice. You can take further actions once you approve it
							</div>
						@endif

						<div class="row">
							<div class="col-md-1">
								<span class="step-no step-one badge btn">{!! $invoice->status == 'draft' ? 1 : '<i class="fa fa-check" aria-hidden="true"></i>' !!}</span>
							</div>
							<div class="col-md-6">
								<h3 class="text-muted margin-top-0">
									Create Invoice
								</h3>
								<strong>Created: </strong>
								@if($invoice->created_at->diffInHours(Carbon::now()) > 24)
									on 	{{ Carbon::parse($invoice->invoice_date)->format('F d, Y \a\t H:i A e') }}
								@else
									{{ $invoice->created_at->diffForHumans(Carbon::now()) }}
								@endif

								@if($recurring_invoice_log = $invoice->recurring_invoice_log)
									from a <a href="{{ url('recurring-invoices/'.$recurring_invoice_log->recurring_invoice_id) }}">recurring invoice</a>
								@endif
							</div>
							<div class="col-md-5 step-one-button-holder">
							@if($invoice->status == 'draft')
								<a class="btn btn-primary btn-approve" onclick="approveInvoice({{ $invoice->id}});"><i class="fa fa-thumbs-o-up" aria-hidden="true"></i> Approve Draft</a>
								<a href="{{ url('invoices/'.$invoice->id.'/edit') }}" class="btn btn-default btn-edit"><i class="fa fa-edit" aria-hidden="true"></i> Edit Draft</a>
							@else
								<a href="{{ url('invoices/'.$invoice->id.'/edit') }}" class="btn btn-default btn-edit"><i class="fa fa-edit" aria-hidden="true"></i> Edit Invoice</a>
							@endif
							</div>
						</div>
					</div>
				</div>
				{{-- /Panel One --}}

				{{-- Panel Two --}}
				<div class="panel panel-{{ $invoice->status == 'unsent' && $invoice->invoice_sent_status == 'unsent' ? 'primary' : 'default' }} panel-two margin-top-20">
					<div class="panel-body">
						<div class="row">
							<div class="col-md-1">
								<span class="step-no step-two badge btn">{!! in_array($invoice->invoice_sent_status, ['marked-as-sent', 'sent']) ? '<i class="fa fa-check" aria-hidden="true"></i>' : '2' !!}</span>
							</div>
							<div class="col-md-6">
								<h3 class="text-muted margin-top-0">Send Invoice</h3>
								<div class="invoice-sent-time">
									@if($invoice->invoice_sent_status == 'unsent')
										<strong>Last Sent: </strong>never
									@elseif($invoice->invoice_sent_status == 'skipped')
										<strong>Skipped: </strong>You never sent this invoice.
									@elseif($invoice->invoice_sent_status == 'marked-as-sent')
										<strong>Last Sent: </strong>marked as sent {{ Carbon::parse($invoice->invoice_sent_date)->diffInDays(Carbon::now()) > 0 ? 'on ' .Carbon::parse($invoice->invoice_sent_date)->format('F d, Y') : 'today' }}
									@else
										<strong>Last Sent: </strong>with {{ config('constants.app_name')}} {{ Carbon::parse($invoice->invoice_sent_date)->diffInDays(Carbon::now()) > 0 ? 'on ' .Carbon::parse($invoice->invoice_sent_date)->format('F d, Y') : 'today' }}
									@endif
								</div>
							</div>
							<div class="col-md-5 step-two-button-holder">
							@if($invoice->status != 'draft')
								@if($invoice->invoice_sent_status == 'unsent')
									<a href="javascript:openInvoiceEmailModal({{ $invoice->id }});" class="btn btn-primary"><i class="fa fa-paper-plane-o" aria-hidden="true"></i> Send Invoice...</a>
									<a href="javascript:skipSendingInvoice({{ $invoice->id }});" class="btn btn-default">Skip Sending...</a>
								@elseif($invoice->invoice_sent_status == 'skipped')
									<a href="javascript:openInvoiceEmailModal({{$invoice->id}});" class="btn btn-primary"><i class="fa fa-paper-plane-o" aria-hidden="true"></i> Send Invoice...</a>
									<a href="javascript:markAsSent({{ $invoice->id }});" class="btn btn-default">Mark as Sent</a>
								@else
									<a href="javascript:openInvoiceEmailModal({{ $invoice->id }});" class="btn btn-default">Resend Invoice...</a>
								@endif
							@endif
							</div>
						</div>
					</div>
				</div>
				{{-- /Panel Two --}}

				{{-- Panel Three --}}
				<div class="panel panel-{{ !in_array($invoice->invoice_sent_status, ['skipped', 'marked-as-sent', 'sent']) || ($invoice->status == 'paid') ? 'default':'primary' }} panel-three margin-top-20">
					<div class="panel-body">
						<div class="row">
							<div class="col-md-1">
								<span class="step-no badge btn">{!! $invoice->status == 'paid' ? '<i class="fa fa-check" aria-hidden="true"></i>' : '3' !!}</span>
							</div>
							<div class="col-md-11">
							@if($invoice->invoice_sent_status == 'unsent')
								<h3 class="text-muted margin-top-0">Get Paid</h3>
								<strong>Amount Due: </strong>৳{{ number_format($amount_due,2) }}
							@else
								@if($invoice->status == 'paid')
									<h3 class="text-muted margin-top-0">Get Paid</h3>
									<div class="row">
										<div class="col-md-8">
											<strong>Status: </strong>Your invoice is paid in full.
										</div>
										<div class="col-md-4 text-right">
											<strong>Amount Due: </strong><span class="amount-due">৳{{ number_format($amount_due, 2) }}</span> 
										</div>
									</div>
								@else
								<div class="row">
									<div class="col-md-6">
										<h3 class="text-muted margin-top-0">Get Paid</h3>
									</div>
									<div class="col-md-6">
										<a href="javascript:openPaymentModal();" class="btn btn-primary">Record a Payment</a>
									</div>
								</div>

								<div class="alert alert-info margin-top-20">
									<div class="row">
										<div class="col-md-8">
											<strong>Status: </strong>{{ $invoice->total - $amount_due ? 'Your invoice is partially paid.' : 'Your invoice is awaiting payment.' }}
										</div>
										<div class="col-md-4 text-right">
											<strong>Amount Due: </strong><span class="amount-due">৳{{ number_format($amount_due, 2) }}</span> 
										</div>
									</div>
								</div>

								<div class="margin-top-20">
									<strong>Get paid on time by scheduling payment reminders for your customer:</strong>
								</div>

								@if($invoice->invoice_sent_status == 'skipped')
									<div class="alert alert-warning margin-top-20">
										To schedule payment reminders for your customer, you must first send the invoice or mark it as sent.
									</div>
								@endif

								{{-- Remind area --}}
								<div class="remind-area margin-top-20{{ $invoice->invoice_sent_status == 'skipped' ? ' form-disabled':''}}">
									<div class="row">

										<div class="col-sm-4">
											<div class="remind-before-area">
												<h5 class="bold">REMIND BEFORE DUE DATE</h5>

												@if(in_array($invoice->invoice_sent_status, ['marked-as-sent', 'sent']) && $invoice->isRemindableDay(-14))
													<div class="checkbox">
													  <label>{!! Form::checkbox('remind_days[]', -14, old('remind_days')) !!} 14 days before</label>
													</div>
												@else
													<div class="checkbox form-disabled">
													  <label>{!! Form::checkbox('remind_days[]', -14, old('remind_days'), ['disabled' => 'disabled']) !!} 14 days before</label>
													</div>
												@endif

												@if(in_array($invoice->invoice_sent_status, ['marked-as-sent', 'sent']) && $invoice->isRemindableDay(-7))
													<div class="checkbox">
													  <label>{!! Form::checkbox('remind_days[]', -7, old('remind_days')) !!} 7 days before</label>
													</div>
												@else
													<div class="checkbox form-disabled">
													  <label>{!! Form::checkbox('remind_days[]', -7, old('remind_days'), ['disabled' => 'disabled']) !!} 7 days before</label>
													</div>
												@endif

												@if(in_array($invoice->invoice_sent_status, ['marked-as-sent', 'sent']) && $invoice->isRemindableDay(-3))
													<div class="checkbox">
													  <label>{!! Form::checkbox('remind_days[]', -3, old('remind_days')) !!} 3 days before</label>
													</div>
												@else
													<div class="checkbox form-disabled">
													  <label>{!! Form::checkbox('remind_days[]', -3, old('remind_days'), ['disabled' => 'disabled']) !!} 3 days before</label>
													</div>
												@endif
											</div>
										</div>

										<div class="col-sm-4">
											<div class="remind-today-area">
												<h5 class="bold">REMINED ON DUE DATE</h5>

												@if(in_array($invoice->invoice_sent_status, ['marked-as-sent', 'sent']) && $invoice->isRemindableDay(0))
												<div class="checkbox">
												  <label>{!! Form::checkbox('remind_days[]', 0, old('remind_days')) !!} On due date</label>
												</div>
												@else
													<div class="checkbox form-disabled">
													  <label>{!! Form::checkbox('remind_days[]', 0, old('remind_days'), ['disabled' => 'disabled']) !!} On due date</label>
													</div>
												@endif
											</div>
										</div>

										<div class="col-sm-4">
											<div class="remind-after-area">
												<h5 class="bold">REMIND AFTER DUE DATE</h5>

												@if(in_array($invoice->invoice_sent_status, ['marked-as-sent', 'sent']) && $invoice->isRemindableDay(3))
													<div class="checkbox">
													  <label>{!! Form::checkbox('remind_days[]', 3, old('remind_days')) !!} 3 days after</label>
													</div>
												@else
													<div class="checkbox form-disabled">
													  <label>{!! Form::checkbox('remind_days[]', 3, old('remind_days'), ['disabled' => 'disabled']) !!} 3 days after</label>
													</div>
												@endif

												@if(in_array($invoice->invoice_sent_status, ['marked-as-sent', 'sent']) && $invoice->isRemindableDay(7))
													<div class="checkbox">
													  <label>{!! Form::checkbox('remind_days[]', 7, old('remind_days')) !!} 7 days after</label>
													</div>
												@else
													<div class="checkbox form-disabled">
													  <label>{!! Form::checkbox('remind_days[]', 7, old('remind_days'), ['disabled' => 'disabled']) !!} 7 days after</label>
													</div>
												@endif

												@if(in_array($invoice->invoice_sent_status, ['marked-as-sent', 'sent']) && $invoice->isRemindableDay(14))
													<div class="checkbox">
													  <label>{!! Form::checkbox('remind_days[]', 14, old('remind_days')) !!} 14 days after</label>
													</div>
												@else
													<div class="checkbox form-disabled">
													  <label>{!! Form::checkbox('remind_days[]', 14, old('remind_days'), ['disabled' => 'disabled']) !!} 14 days after</label>
													</div>
												@endif
											</div>
										</div>
									</div>
								</div>
								{{-- /Remind area --}}
								@endif
							@endif

							@if(in_array($invoice->getStatus(false), ['sent', 'partial', 'viewed', 'overdue']))
								<div class="margin-top-20">
									<a href="{{ url('invoice-reminder-preview/'.$invoice->identifier) }}" target="_blank">See a preview</a> of the reminder email, or <a href="javascript:openRemindInvoiceModal({{ $invoice->id }});">send a reminder</a> now.
								</div>
							@endif

							@if($payments->isNotEmpty())
								<hr>
								{{-- Payment Info --}}
								<section id="payment-info">
									<h4 class="bold">Payment Received:</h4>
									@foreach($payments as $payment)
										<div id="payment-item-{{ $payment->id }}" class="payment-item details-info margin-bottom-20">
											<span>{{ Carbon::parse($payment->payment_date)->format(getOption($company->id, 'invoice_date_format', 'Y-m-d')) }} - A payment for <strong>৳{{ number_format($payment->amount, 2) }}</strong>  ‎was made using {{ str_replace('-', ' ', $payment->payment_mood) }}</span>
											@if($payment->memo)
												<br>
												<small class="text-muted">{{ $payment->memo }}</small>
											@endif
											<br>
											<div class="action-area">
												<a href="javascript:openSendReceipt({{ $payment->id }});" class="btn btn-primary btn-xs"><i class="fa fa-send" aria-hidden="true"></i> Send a Receipt</a>

												<a href="javascript:openPaymentModal({{ $payment->id }});" class="btn btn-default btn-xs"><i class="fa fa-edit" aria-hidden="true"></i> Edit Payment</a>

												<a href="javascript:openRemovePaymentModal({{ $payment->id }});" class="btn btn-danger btn-xs btn-delete" data-amount="{{ $payment->amount }}"><i class="fa fa-trash" aria-hidden="true"></i> Remove Payment</a>
											</div>
										</div>
									@endforeach
								</section>
								{{-- /Payment Info --}}
							@endif
							</div>
						</div>
					</div>
				</div>
				{{-- /Panel Three --}}

				{{-- Panel Invoice --}}
				@include('invoices.invoice_panel')
				{{-- /Panel Invoice --}}

			</div><!-- /.panel-body -->
		</div>
	</div>
</div>

@include('invoices.invoice_email_modal')
@include('invoices.invoice_email_success_modal')
@include('invoices.payment_modal')
@include('invoices.payment_success_modal')
@include('invoices.payment_delete_modal')
@include('invoices.send_receipt_modal')
@include('invoices.send_receipt_success_modal')
@include('invoices.invoice_remind_modal')
@include('invoices.invoice_remind_success_modal')
	
@endsection

@section('custom-style')
{{-- Bootstrap Tags Input --}}
{!! Html::style($assets. '/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css') !!}
{{-- iCheck --}}
{!! Html::style($assets . '/plugins/icheck/skins/minimal/blue.css') !!}
@endsection

@section('custom-script')
{{-- Bootstrap Tags Input --}}
{!! Html::script($assets. '/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js') !!}

{{-- iCheck --}}
{!! Html::script($assets . '/plugins/icheck/icheck.min.js') !!}

<script>

{{-- To Approve Invoice --}}
function approveInvoice(invoiceId)
{
	$('#ajaxloader').removeClass('hide');
	$.ajax({
		url:'{{ url('invoices/approve') }}/'+invoiceId,
		method:'POST',
		success:function(response) {
			toastMsg(response.message, response.type);
			if(response.type == 'success') {
				$('.invoice-status-wrapper').html(response.invoice_status);
				if($('.panel-one').hasClass('panel-primary')) {
					$('.panel-one').removeClass('panel-primary').addClass('panel-default');
				}
				$('.draft-message').remove();

				$('.step-one').html('<i class="fa fa-check" aria-hidden="true"></i>');
				$('.step-one-button-holder').html('<a href="{{ url('invoices/'.$invoice->id.'/edit') }}" class="btn btn-default btn-edit"><i class="fa fa-edit" aria-hidden="true"></i> Edit Invoice</a>');
				if($('.panel-two').hasClass('panel-default')) {
					$('.panel-two').removeClass('panel-default').addClass('panel-primary');
				}
				$('.step-two-button-holder').html('<a href="javascript:openInvoiceEmailModal({{ $invoice->id }});" class="btn btn-primary"><i class="fa fa-paper-plane-o" aria-hidden="true"></i> Send Invoice...</a> <a href="javascript:skipSendingInvoice('+invoiceId+');" class="btn btn-default">Skip Sending...</a>');
			}
		},
		complete:function() {
			$('#ajaxloader').addClass('hide');
		},
		error:function(xhr, ajaxOptions, thrownError) {
			alert(xhr.status + ' ' + thrownError);
		}
	});
}

{{-- To Skip Sending Invoice --}}
function skipSendingInvoice(invoiceId)
{
	$('#ajaxloader').removeClass('hide');
	$.ajax({
		url:'{{ url('invoices/skip-sending-invoice') }}/'+invoiceId,
		method:'POST',
		success:function(response) {
			toastMsg(response.message, response.type);
			
			if(response.type == 'success') {
				// $('.invoice-status').text('UNSENT');
				if($('.panel-two').hasClass('panel-primary')) {
					$('.panel-two').removeClass('panel-primary').addClass('panel-default');
				}

				$('.invoice-sent-time').html('<strong>Skipped: </strong>You never sent this invoice.');

				$('.step-two-button-holder').html('<a href="javascript:openInvoiceEmailModal({{$invoice->id}});" class="btn btn-primary"><i class="fa fa-paper-plane-o" aria-hidden="true"></i> Send Invoice...</a> <a href="javascript:markAsSent({{ $invoice->id }});" class="btn btn-default">Mark as Sent</a>');
				if($('.panel-three').hasClass('panel-default')) {
					$('.panel-three').removeClass('panel-default').addClass('panel-primary');
				}

				$('.panel-three .col-md-11').html(response.panel_three_html);
				$('input[type="checkbox"]').iCheck({
				     checkboxClass: 'icheckbox_minimal-blue',
				     increaseArea: '20%' // optional
				});
			}
		},
		complete:function() {
			$('#ajaxloader').addClass('hide');
		},
		error:function(xhr, ajaxOptions, thrownError) {
			alert(xhr.status + ' ' + thrownError);
		}
	});
}

{{-- To Skip Sending Invoice --}}
function markAsSent(invoiceId)
{
	$('#ajaxloader').removeClass('hide');
	$.ajax({
		url:'{{ url('invoices/marked-as-sent') }}/'+invoiceId,
		method:'POST',
		success:function(response) {
			toastMsg(response.message, response.type);
			
			if(response.type == 'success') {

				$('.invoice-status-wrapper').html(response.invoice_status);
				$('.step-two').html('<i class="fa fa-check" aria-hidden="true"></i>');
				$('.step-two-button-holder').html('<a href="javascript:openInvoiceEmailModal({{ $invoice->id }});" class="btn btn-default">Resend Invoice...</a>');
				$('.invoice-sent-time').html('<strong>Last Sent: </strong>marked as sent today.');
				$('.panel-three .col-md-11').html(response.panel_three_html);
				$('input[type="checkbox"]').iCheck({
				     checkboxClass: 'icheckbox_minimal-blue',
				     increaseArea: '20%' // optional
				});
			}
		},
		complete:function() {
			$('#ajaxloader').addClass('hide');
		},
		error:function(xhr, ajaxOptions, thrownError) {
			alert(xhr.status + ' ' + thrownError);
		}
	});
}

{{-- Open Invoice Email Modal --}}
function openInvoiceEmailModal(invoiceId)
{
	$('input[type=checkbox]').iCheck('uncheck');
	$('#invoiceEmailModal input[name=ccs]').tagsinput('removeAll');
	$('#invoiceEmailModal').modal();
}

function openCC()
{
	$('.cc-row').toggleClass('hide');
}

function openEmailSubject()
{
	$('.email-subject').addClass('hide');
	$('#subject').removeClass('hide');
}

function sendEmail(invoiceId)
{
	$('#ajaxloader').removeClass('hide');
	$.ajax({
		url:'{{ url('invoices/send-email') }}/'+invoiceId,
		method:'POST',
		data:$('#invoice-email-form').serialize(),
		success:function(response) {
			if(response.status == 400){
				//validation error
				$.each(response.error, function(index, value) {
					$("#ve-"+index).html('['+value+']');
				});
			}else {
				toastMsg(response.message,response.type);
				if(response.type == 'success') {
					$('.invoice-status-wrapper').html(response.invoice_status);
					$('.panel-two').removeClass('panel-primary').addClass('panel-default');
					$('.step-two').html('<i class="fa fa-check" aria-hidden="true"></i>');
					$('.invoice-sent-time').html(response.invoice_sent_time);
					$('.step-two-button-holder').html('<a href="javascript:openInvoiceEmailModal({{ $invoice->id }});" class="btn btn-default">Resend Invoice...</a>');
					$('.panel-three').removeClass('panel-default').addClass('panel-primary');
					$('.panel-three .col-md-11').html(response.panel_three_html);
					$('input[type="checkbox"]').iCheck({
					     checkboxClass: 'icheckbox_minimal-blue',
					     increaseArea: '20%' // optional
					});
					$('#invoiceEmailModal').modal('hide');

					setTimeout(function() {
						$('#invoiceEmailSuccessModal').modal('show');
					}, 1000);
				}
			}
		},
		complete:function() {
			$('#invoice-email-form').trigger('reset');
			$('#ajaxloader').addClass('hide');
		},
		error:function(xhr, ajaxOptions, thrownError) {
			alert(xhr.status + ' ' +thrownError);
		}
	});
}

{{-- Open payment modal --}}
function openPaymentModal(payment_id=null)
{
	$('#ve-amount,#ve-payment_date').text('*');
	if(payment_id) {
		$('#ajaxloader').removeClass('hide');
		$.ajax({
			url:'{{ url('invoices/fetch-payment') }}/'+payment_id,
			method:'POST',
			success:function(response) {
				if(response.type == 'error') {
					toastMsg(response.message, response.type);
				}else {
					$('#paymentModal #payment_date').val(response.payment_date);
					$('#paymentModal #amount').val(response.amount);
					$('#paymentModal #payment_mood').val(response.payment_mood).trigger('chosen:updated');
					$('#paymentModal #memo').val(response.memo);
					$('#paymentModal input[name=payment_id]').val(response.id);
				}
			},
			complete:function() {
				$('#ajaxloader').addClass('hide');
				$('#paymentModal').modal();
			},
			error:function(xhr, ajaxOptions, thrownError) {
				alert(xhr.status +' '+ thrownError);
			}
		});
	}else {
		$('#ajaxloader').removeClass('hide');
		$.ajax({
			url:'{{ url('invoices/fetch-invoice') }}/'+{{ $invoice->id }},
			method:'POST',
			dataType:'JSON',
			success:function(response) {
				if(response.type == 'error') {
					toastMsg(response.message, response.type);
					return;
				}
				$('#paymentModal #payment_date').datepicker('update', new Date());
				$('#paymentModal #amount').val(response.amount_due);
				$('#paymentModal #payment_mood').val('bank-payment').trigger('chosen:updated');
				$('#paymentModal #memo').val('')
				$('#paymentModal input[name=payment_id]').val('');
			},
			complete:function() {
				$('#paymentModal').modal();
				$('#ajaxloader').addClass('hide');
			},
			error:function(xhr,ajaxOptions,thrownError) {
				alert(xhr.status + ' ' + thrownError);
			}
		});
	}
}

{{-- Record Payment --}}
function recordPayment(invoiceId)
{
	$('#ajaxloader').removeClass('hide');
	$.ajax({
		url:'{{ url('invoices/record-payment') }}/'+invoiceId,
		method:'POST',
		data:$('#invoice-payment-form').serialize(),
		success:function(response) {
			if(response.status == 400){
				//validation error
				$.each(response.error, function(index, value) {
					$("#ve-"+index).html('['+value+']');
				});
			}else {
				toastMsg(response.message,response.type);
				if(response.type == 'success') {
					
					setPaymentInfo(response)
					
					$('#paymentModal').modal('hide');
					
					setTimeout(function() {
						
						if(response.action == 'add') {
							$('#paymentSuccessModal .btn-send-receipt').attr('onclick', 'openSendReceipt('+response.payment_id+');')
							$('#paymentSuccessModal').modal('show');
						}
					}, 1000);
				}
			}
		},
		complete:function() {
			$('#ajaxloader').addClass('hide');
		},
		error:function(xhr, ajaxOptions, thrownError) {
			alert(xhr.status + ' ' + thrownError);
		}
	});
}

function setPaymentInfo(response) 
{
	$('.invoice-status-wrapper').html(response.invoice_status);
	$('#paymentModal #amount').val(response.amount_due);
	$('.amount-due').text('৳' + parseFloat(response.amount_due).toFixed(2));
	if(parseInt(response.amount_due) <= 0) {
		$('.panel-three').removeClass('panel-primary').addClass('panel-default');
		$('.panel-three .step-no').html('<i class="fa fa-check" aria-hidden="true"></i>');
	}else {
		$('.panel-three').removeClass('panel-default').addClass('panel-primary');
		$('.panel-three .step-no').html(3);
	}
	$('.due-time').text(response.due);
	$('.panel-three .col-md-11').html(response.get_paid_info);

	if($('.payment-row').length) {
		$('.payment-row').remove();
	}
	
	$(response.table_row_of_payment).insertBefore('#amount-due-row');
	$('input[type="checkbox"]').iCheck({
	     checkboxClass: 'icheckbox_minimal-blue',
	     increaseArea: '20%' // optional
	});
}

function openRemovePaymentModal(paymentId)
{
	var paymentAmount = $('#payment-item-'+paymentId).find('.btn-delete').attr('data-amount');
	$('#paymentDeleteModal input[name=payment_id]').val(paymentId);
	$('#paymentDeleteModal .payment-amount').text(paymentAmount);

	$('#paymentDeleteModal').modal('show');
}

function removePayment(invoiceId)
{
	$('#ajaxloader').removeClass('hide');
	$.ajax({
		url:'{{ url('invoices/remove-payment') }}/'+invoiceId,
		method:'POST',
		data:{ payment_id : $('#paymentDeleteModal input[name=payment_id]').val() },
		success:function(response) {
			toastMsg(response.message, response.type);
			if(response.type == 'success') {
				setPaymentInfo(response);
			}
		},
		error:function(xhr, ajaxOptions, thrownError) {
			alert(xhr.status + ' ' +thrownError);
		},
		complete:function() {
			setTimeout(function() {
				$('#paymentDeleteModal').modal('hide');
				$('#ajaxloader').addClass('hide');
			}, 1500);
		}
	});
}

function openSendReceipt(paymentId)
{
	$('#ajaxloader').removeClass('hide');
	$('#sentReceiptModal input[type=checkbox]').iCheck('uncheck');
	$('#sentReceiptModal input[name=ccs]').tagsinput('removeAll');
	$('#sent-receipt-form').trigger('reset');
	$.ajax({
		url:'{{ url('invoices/fetch-payment') }}/'+paymentId,
		method:'POST',
		success:function(response) {
			$('#paymentSuccessModal').modal('hide');
			$('#sentReceiptModal #to').val(response.customer_email);
			$('#sentReceiptModal #message').val(response.message);
			$('#sentReceiptModal input[name=payment_id]').val(response.id);

		},
		complete:function() {
			$('#sentReceiptModal').modal('show');
			$('#ajaxloader').addClass('hide');
		},
		error:function(xhr, ajaxOptions, thrownError) {
			alert(xhr.status + ' ' +thrownError);
		}
	});
}

function sendReceipt()
{
	$('#ajaxloader').removeClass('hide');
	$.ajax( {
		url:'{{ url('invoices/send-receipt') }}',
		method:'POST',
		data:$('#sent-receipt-form').serialize(),
		success:function(response) {
			if(response.status == 400){
				//validation error
				$.each(response.error, function(index, value) {
					$("#sentReceiptModal #ve-"+index).html('['+value+']');
				});
			}else {
				toastMsg(response.message,response.type);
				if(response.type == 'success') {
					$('#sentReceiptModal').modal('hide');
					$('#sentReceiptSuccessModal').modal('show');
				}
			}
		},
		complete:function() {
			$('#ajaxloader').addClass('hide');
		},
		error:function(xhr, ajaxOptions, thrownError) {
			alert(xhr.status + ' ' + thrownError);
		}
	});
}

function openRemindInvoiceModal(id) 
{
    $('#ajaxloader').removeClass('hide');
    $.ajax({
        url:'{{ url('invoices/fetch-invoice') }}/'+id,
        method:'POST',
        dataType:'JSON',
        success:function(response) {
            if(response.type == 'error') {
                toastMsg(response.message, response.type);
            }else {

                $('#invoiceRemindModal .mail-from').text(response.owner_email);
                $('#invoiceRemindModal .mail-to').text(response.customer_email);
                $('#invoiceRemindModal input[name=invoice_id]').val(response.id);
                $('#invoiceRemindModal .btn-preview').attr('href', '{{ url('invoice-reminder-preview') }}/'+response.identifier);
            }
        },
        complete:function() {
            $('#ajaxloader').addClass('hide');
            $('#invoiceRemindModal').modal();
        },
        error:function(xhr, ajaxOptions, thrownError) {
            alert(xhr.status + ' ' + thrownError);
        }
    });
}

function sendInvoiceReminder()
{
    $('#ajaxloader').removeClass('hide');
    
    $.ajax({
        url:'{{ url('invoices/send-reminder') }}',
        method:'POST',
        data:$('#invoice-reminder-form').serialize(),
        success:function(response) {
            toastMsg(response.message, response.type);

            if(response.type == 'success') {
                $('#invoiceRemindModal').modal('hide');

                setTimeout(function() {
                    $('#invoiceRemiderSuccessModal').modal('show');
                }, 1000);
            }
        },
        complete:function() {
            $('#ajaxloader').addClass('hide');
        },
        error:function(xhr, ajaxOptions, thrownError) {
            alert(xhr.status + ' ' + thrownError);
        }
    })
}

(function() {
	$('.customer-profile').popover({
		content:'{!! $customer_profile_html !!}',
		html:true,
		placement:"bottom",
	});

	$(".form-disabled :checkbox").bind("click", false);

	$('input[type="checkbox"]').iCheck({
	     checkboxClass: 'icheckbox_minimal-blue',
	     increaseArea: '20%' // optional
	});

	$('input[type="radio"]').iCheck({
	     radioClass: 'iradio_minimal-blue',
	     increaseArea: '20%' // optional
	});
})();
</script>
@endsection