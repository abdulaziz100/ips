<!-- The Invoice Remind Modal -->
<div class="modal fade" id="invoiceRemindModal">
  <div class="modal-dialog">
    <div class="modal-content">

      {!! Form::open(['url' => '', 'role' => 'form', 'id' => 'invoice-reminder-form']) !!}

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Send a Payment Reminder</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body text-center">
        <div class="mail-info text-left">
            <div>
              <span>From: </span><span class="mail-from"></span>
            </div>
            <div class="margin-top-20">
              <span>To: </span><span class="mail-to"></span>
            </div>
        </div>

        <hr>

        <span>Below is an example of the email reminder that your customer will receive:</span>

        <div class="mt-30">
            <img class="img-thumbnail img-fluid" src="{{ $assets }}/images/invoice-reminder-preview.png" alt="Invoice Preview">
        </div>
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        {!! Form::hidden('invoice_id', '') !!}
        <a href="" class="btn btn-success btn-preview" target="_blank"><i class="fa fa-eye"></i> Preview</a>
        <a href="javascript:sendInvoiceReminder();" class="btn btn-info"><i class="fa fa-send"></i> Send Reminder</a> 
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>
      
      {!! Form::close() !!}
    </div>
  </div>
</div>