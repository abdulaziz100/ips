{{-- The Invoice Remider Success Modal --}}
<div id="invoiceRemiderSuccessModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Send a Payment Reminder</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <div class="jumbotron text-center"><i class="fa fa-check-square-o fa-5x" aria-hidden="true"></i>
            <h3>Reminder has been sent.</h3>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
