{{-- Payment Modal --}}
<div id="paymentModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Record a Payment for this Invoice</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      {!! Form::open(['url' => '', 'role' => 'form', 'id' => 'invoice-payment-form']) !!}
      <div class="modal-body">
        <p>Record a payment you’ve already received, such as cash, cheque, or bank payment.</p>
        <div class="form-group has-feedback">
            <label for="payment_date">Payment Date: {!! validation_error($errors->first('payment_date'),'payment_date') !!}</label>
            {!! Form::text('payment_date',date('Y-m-d'),['class'=>'form-control datepicker','placeholder'=>'YYYY-MM-DD', 'id' => 'payment_date']) !!} 
            <span class="glyphicon glyphicon-calendar form-control-feedback"></span>
        </div>
        <div class="form-group">
            <label for="amount">Amount: {!! validation_error($errors->first('amount'),'amount') !!}</label>
            @if(!empty($invoice))
                {!! Form::text('amount',number_format($invoice->getAmountDue(),2),['class'=>'form-control','placeholder'=>'Amount', 'id' => 'amount', 'step' => 0.01,'min' => 0]) !!}
            @else
                {!! Form::text('amount',0,['class'=>'form-control','placeholder'=>'Amount', 'id' => 'amount', 'step' => 0.01,'min' => 0]) !!}
            @endif
        </div>
        <div class="form-group">
            <label for="payment_mood">Payment Method: {!! validation_error($errors->first('payment_mood'),'payment_mood') !!}</label>
            {!! Form::select('payment_mood',config('constants.payment_mood'),null, ['class'=>'form-control chosen-select', 'id' => 'payment_mood']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('memo', 'Memo/Notes..') !!}
            {!! Form::textarea('memo', null, ['class'=>'form-control', 'palceholder' => 'Memo...', 'size' => '30x3']) !!}
        </div>
      </div>
      <div class="modal-footer">
        {!! Form::hidden('payment_id', '') !!}
        @if(!empty($invoice))
            <button type="button" class="btn btn-primary btn-submit" onclick="recordPayment({{ $invoice->id }});"><i class="fa fa-save" aria-hidden="true"></i> Submit</button>
        @else
            <button type="button" class="btn btn-primary btn-submit" onclick="recordPayment('');"><i class="fa fa-save" aria-hidden="true"></i> Submit</button>
        @endif
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
      {!! Form::close() !!}
    </div>

  </div>
</div>
