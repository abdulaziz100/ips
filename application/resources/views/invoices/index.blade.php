@extends('layouts.master')

@section('title') List of Invoices @endsection

@section('content')
<div class="row">
	<div class="col-sm-12">
		<div class="card m-b-30">
			@include('invoices.invoice_card_content')
		</div>
	</div>
</div>

@include('invoices.invoice_email_modal')
@include('invoices.invoice_email_success_modal')
@include('invoices.invoice_remind_modal')
@include('invoices.invoice_remind_success_modal')
@include('invoices.payment_modal')
@include('invoices.payment_success_modal')
@include('invoices.send_receipt_modal')
@include('invoices.send_receipt_success_modal')
@include('invoices.invoice_remind_modal')
@include('invoices.invoice_remind_success_modal')
	
@endsection

@section('custom-style')
{{-- Daterange Picker --}}
{!! Html::style($assets . '/plugins/daterangepicker/daterangepicker-bs3.css') !!}
{{-- Bootstrap Tags Input --}}
{!! Html::style($assets. '/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css') !!}
{{-- iCheck --}}
{!! Html::style($assets . '/plugins/icheck/skins/minimal/blue.css') !!}
@endsection

@section('custom-script')
{{-- Date rangepicker --}}
{!! Html::script($assets . '/plugins/daterangepicker/moment.min.js') !!}
{!! Html::script($assets . '/plugins/daterangepicker/daterangepicker.js') !!}
{{-- Bootstrap Tags Input --}}
{!! Html::script($assets. '/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js') !!}
{{-- iCheck --}}
{!! Html::script($assets . '/plugins/icheck/icheck.min.js') !!}

<script>

var url = '{{ request()->fullUrl() }}';

function getInvoices(url) {
    $.ajax({
        url : url,
    }).done(function (data) {
        $('.card').html(data);
        $('.chosen-select').chosen();
        @if(!empty($from_date) && !empty($to_date))
		var start = moment('{{ $from_date }}');
		var end = moment('{{ $to_date }}');
		@else
		var start = '';
	    var end = '';
	    @endif

	    function cb(start, end) {

	    	if(start && end) {
		        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
		        $('input[name=date_range]').val(start.format('YYYY-MM-DD')+ ' - ' + end.format('YYYY-MM-DD'));
	        }else {
	        	$('#reportrange span').html('Select invoice date range');
		        $('input[name=date_range]').val('');
	        }
	    }

	    $('#reportrange').daterangepicker({
	        ranges: {
	           'Today': [moment(), moment()],
	           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
	           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
	           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
	           'This Month': [moment().startOf('month'), moment().endOf('month')],
	           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
	        }
	    }, cb);

		cb(start, end);

		$('#reportrange').on('apply.daterangepicker', function(ev, picker) {
			$('#invoice-filter-form').submit();
		});
    }).fail(function () {
        alert('Invoices could not be loaded.');
    });
}

{{-- To Approve Invoice --}}
function approveInvoice(invoiceId)
{
	$('#ajaxloader').removeClass('hide');
	$.ajax({
		url:'{{ url('invoices/approve') }}/'+invoiceId,
		method:'POST',
		success:function(response) {
			toastMsg(response.message, response.type);
			if(response.type == 'success') {
				setTimeout(getInvoices(url), 1500);
			}
		},
		complete:function() {
			$('#ajaxloader').addClass('hide');
		},
		error:function(xhr, ajaxOptions, thrownError) {
			alert(xhr.status + ' ' + thrownError);
		}
	});
}

{{-- Open Invoice Email Modal --}}
function openInvoiceEmailModal(invoiceId)
{
	$('#invoiceEmailModal input[name=from]').val('');
	$('#invoiceEmailModal input[name=to]').val('');
	$('#ajaxloader').removeClass('hide');
	$.ajax({
		url:'{{ url('invoices/fetch-invoice/') }}/'+invoiceId,
		method:'POST',
		dataType:'JSON',
		success:function(response) {
			if(response.type == 'error') {
				toastMsg(response.message, response.type);
			}else {
				$('#invoiceEmailModal input[name=from]').val(response.owner_email);
				$('#invoiceEmailModal input[name=to]').val(response.customer_email);

				var emailSubject = 'Invoice #' + response.identifier +' from {{ Auth::user()->companies()->first()->name }}';
				$('#invoiceEmailModal .email-subject-name').text(emailSubject);
				$('#invoiceEmailModal input[name=subject]').val(emailSubject);
				$('#invoiceEmailModal textarea[name=message]').attr('placeholder', 'Enter your message to '+response.customer_name);
				$('#invoiceEmailModal .btn-send').attr('onclick', 'sendEmail('+invoiceId+');');
			}
		},
		complete:function() {
			$('#ajaxloader').addClass('hide');
			$('input[type=checkbox]').iCheck('uncheck');
			$('#invoiceEmailModal input[name=ccs]').tagsinput('removeAll');
			$('#invoiceEmailModal').modal();
		},
		error:function(xhr, ajaxOptions, thrownError) {
			alert(xhr.status + ' ' + thrownError);
		}
	});
}

function openCC()
{
	$('.cc-row').toggleClass('hide');
}

function openEmailSubject()
{
	$('.email-subject').addClass('hide');
	$('#subject').removeClass('hide');
}

function sendEmail(invoiceId)
{
	$('#ajaxloader').removeClass('hide');
	$.ajax({
		url:'{{ url('invoices/send-email') }}/'+invoiceId,
		method:'POST',
		data:$('#invoice-email-form').serialize(),
		success:function(response) {
			if(response.status == 400){
				//validation error
				$.each(response.error, function(index, value) {
					$("#ve-"+index).html('['+value+']');
				});
			}else {
				toastMsg(response.message,response.type);
				if(response.type == 'success') {
					$('#invoiceEmailModal').modal('hide');

					setTimeout(function() {
						$('#invoiceEmailSuccessModal').modal('show');
						getInvoices(url);
						window.history.pushState("", "", url);
					}, 1000);
				}
			}
		},
		complete:function() {
			$('#invoice-email-form').trigger('reset');
			$('#ajaxloader').addClass('hide');
		},
		error:function(xhr, ajaxOptions, thrownError) {
			alert(xhr.status + ' ' +thrownError);
		}
	});
}

{{-- Open payment modal --}}
function openPaymentModal(invoiceId)
{
	$('#ve-amount,#ve-payment_date').text('*');
	$('#ajaxloader').removeClass('hide');
	$.ajax({
		url:'{{ url('invoices/fetch-invoice') }}/'+invoiceId,
		method:'POST',
		dataType:'JSON',
		success:function(response) {
			if(response.type == 'error') {
				toastMsg(response.message, response.type);
				return;
			}
			$('#paymentModal #payment_date').datepicker('update', new Date());
			$('#paymentModal #amount').val(response.amount_due);
			$('#paymentModal #payment_mood').val('bank-payment').trigger('chosen:updated');
			$('#paymentModal #memo').val('')
			$('#paymentModal input[name=payment_id]').val('');
			$('#paymentModal .btn-submit').attr('onclick', 'recordPayment('+invoiceId+');');
		},
		complete:function() {
			$('#paymentModal').modal();
			$('#ajaxloader').addClass('hide');
		},
		error:function(xhr,ajaxOptions,thrownError) {
			alert(xhr.status + ' ' + thrownError);
		}
	});
	
}

{{-- Record Payment --}}
function recordPayment(invoiceId)
{
	$('#ajaxloader').removeClass('hide');
	$.ajax({
		url:'{{ url('invoices/record-payment') }}/'+invoiceId,
		method:'POST',
		data:$('#invoice-payment-form').serialize(),
		success:function(response) {
			if(response.status == 400){
				//validation error
				$.each(response.error, function(index, value) {
					$("#ve-"+index).html('['+value+']');
				});
			}else {
				toastMsg(response.message,response.type);
				if(response.type == 'success') {
					
					$('#paymentModal').modal('hide');
					
					setTimeout(function() {
						getInvoices(url);
						window.history.pushState("", "", url);
						$('#paymentSuccessModal .btn-send-receipt').attr('onclick', 'openSendReceipt('+response.payment_id+');');
						$('#paymentSuccessModal').modal('show');
					}, 1000);
				}
			}
		},
		complete:function() {
			$('#ajaxloader').addClass('hide');
		},
		error:function(xhr, ajaxOptions, thrownError) {
			alert(xhr.status + ' ' + thrownError);
		}
	});
}

function openSendReceipt(paymentId)
{
	$('#ajaxloader').removeClass('hide');
	$('#sentReceiptModal input[type=checkbox]').iCheck('uncheck');
	$('#sentReceiptModal input[name=ccs]').tagsinput('removeAll');
	$('#sent-receipt-form').trigger('reset');
	$.ajax({
		url:'{{ url('invoices/fetch-payment') }}/'+paymentId,
		method:'POST',
		success:function(response) {
			$('#paymentSuccessModal').modal('hide');
			$('#sentReceiptModal #to').val(response.customer_email);
			$('#sentReceiptModal #email-subject').text('Payment Receipt for Invoice #' + response.invoice_identifier);
			$('#sentReceiptModal #message').val(response.message);
			$('#sentReceiptModal input[name=payment_id]').val(response.id);

		},
		complete:function() {
			$('#sentReceiptModal').modal('show');
			$('#ajaxloader').addClass('hide');
		},
		error:function(xhr, ajaxOptions, thrownError) {
			alert(xhr.status + ' ' +thrownError);
		}
	});
}

function sendReceipt()
{
	$('#ajaxloader').removeClass('hide');
	$.ajax( {
		url:'{{ url('invoices/send-receipt') }}',
		method:'POST',
		data:$('#sent-receipt-form').serialize(),
		success:function(response) {
			if(response.status == 400){
				//validation error
				$.each(response.error, function(index, value) {
					$("#sentReceiptModal #ve-"+index).html('['+value+']');
				});
			}else {
				toastMsg(response.message,response.type);
				if(response.type == 'success') {
					$('#sentReceiptModal').modal('hide');

					setTimeout(function() {
						getInvoices(url);
						$('#sentReceiptSuccessModal').modal('show');
					}, 1000);
					
				}
			}
		},
		complete:function() {
			$('#ajaxloader').addClass('hide');
		},
		error:function(xhr, ajaxOptions, thrownError) {
			alert(xhr.status + ' ' + thrownError);
		}
	});
}

function openRemindInvoiceModal(id) 
{
    $('#ajaxloader').removeClass('hide');
    $.ajax({
        url:'{{ url('invoices/fetch-invoice') }}/'+id,
        method:'POST',
        dataType:'JSON',
        success:function(response) {
            if(response.type == 'error') {
                toastMsg(response.message, response.type);
            }else {

                $('#invoiceRemindModal .mail-from').text(response.owner_email);
                $('#invoiceRemindModal .mail-to').text(response.customer_email);
                $('#invoiceRemindModal input[name=invoice_id]').val(response.id);
                $('#invoiceRemindModal .btn-preview').attr('href', '{{ url('invoice-reminder-preview') }}/'+response.identifier);
            }
        },
        complete:function() {
            $('#ajaxloader').addClass('hide');
            $('#invoiceRemindModal').modal();
        },
        error:function(xhr, ajaxOptions, thrownError) {
            alert(xhr.status + ' ' + thrownError);
        }
    });
}

function sendInvoiceReminder()
{
    $('#ajaxloader').removeClass('hide');
    
    $.ajax({
        url:'{{ url('invoices/send-reminder') }}',
        method:'POST',
        data:$('#invoice-reminder-form').serialize(),
        success:function(response) {
            toastMsg(response.message, response.type);

            if(response.type == 'success') {
                $('#invoiceRemindModal').modal('hide');

                setTimeout(function() {
                    $('#invoiceRemiderSuccessModal').modal('show');
                }, 1000);
            }
        },
        complete:function() {
            $('#ajaxloader').addClass('hide');
        },
        error:function(xhr, ajaxOptions, thrownError) {
            alert(xhr.status + ' ' + thrownError);
        }
    })
}



(function() {

	@if(!empty($from_date) && !empty($to_date))
	var start = moment('{{ $from_date }}');
	var end = moment('{{ $to_date }}');
	@else
	var start = '';
    var end = '';
    @endif

    function cb(start, end) {

    	if(start && end) {
	        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
	        $('input[name=date_range]').val(start.format('YYYY-MM-DD')+ ' - ' + end.format('YYYY-MM-DD'));
        }else {
        	$('#reportrange span').html('Select invoice date range');
	        $('input[name=date_range]').val('');
        }
    }

    $('#reportrange').daterangepicker({
        ranges: {
           'Today': [moment(), moment()],
           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
           'This Month': [moment().startOf('month'), moment().endOf('month')],
           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    }, cb);

	cb(start, end);

	$('#reportrange').on('apply.daterangepicker', function(ev, picker) {
		$('#invoice-filter-form').submit();
	});

	$('input[type="checkbox"]').iCheck({
	     checkboxClass: 'icheckbox_minimal-blue',
	     increaseArea: '20%' // optional
	});
})();
</script>
@endsection