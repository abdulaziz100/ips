<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>
      @hasSection ('title')
          @yield('title') - {{env('APP_NAME')}}
      @else
          {{ env('APP_NAME') }}
      @endif
    </title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <link rel="stylesheet" href="{{$assets}}/css/bootstrap.min.css" media="screen">
    <link href="{{ $assets }}/css/icons.css" rel="stylesheet" type="text/css">
    <link href="{{ $assets }}/css/style.css" rel="stylesheet" type="text/css">
    <link href="{{ $assets }}/css/custom.css" rel="stylesheet" type="text/css">

    @yield('custom-style')
  </head>
  <body>
        
    @yield('header')
       
    @yield('content')

    <footer class="margin-top-20 text-center">
        <h4>Powered by <strong>{{ env('APP_NAME') }}</strong></h4>
        <p>Get paid, track expenses, and manage your money with {{ env('APP_NAME') }}.</p>
    </footer>
    <script src="{{ $assets }}/js/jquery.min.js"></script>
    <script src="{{ $assets }}/js/bootstrap.min.js"></script>

    <!-- Theme's Custom Script-->

    @yield('custom-script')

  </body>
</html>
 