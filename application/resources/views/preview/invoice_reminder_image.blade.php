@extends('preview.master')

@section('title') Reminder Preview @endsection

@section('content')

<div style="border:1px solid #dfdfdf;background-color:#fff;border-radius:5px;margin:100px auto;max-width:460px;text-align:center;position:relative">
    <div class="preview">PREVIEW</div>
    <div style="padding:15px">
        <div style="margin-top:20px;text-align:center;">
            <h2 style="font-style:italic;color:#00BD1A;margin:0">Your Logo</h2>
            <span style="color:#727D82;">GOES HERE</span>
        </div>

        <div style="margin-top:20px;text-align:center;">
        	<h1 style="font-weight:normal">Reminder for <br> <span style="font-weight:bold">Invoice #{{ strtoupper($invoice->identifier) }}</span></h1>
			<span style="color:#707070">due on {{ \Carbon::parse($invoice->payment_due)->format('F d, Y') }}</span>

            <hr style="margin-top:20px;">

			<div style="margin-top:20px;">
                <p>{{ $invoice->customer_profile->name }}, <br> <br>
                Just a friendly reminder, You have received an invoice that is due on {{ \Carbon::parse($invoice->payment_due)->format('F d, Y') }}, and have not completed payment. <br>
                The invoice was issued on {{ \Carbon::parse($invoice->invoice_date)->format('F d, Y') }}. <br>
                Thanks, <br>
                Your Company Inc
            </div>

            <div style="border-top:1px solid #dfdfdf;border-bottom:1px solid #dfdfdf;margin:20px;padding:30px;text-align:center">
                <h4 style="font-weight:normal;line-height:30px;">
                    Amount Due: <strong>৳{{ number_format(round($invoice->total,2),2) }} BDT</strong> <br>
                    Due: <strong>{{ \Carbon::parse($invoice->payment_due)->format('F d, Y') }}</strong> 
                </h4>
            </div>

            <div style="padding:30px; text-align:center">
                <span><img src="{{ $assets }}/images/unpaid-icon.png" alt="Unpaid Icon"></span> <br>
                <a href="{{ url('invoice-details/'.$invoice->identifier) }}" style="background-color: #0097a7;border: 1px solid #0097a7;color:#fff;display: inline-block;font-weight: 400;border: 1px solid transparent;padding: .375rem .75rem;font-size: 1rem;line-height: 1.5;border-radius: .25rem;transition: color .15s ease-in-out,background-color .15s ease-in-out,border-color .15s ease-in-out,box-shadow .15s ease-in-out;margin-top:40px;">View &amp; Pay Invoice</a>
            </div>
        </div>
    </div>

    <div style="background-color: #F4F5F5;color:#727D82;margin-top:20px;padding:30px">
        Thanks for your business. If this invoice was sent in error, please contact 
        <a href="mailto:{{ $companyOwner->email }}">you@yourcompany.com</a>
    </div>
</div>
@endsection