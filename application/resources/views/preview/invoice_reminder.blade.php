@extends('preview.master')

@section('title') Reminder Preview @endsection

@section('header')
<header>
    <nav class="navbar bg-info fixed-top">
        <div class="container-fluid">
            <div class="row w-100">
                <div class="col-sm-8 text-white">
                    <div style="margin-top:5px;">This is a preview of the email for the payment reminder that your customer will see.</div>
                </div>
                <div class="col-sm-4">
                    <a href="javascript:closeTab();" class="btn btn-outline-dark btn-sm float-right text-white"><i class="fa fa-times-circle-o" aria-hidden="true"></i> Close this tab</a>
                </div>
            </div>
        </div>
    </nav>
</header>  
@endsection

@section('content')
    @include('preview.invoice_reminder_content')
@endsection

@section('custom-script')
<script>
    function closeTab() {
        open(location, '_self').close();
    }
</script>
@endsection