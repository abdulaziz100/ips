@extends('preview.master')

@section('title') Payment #{{ $payment->id }} from {{ $company->name }} @endsection

@section('header')
@if(Auth::check())
<header>
    <nav class="navbar bg-secondary fixed-top">
        <div class="container-fluid">
            <div class="row w-100">
                <div class="col-sm-8 text-white">
                    <h4>You are previewing your customer's receipt for payment on invoice {{ $invoice->indentifier }}</h4>
                </div>
                <div class="col-sm-4">
                    <a href="{{ url('invoice-details/'.$invoice->identifier) }}" class="btn btn-default btn-sm float-right"><i class="fa fa-reply" aria-hidden="true"></i> Go to {{ env('APP_NAME') }}</a>
                </div>
            </div>
        </div>
    </nav>
</header>
@else
<header style="margin:0 auto 20px;max-width:460px" class="padding-10">
    <a href="{{ url('invoice-details/'.$invoice->identifier) }}" class="btn btn-default btn-xs float-right"><i class="fa fa-reply"></i> Back to invoice</a>
</header>
@endif  
@endsection

@section('content')
<div style="margin:80px auto 20px;max-width:460px" class="card text-center">
    <div class="card-body">
        <div class="invoice-logo-holder text-center margin-top-20">
            <img src="{{ getCompanyLogoPath($company->id) }}" alt="Company Logo" width="180">
        </div>

        <div class="margin-top-20 text-center">
        	<h2 class="margin-top-0 bold">Payment Receipt</h2>
            <h5 class="bold">Invoice #{{ $invoice->identifier }}</h5>
			<span>for {{ $invoice->customer_profile->name }}</span>

			<div class="invoice-contact-location margin-top-20">
                <strong>{{ $company->name }}</strong> <br>
                {{ $company->address }} <br>
                {{ $company->state }},{{ $company->city }} {{ $company->zip_code }} <br>
                {{ $company->country_obj->name }}
            </div>

            <div class="invoice-contact-phone margin-top-20">
                <span>Phone: </span>{{ $company->primary_phone }}
                <br>
                <span>Alt Phone: </span>{{ $company->alt_phone }}
                <br>
                <a href="{{ str_contains($company->website, 'http://') ? $company->website : 'http://'.$company->website }}">{{ $company->website }}</a>
            </div>
        </div>

        <hr>
        
        @if(!empty($payment->memo))
            <div class="margin-top-20 padding-10 text-justify">{{ $payment->memo }}</div>
            <hr>
        @endif

        <div style="padding:0 10px 10px;margin-bottom:40px">
            <h4 class="bold">Payment Amount: ৳{{ number_format($payment->amount,2) }} BDT</h4>
            <hr>
            <h6><strong>PAYMENT METHOD:</strong> {{ strtoupper(config('constants.payment_mood.'.$payment->payment_mood)) }}</h6>
        </div>

        <div class="invoice-contact-email well well-sm margin-top-20">
            Thanks for your business. If this invoice was sent in error, please contact 
            <a href="mailto:{{ $owner->email }}">{{ $owner->email }}</a>
        </div>
    </div>
</div>
@endsection