@extends('preview.master')

@section('title') Invoice #{{ strtoupper($invoice->identifier) }} @endsection

@section('header')

@if(Auth::check())
<header>
	<nav class="navbar bg-secondary fixed-top">
	  	<div class="container-fluid">
		    <div class="row w-100">
		    	<div class="col-sm-6 text-white">
		    		<h4 class="bold">Preview Mode</h4>
		    		<p>You are previewing how your customer will see the web version of your invoice.</p>
		    	</div>
		    	<div class="col-sm-6">
		    		<a href="{{ url('invoices/list') }}" class="btn btn-default btn-sm margin-top-20 float-right"><i class="fa fa-reply" aria-hidden="true"></i> Go back to {{ env('APP_NAME') }}</a>
		    	</div>
		    </div>
	  	</div>
	</nav>
</header>
@endif

@endsection

@section('content')

<div class="container-fluid text-center{{ Auth::check() ? ' margin-top-100' :'' }}">
	<h1>Request for Payment from {{ $company->name }} </h1>
	<h4 class="bg-secondary padding-10 margin-top-20 text-white">{{ $invoice->title }} {{ strtoupper($invoice->identifier) }} * Amount Due: ৳{{ number_format($invoice->getAmountDue(),2) }} * Due On: {{ Carbon::parse($invoice->payment_due)->format('F d, Y') }}
	</h4>
</div>

<div class="container">
	<div class="row">
		<div class="col-md-10 offset-md-1">
			<div class="card panel-invoice">
				<div class="card-header">
					<div class="card-title">
						<a href="{{ Request::url().'?view=print' }}" class="btn btn-primary" target="_blank"><i class="fa fa-print"></i> Print </a>
						<a href="{{ Request::url().'?view=pdf' }}" class="btn btn-primary" target="_blank"><i class="fa fa-file-pdf-o"></i> Pdf </a>
					</div>
				</div>
				
				<div class="card-body">
					@include('invoices.invoice_panel')
				</div><!-- /.card-body -->
			</div>
		</div>
	</div>
</div>
	
@endsection

@section('custom-style')

@endsection

@section('custom-script')

@endsection