@extends('prints.master')

@section('title') {{ $company->name }} - {{ $invoice->title }} - #{{ strtoupper($invoice->identifier) }} @endsection

@section('content') 
	@include('invoices.invoice_panel')
@endsection

@section('custom-style')
	<style>
		.panel {
			border:0;
		}
		.card-body{
			padding: 5px;
		}
	</style>
@endsection