@extends('prints.master')

@section('title') Customerwise Income Report @endsection 

@section('content')
    <div class="card margin-top-20">
    <div class="card-header">
        <div class="card-title">Customerwise Income Report</div>
    </div>
    <div class="card-body">

        @if(!empty($from_date) && !empty($to_date))
            <h3><strong>Customerwise Income Report from {{ $from_date }} to {{ $to_date }}</strong></h3>
        @endif

        <div class="table-responsive">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Email</th>
                        <th width="13%">Member Since</th>
                        <th width="4%" class="text-center"><span data-toggle="tooltip" title="Total Number of Invoices">T.N.I</span></th>
                        <th width="10%" class="text-right"><span data-toggle="tooltip" title="Total Invoice Amount">T.I.A</span></th>
                        <th width="10%" class="text-right"><span data-toggle="tooltip" title="Total Invoice Paid Amount">T.I.P.A</span></th>
                        <th width="10%" class="text-right"><span data-toggle="tooltip" title="Total Invoice Due Amount">T.I.D.A</span></th>
                    </tr>
                </thead>
                <tbody>  
                @forelse($invoices as $invoice)
                    <tr>
                        <td>{{ $invoice->name }}</td>
                        <td>{{ $invoice->email }}</td>
                        <td class="text-center">{{ Carbon::parse($invoice->created_at)->format('d M, Y') }}</td>
                        <td class="text-center">{{ $invoice->total_invoice }}</td>
                        <td class="text-right">{{ number_format(round($invoice->total_amount, 2),2) }}</td>
                        <td class="text-right">{{number_format(round($invoice->total_paid_amount, 2),2) }}</td>
                        <td class="text-right">{{number_format(round(($invoice->total_amount - $invoice->total_paid_amount), 2),2) }}</td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="5" align="center">No Data Found!</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection

@section('custom-style')
<style>
    .container {
        max-width: 973px;
    }
    .table {
        margin: 0;
    }
    table td {
        border-top: 1px solid transparent !important;
    }
</style>
@endsection
 