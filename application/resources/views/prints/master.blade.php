<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>
      @hasSection ('title')
          {{env('APP_NAME')}} - @yield('title')
      @else
          {{ env('APP_NAME') }}
      @endif
    </title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <link media="all" rel="stylesheet" href="{{ url('assets') }}/css/bootstrap.min.css" >
    <link media="all" rel="stylesheet" href="{{ url('assets') }}/plugins/font-awesome/css/font-awesome.min.css">
    <link media="all" rel="stylesheet" href="{{ url('assets') }}/css/custom.css">

    @yield('custom-style')

  </head>
  <body>
    <div class="container">
      @yield('content')
    </div>

    <script>
    (function() {
      window.print();
    })();
    </script>

  </body>
</html>
