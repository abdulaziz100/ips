@extends('prints.master')

@section('title') Invoice Report @endsection 

@section('content')
<div class="card margin-top-20">
    <div class="card-header">
        <div class="card-title">Invoice Report</div>
    </div>
    <div class="card-body">
        @if(!empty($from_date) && !empty($to_date))
        <h3><strong>Invoice Report from {{ $from_date }} to {{ $to_date }}</strong></h3>
        @endif
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th width="17%">Date</th>
                    <th width="14%">Invoice ID</th>
                    <th>Customer</th>
                    <th width="9%" align="right">Amount</th>
                    <th width="6%">Status</th>
                </tr>
            </thead>
            <tbody>  
            @forelse($invoices as $invoice)
                <tr>
                    <td>{{ Carbon::parse($invoice->invoice_date)->format('d M, Y') }}</td>
                    <td>{{ strtoupper($invoice->identifier) }}</td>
                    <td>{{ $invoice->customer_profile->name }}</td>
                    <td>{{ number_format($invoice->total,2) }}</td>
                    <td>{{ ucfirst($invoice->status) }}</td>
                </tr>
            @empty
                <tr>
                    <td colspan="7" align="center">No Data Found!</td>
                </tr>
            @endforelse
            </tbody>
        </table>
    </div>
</div>
@endsection

@section('custom-style')
<style>
    .container {
        max-width: 973px;
    }
    .table {
        margin: 0;
    }
    table td {
        border-top: 1px solid transparent !important;
    }
</style>
@endsection
 