@extends('prints.master')

@section('title') Popular Services @endsection

@section('content') 
<div class="card margin-top-20">
    <div class="card-header">
        <div class="card-title">Report of Poplular Services</div>
    </div>
    <div class="card-body">
		<div class="table-responsive">
		    <table class="table table-bordered">
		        <thead>
		            <tr>
		                <th>Service Name</th>
		                <th width="18%" class="text-right">Service Price</th>
		                <th width="18%" class="text-right">Number of Order</th>
		                <th width="32%" class="text-right">Customer Order Amount</th>
		            </tr>
		        </thead>
		        <tbody>  
		        @forelse($services as $service)
		            <tr>
		                <td>{{ $service->title }}</td>
		                <td class="text-right">{{ number_format($service->promotional_price,2) }}</td>
		                <td class="text-right">{{ $service->total_products }}</td>
		                <td class="text-right">{{ number_format(($service->total_products * $service->promotional_price), 2) }}</td>
		            </tr>
		        @empty
		            <tr>
		                <td colspan="3" align="center">No Data Found!</td>
		            </tr>
		        @endforelse
		        </tbody>
		    </table>
		</div>	
	</div>
</div>
@endsection

@section('custom-style')
	<style>
		.panel {
			border:0;
		}
		.card-body{
			padding: 5px;
		}
	</style>
@endsection