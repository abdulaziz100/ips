@extends('layouts.master')

@section('title') List of Recurring Invoices @endsection

@section('content')
<div class="row">
	<div class="col-sm-12">
		<div class="card m-b-30">
			<div class="card-header">
				<h4 class="card-title">List of Recurring Invoices
					<a href="{{ url('recurring-invoices/create')}}" class="btn btn-danger btn-xs pull-right"><i class="fa fa-plus-circle"></i> Add New</a>
				</h4>
			</div>
			
			<div class="card-body">

				{!! Form::open(['url' => Request::url(), 'role'=>'form', 'id' => 'invoice-filter-form']) !!}
					<div class="row">
						<div class="col-lg-3">
							<div class="form-group">
                                {!! Form::select('customer_profile_id',$customer_profiles, Request::input('customer_profile_id') ? Request::input('customer_profile_id') :'',['class'=>'form-control chosen-select','data-placeholder'=>'Select a customer', 'onchange' => 'this.form.submit();']) !!}
                            </div>
						</div>
						<div class="col-lg-3">
							<div class="form-group">
                                {!! Form::select('status',$recurring_invoice_statuses, Request::input('status') ? Request::input('status') :'',['class'=>'form-control chosen-select', 'onchange' => 'this.form.submit();']) !!}
                            </div>
						</div>

						<div class="col-lg-1">
							<a href="{{ url()->current() }}" class="btn btn-default pull-right" data-toggle="tooltip" title="Refresh"><i class="fa fa-refresh"></i></a>
						</div>
					</div>
				{!! Form::close() !!}

				<div class="table-responsive">
					<table class="table table-responsive table-bordered training-table">
						<thead>
							<th width="7%">Status</th>
							<th>Customer</th>
							<th width="25%">Schedule</th>
							<th width="7%">Previous Invoice</th>
							<th width="7%">Next Invoice</th>
							<th width="12%" class="text-right">Invoice Amount</th>
							<th width="6%" class="text-center">Action</th>
						</thead>
						<tbody>
						@forelse($invoices as $invoice)
							<tr>
								<td><span class="label label-{{ config('constants.invoice_status_class.'.$invoice->status) }}"> {{ ucfirst($invoice->status) }}</td>
								<td>{{ $invoice->customer_profile->name }}</td>
								<td>{!! $invoice->getScheduleInfo() !!}</td>
								<td>{{ $invoice->getPreviousInvoiceDate() }}</td>
								<td>{{ $invoice->getNextInvoiceDate() }}</td>
								<td class="text-right">৳{{ number_format($invoice->total, 2) }}</td>
								<td class="text-center">
									<button class="btn btn-secondary btn-xs dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">

									</button>
									<div class="dropdown-menu">
										{{-- View --}}
										<a class="dropdown-item" href="{{url('recurring-invoices/'.$invoice->id)}}" title="View Invoice"><i class="fa fa-eye"></i> View</a>
										{{-- Edit --}}
										<a class="dropdown-item" href="{{url('recurring-invoices/'.$invoice->id.'/edit')}}" title="Edit this information"><i class="fa fa-pencil"></i> Edit</a>

										@if(in_array($invoice->status, ['active', 'end']))
									  		<a class="dropdown-item" href="{{ url('invoices/list') }}?recurring_invoice_id={{ $invoice->id }}"><i class="fa fa-eye-slash"></i> View Created Invoices</a>
									  	@else
											<a class="dropdown-item disabled" href="#/"><i class="fa fa-eye-slash"></i> View Created Invoices</a>
									  	@endif

										<div class="dropdown-divider"></div>
										
										@if($invoice->status == 'active')
								    		<a class="dropdown-item alert-dialog" href="#" data-id="{{$invoice->id}}" data-action="{{ url('recurring-invoices/terminate') }}" data-message="Are you sure, You want to terminate this invoice?" title="Terminate Invoice"><i class="fa fa-ban"></i> End</a>
								    	@else
											<a class="dropdown-item disabled" href="#"><i class="fa fa-ban"></i> End</a>
								    	@endif
								    	{{-- Delete --}}
										<a class="dropdown-item alert-dialog" href="#" data-id="{{$invoice->id}}" data-action="{{ url('recurring-invoices/delete') }}" data-message="Are you sure, You want to delete this invoice?" title="Delete invoice btn-danger"><i class="fa fa-trash white"></i> Delete</a>
									</div>
								</td>
							</tr>
						@empty
							<tr>
								<td colspan="7" align="center">No Records Found</td>
							</tr>
						@endforelse
						</tbody>
					</table>
				</div>
			</div><!-- /.card-body -->

			@if($invoices->total() > 10)
			<div class="card-footer">
				<div class="row">
					<div class="col-md-4">
						{{ $invoices->paginationSummery }}
					</div>
					<div class="col-md-8 text-right">
						<div class="float-right">
							{!! $invoices->links() !!}
						</div>
					</div>
				</div>
			</div>
			@endif
		</div>
	</div>
</div>
	
@endsection

@section('custom-style')

@endsection

@section('custom-script')

@endsection