{{-- Customer Modal --}}
<div id="customerModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">New Customer</h4>
      </div>
      {!! Form::open(['url' => '', 'role' => 'form', 'id' =>'customer-form']) !!}
      <div class="modal-body">
        <ul class="nav nav-tabs">
          <li class="active"><a data-toggle="tab" href="#basic-info">Basic Info</a></li>
          <li><a data-toggle="tab" href="#billini-info">Billing Info</a></li>
        </ul>

        <div class="tab-content">
          <div id="basic-info" class="tab-pane fade in active">
            @include('customer_profiles.basic_info')
          </div>
          <div id="billini-info" class="tab-pane fade">
            @include('customer_profiles.billing_info')
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" onclick="saveCustomer();"><i class="fa fa-save"></i> Save</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
      {!! Form::close() !!}
    </div>

  </div>
</div>
{{-- Customer Modal --}}