@extends('layouts.master')

@section('title') Invoice #{{ strtoupper($invoice->identifier) }} @endsection

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="card m-b-30 panel-invoice">
			<div class="card-header">
				<h4 class="card-title">
					Invoice #{{ strtoupper($invoice->identifier) }}
					
					<div class="pull-right">
						<div class="btn-group">
						  <button class="btn btn-secondary btn-lg dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						    More Action
						  </button>
						  <div class="dropdown-menu">
						  	@if(in_array($invoice->status, ['active', 'end']))
						  		<a class="dropdown-item" href="{{ url('invoices/list') }}?recurring_invoice_id={{ $invoice->id }}"><i class="fa fa-eye"></i> View Created Invoices</a>
						  	@else
								<a class="dropdown-item disabled" href="#/"><i class="fa fa-eye"></i> View Created Invoices</a>
						  	@endif
						    
						    @if($invoice->status == 'active')
						    	<a class="dropdown-item alert-dialog" href="#" data-id="{{$invoice->id}}" data-action="{{ url('recurring-invoices/terminate') }}" data-message="Are you sure, You want to terminate this invoice?" title="Terminate Invoice"><i class="fa fa-ban"></i> End</a>
						    @else
								<a class="dropdown-item disabled" href="#"><i class="fa fa-ban"></i> End</a>
						    @endif
						  </div>
						</div>

						<a href="{{ url('recurring-invoices/create') }}" class="btn btn-secondary btn-lg">Create Another Invoice</a>
					</div>
				</h4>
			</div>
			
			<div class="card-body">
				<div class="row details-info">
					<div class="col-md-2">
						<strong class="text-muted">STATUS</strong>
						<div class="invoice-status-wrapper">
							<span class="label label-{{ config('constants.invoice_status_class.'.$invoice->status) }} invoice-status"> {{ strtoupper($invoice->status) }}</span>
						</div>
					</div>
					<div class="col-md-4">
						<strong class="text-muted">CUSTOMER</strong> <br>
						<h4 class="text-primary no-margin inline-block"><a href="javascript:void(0);" class="customer-profile bold">{{ $customer_profile->name }} <i class="fa fa-exclamation-circle" aria-hidden="true"></i></a></h4>
					</div>
					<div class="col-md-3">
						<strong class="text-muted">INVOICE AMOUNT</strong> <br>
						<span class="amount-due">৳{{ number_format($invoice->total,2) }}</span>
					</div>
					<div class="col-md-3">
						<strong class="text-muted">CREATED TO DATE  </strong> <br>
						<span class="total-invoices">{{ $total_invoices ? ($total_invoices == 1 ? 1 .' Invoice' : $total_invoices.' Invoices') : 0 .' Invoices'}}</span>
					</div>
				</div>

				{{-- Panel One --}}
				<div class="panel panel-default panel-one margin-top-20">
					<div class="panel-body">

						<div class="row">
							<div class="col-md-1">
								<span class="step-no step-one badge btn"><i class="fa fa-check" aria-hidden="true"></i></span>
							</div>
							<div class="col-md-6">
								<h3 class="text-muted margin-top-0">
									Create Invoice
								</h3>
								<strong>Created On: </strong>
								{{ $invoice->created_at->format('F dS Y') }} <br>

								<strong>Payment Terms:</strong> {{ config('constants.payment_due.'.$invoice->getDayDiff()) }}
							</div>
							<div class="col-md-5 step-one-button-holder text-right">
								@if($invoice->status == 'end')
									<a href="#/" class="btn btn-default disabled"><i class="fa fa-edit" aria-hidden="true"></i> Edit</a>
								@else
									<a href="{{ url('recurring-invoices/'.$invoice->id.'/edit') }}" class="btn btn-default btn-edit"><i class="fa fa-edit" aria-hidden="true"></i> Edit</a>
								@endif
							</div>
						</div>
					</div>
				</div>
				{{-- /Panel One --}}

				{{-- Panel Two --}}
				<div class="panel panel-{{ $invoice->invoice_schedule ? 'default' : 'primary' }} panel-two panel-schedule margin-top-20">
					<div class="panel-body">
						<div class="row">
							<div class="col-md-1">
								<span class="step-no step-two badge btn">{!! $invoice->invoice_schedule ? '<i class="fa fa-check" aria-hidden="true"></i>' : '2' !!}</span>
							</div>
							<div class="col-md-9">
								<h3 class="text-muted margin-top-0">Set Schedule</h3>

								@if(!$schedule)
									{!! Form::open(['url' => '', 'role' => 'form', 'id' => 'invoice-schedule-form', 'class' => 'margin-top-20']) !!}
								@else
									{!! Form::model($schedule, ['url' => '', 'role' => 'form', 'id' => 'invoice-schedule-form', 'class' => 'margin-top-20 hide']) !!}
								@endif
									<div class="row margin-bottom-20">
										<div class="col-sm-6">
											<div class="row">
												{!! Form::label('frequency','Repeat this invoice', ['class' => 'control-label col-sm-6 text-right padding-right-0']) !!}
												<div class="col-sm-6">
													{!! Form::select('frequency',config('constants.frequency'), $schedule ? $schedule->frequency : 'monthly',['class'=>'form-control chosen-select', 'onchange' => 'setFrequencyAssociates(this.value);']) !!}
												</div>
											</div>
										</div>
										<div class="col-sm-6">
											<div class="monthly-content">
												on the {!! Form::select('day_of_month',getDayOfMonth(), 1,['class'=>' chosen-select']) !!} day of every month
											</div>
											<div class="weekly-content hide">
												every {!! Form::select('day_of_week',config('constants.day_of_week'), 2,['class'=>' chosen-select']) !!} 
											</div>
											<div class="yearly-content hide">
												every {!! Form::select('month',getMonth(), date('n'),['class'=>' chosen-select', 'onchange' => 'getMonthDays(this.value);']) !!} on the 
											</div>
										</div>
									</div>

									<div class="row yearly-content hide margin-bottom-20">
										<div class="offset-md-3 col-md-9">
											{!! Form::select('yearly_day_of_month',getDayOfMonth(date('n')), date('j'),['class'=>' inline-block chosen-select']) !!} day of the month
										</div>
									</div>

									<div class="row margin-bottom-20">
										<div class="col-md-6">
											<div class="row margin-bottom-20">
												{!! Form::label('start','Create first invoice on', ['class' => 'control-label col-sm-6 padding-right-0']) !!}
												<div class="col-sm-6">
													<div class="has-feedback">
													{!! Form::text('start',$schedule ? $schedule->start : date('Y-m-d'), ['class'=>'form-control datepicker', 'placeholder' => 'YYYY-MM-DD']) !!}	
													<span class="glyphicon glyphicon-calendar form-control-feedback"></span>
													</div>
													
												</div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="row margin-bottom-20">
												{!! Form::label('until','Until', ['class' => 'control-label col-sm-6 col-md-2 padding-right-0']) !!}
												<div class="col-sm-6">
													<div class="has-feedback">
													{!! Form::text('until',null, ['class'=>'form-control datepicker', 'placeholder' => 'YYYY-MM-DD']) !!}	
													<span class="glyphicon glyphicon-calendar form-control-feedback"></span>
													</div>
												</div>
											</div>
										</div>
									</div>

									<div class="row">
										{!! Form::label('timezone','Create in', ['class' => 'control-label col-md-3 text-right padding-right-0']) !!}
										<div class="col-md-9">
											{!! Form::select('timezone',getTimezones(), 'Asia/Dhaka', ['class'=>'form-control chosen-select']) !!} time zone	 <br>

											<small class="text-muted">Set a time zone to ensure invoice delivery in the morning based on the recipient's time zone.</small>
										</div>
									</div>

									<div class="row margin-top-20">
										{!! Form::label('trigger_time','Trigger Time', ['class' => 'control-label col-md-3 text-right padding-right-0']) !!}
										<div class="col-md-4">
											{!! Form::text('trigger_time',!empty($schedule->trigger_time) ? $schedule->getTriggerTime() : date('h:i A'),['class'=>'trigger-time form-control', 'data-date-format' => 'LT']) !!}
										</div>
									</div>	
									
									@if(!$schedule)
										{!! Form::hidden('invoice_schedule_id', '') !!}
									@else
										{!! Form::hidden('invoice_schedule_id', $schedule->id) !!}
									@endif
								{!! Form::close() !!}

							@if($schedule)
								<div class="details-info invoice-schedule-info">
									<strong>Repeat {{ ucfirst($schedule->frequency) }}:</strong> {{ $schedule->getRepeatInfo() }}<br>
									<strong>Dates:</strong>  Create first invoice on {{ Carbon::parse($schedule->start)->format('F jS Y') }},and end {{ $schedule->until ? Carbon::parse($schedule->until)->format('F jS Y') : 'never' }}<br>
									<strong>Time Zone:</strong> {{ $schedule->timezone }}
								</div>
							@endif
							</div>
							<div class="col-md-2 step-two-button-holder text-right">
							@if(!$schedule)
								<a href="javascript:setInvoiceSchedule({{ $invoice->id }});" class="btn btn-primary btn-save btn-next"> Next</a>
							@else
								@if($invoice->status == 'end')
									<a href="#/" class="btn btn-default disabled"><i class="fa fa-edit" aria-hidden="true"></i> Edit</a>
								@else
									<a href="javascript:editInvoiceSchedule();" class="btn btn-default btn-edit-schedule"><i class="fa fa-edit" aria-hidden="true"></i> Edit</a>
								@endif
								<div class="schedule-buttons hide">
									<a href="javascript:cancelEditSchedule();" class="btn btn-danger btn-xs"> Cancel</a>
									<a href="javascript:setInvoiceSchedule({{ $invoice->id }});" class="btn btn-primary btn-xs btn-save"><i class="fa fa-save" aria-hidden="true"></i> Save</a>
								</div>
							@endif
							</div>
						</div>
					</div>
				</div>
				{{-- /Panel Two --}}
				
				{{-- Panel Three --}}
				<div class="panel panel-{{ $schedule && !$invoice_sent_option ? 'primary':'default' }} panel-three margin-top-20">
					<div class="panel-body">
						<div class="row">
							<div class="col-md-1">
								<span class="step-no badge btn">{!! $invoice_sent_option ? '<i class="fa fa-check" aria-hidden="true"></i>' : '3' !!}</span>
							</div>
							<div class="col-md-11">
								<div class="row sent-option-header">
									<div class="col-md-4">
										<h3 class="text-muted margin-top-0">Send Option</h3>
									</div>
									@if($schedule)
									<div class="col-md-8">
										<div class="sent-option-button-area text-right{{$invoice_sent_option ? '': ' hide'}}">
											@if($invoice_sent_option)
											<a href="javascript:cancelEditSentOption();" class="btn btn-danger btn-cancel hide">Cancel</a>
											@endif
											<a href="javascript:openAutomaticArea({{ $invoice_sent_option ? $invoice_sent_option->id : '' }});" class="btn btn-default btn-automatic hide">Switch to Automatic Sending</a>
											<a href="javascript:openManualArea({{ $invoice_sent_option ? $invoice_sent_option->id : '' }});" class="btn btn-default btn-manual hide">Switch to Manual Sending</a>

											@if(!$invoice_sent_option)
											<a href="javascript:saveSentOption();" class="btn btn-primary btn-save">Next</a>
											@else
											<a href="javascript:saveSentOption();" class="btn btn-primary btn-save hide">Save</a>
												@if($invoice->status == 'end')
												<a href="#/" class="btn btn-default btn-edit disabled"><i class="fa fa-edit"></i> Edit</a>
												@else
												<a href="javascript:{{$invoice_sent_option->sent_media == 'manual' ? 'openManualArea('.($invoice_sent_option ? $invoice_sent_option->id : '').')' : 'openAutomaticArea('.($invoice_sent_option ? $invoice_sent_option->id : '').')'}};" class="btn btn-default btn-edit"><i class="fa fa-edit"></i> Edit</a>
												@endif
											@endif
										</div>
									</div>
									@endif
								</div>

								@if($schedule)
								@if(!$invoice_sent_option)
								{{-- Sent Media Area --}}
								<div class="sent-media-area margin-top-20">
									<div class="row">
										<div class="col-sm-6">
											<div class="sent-media" onclick="openAutomaticArea();">
												<i class="fa fa-paper-plane fa-3x"></i>
												<h4 class="bold margin-top-0">Send Automatically</h4>
												<span>Each invoice will be automatically emailed to your customer.</span>
											</div>
										</div>
										<div class="col-sm-6">
											<div class="sent-media"  onclick="openManualArea();">
												<i class="fa fa-envelope fa-3x"></i>
												<h4 class="bold margin-top-0">Send Manually</h4>
												<span>I will manually send each invoice</span>
											</div>
										</div>
									</div>
								</div>
								{{-- /Sent Media Area --}}
								@else
								<div class="sent-option-info margin-top-20">
									@if($invoice_sent_option->sent_media == 'manual')
									<div class="manual-sending-info">
										<strong>Manual Sending:</strong> I will send each invoice to my customer.
									</div>
									@elseif($invoice_sent_option->sent_media == 'automatic')
										<strong>Automatic Sending:</strong> Email the invoice automatically to {{ $invoice_sent_option->getEmails() }} when the invoice is generated.
									@endif
								</div>
								@endif
								
								{{-- Manual Send Area --}}
								<div class="manual-send-area bg-primary margin-top-20 padding-10 hide">
									<div class="row">
										<div class="col-sm-2"><i class="fa fa-envelope fa-3x"></i></div>
										<div class="col-sm-10">
											<h4 class="bold margin-top-0">Send Manually</h4>
											<span>I will manually send each invoice to my customer.</span>
										</div>
									</div>
								</div>
								{{-- /Manual Send Area --}}
								
								{{-- Automatic Send Area --}}
								<div class="automatic-send-area  margin-top-20 hide">
									<div class="bg-primary padding-10">
										<div class="row">
											<div class="col-sm-2"><i class="fa fa-paper-plane fa-3x"></i></div>
											<div class="col-sm-10">
												<h4 class="bold margin-top-0">Send Automatically</h4>
												<span>The invoice will be automatically emailed to your customer.</span>
											</div>
										</div>
									</div>
									
									@if(!$invoice_sent_option)
									{!! Form::open(['url' => '', 'role' => 'form', 'id' => 'invoice-send-form', 'class' => 'form-horizontal margin-top-20']) !!}
									@else
									{!! Form::model($invoice_sent_option,['url' => '', 'role' => 'form', 'id' => 'invoice-send-form', 'class' => 'form-horizontal margin-top-20']) !!}
									@endif
										<div class="form-group">
											<div class="col-sm-2 text-right bold">Send</div>
											<div class="col-sm-6">
												<strong>Invoice only</strong> <br>
												<small class="text-muted">Will be sent when the invoice is generated.</small>
											</div>
										</div>
										<div class="form-group">
											{!! Form::label('from', 'Send From', ['class' => 'control-label col-sm-2 text-right']) !!}
										    <div class="col-sm-6">
										      {!! Form::email('from', $invoice->owner->email, ['class' => 'form-control', 'placeholder' => 'Enter Sender Email']) !!}
										    </div>
										</div>

										<div class="form-group">
											{!! Form::label('to', 'Send To', ['class' => 'control-label col-sm-2 text-right']) !!}
										    <div class="col-sm-6">
										      {!! Form::email('to', $invoice->customer_profile->email, ['class' => 'form-control', 'placeholder' => 'Enter Recepient Email']) !!} 
										    </div>
										    <div class="col-sm-4">
										    	<a class="btn btn-primary btn-xs" href="javascript:openCC();" title="Add CC"><i class="fa fa-plus-circle" aria-hidden="true"></i></a>
										    </div>
										</div>

										<div class="form-group cc-row hide">
											{!! Form::label('ccs', 'Send CC', ['class' => 'control-label col-sm-2 text-right']) !!}
										    <div class="col-sm-6">
										      {!! Form::text('ccs',null,['class'=>'form-control','placeholder'=>'Enter CC', 'data-role' => 'tagsinput']) !!} 
										    </div>
										</div>

										<div class="form-group">
											{!! Form::label('message', 'Custom Message', ['class' => 'control-label col-sm-2 text-right']) !!}
										    <div class="col-sm-6">
										      {!! Form::textarea('message', null, ['class' => 'form-control', 'placeholder' => 'Enter Message to your customer','size'=>'30x3']) !!} 
										    </div>
										</div>

										<div class="form-group">
											<div class="offset-sm-2 col-sm-6">
							                    <div class="checkbox">
							                        <label>{!! Form::checkbox('is_attached',1,false) !!} Attach PDF of the invoice to the email sent to the customer </label>
							                    </div>
							                    <div class="checkbox">
							                        <label>{!! Form::checkbox('copy_myself',1,false) !!} Email a copy of each invoice to myself</label>
							                    </div>
											</div>
										</div>
										{!! Form::hidden('sent_media', '') !!}
										{!! Form::hidden('invoice_id', $invoice->id) !!}
										@if(!$invoice_sent_option)
										{!! Form::hidden('invoice_sent_option_id', '') !!}
										@else
										{!! Form::hidden('invoice_sent_option_id', $invoice_sent_option->id) !!}
										@endif
									{!! Form::close() !!}
								</div>
								{{-- /Automatic Send Area --}}
								@endif
							</div>
						</div>
					</div>
				</div>
				{{-- /Panel Three --}}

				@if($invoice_sent_option && $invoice->status == 'draft')
				{{-- Panel Four --}}
				<div class="panel panel-primary panel-four">
					<div class="panel-body text-center">
						<h2>You're almost set!</h2>
						<h3 class="text-muted">Review the details of your recurring invoice above and approve it when you're ready.</h3>
						<h4 class="bold">Once approved, your first invoice will be created and sent on {{ Carbon::parse($invoice_sent_option->start_date)->format('F dS, Y') }}.</h4>
						<a href="javascript:approveInvoice({{ $invoice->id }});" class="btn btn-primary"><i class="fa fa-thumbs-up"></i> Approve and Start Recurring Invoice</a>
					</div>
				</div>
				{{-- Panel Four --}}
				@endif
				
				{{-- Panel Invoice --}}
				@include('invoices.invoice_panel')
				{{-- /Panel Invoice --}}

			</div><!-- /.panel-body -->
		</div>
	</div>
</div>
	
@endsection

@section('custom-style')
{{-- Bootstrap Tags Input --}}
{!! Html::style($assets. '/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css') !!}
{{-- iCheck --}}
{!! Html::style($assets . '/plugins/icheck/skins/minimal/blue.css') !!}
{{-- Bootstrap Timepicker --}}
{!! Html::style($assets . '/plugins/bootstrap-timepicker/bootstrap-timepicker.min.css') !!}
@endsection

@section('custom-script')
{{-- Bootstrap Tags Input --}}
{!! Html::script($assets. '/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js') !!}

{{-- iCheck --}}
{!! Html::script($assets . '/plugins/icheck/icheck.min.js') !!}
{{-- Bootstrap Timepicker --}}
{!! Html::script($assets . '/plugins/bootstrap-timepicker/bootstrap-timepicker.min.js') !!}
<script>

function setFrequencyAssociates(value) 
{
	if(value) {
		if(value == 'daily') {
			$('.weekly-content,.monthly-content,.yearly-content').addClass('hide');
			
		}else if(value == 'weekly') {
			$('.monthly-content,.yearly-content').addClass('hide');
			$('.weekly-content').removeClass('hide');
		}else if(value == 'monthly') {
			$('.weekly-content,.yearly-content').addClass('hide');
			$('.monthly-content').removeClass('hide');
		}else if(value == 'yearly') {
			$('.weekly-content,.monthly-content').addClass('hide');
			$('.yearly-content').removeClass('hide');
		}
	}else {
		alert('Please select frequency properly.');
	}
}

function getMonthDays(value)
{
	if(value) {
		$('#ajaxloader').removeClass('hide');
		$.ajax({
			url:'{{ url('recurring-invoices/fetch-month-days') }}/'+value,
			success:function(response) {
				$('.yearly-content select[name=day_of_month]').html(response).val({{date('j')}}).trigger('chosen:updated');
			},
			error:function(xhr, ajaxOptions, thrownError) {
				alert(xhr.status + + thrownError);
			},
			complete:function() {
				$('#ajaxloader').addClass('hide');
			}
		});
	}else {
		alert('Please select month correctly.');
	}
}

var spinLoader = '<i class="fa fa-circle-o-notch fa-spin"></i><span class="sr-only">Loading...</span>';

function setInvoiceSchedule(invoice_id)
{
	var btnSave = $('.panel-two').find('.btn-save');
	var btnSaveText = btnSave.text();
	var btnSaveHref = btnSave.attr('href');
	btnSave.attr('href', '#').html(spinLoader);

	$.ajax({
		url:'{{ url('recurring-invoices/set-schedule') }}/'+invoice_id,
		method:'POST',
		data:$('#invoice-schedule-form').serialize(),
		success:function(response) {
			toastMsg(response.message, response.type);
			if(response.type == 'success') {
				// setTimeout(function() {
				// 	location.reload();
				// }, 2000);
				$('.panel-two .col-md-9').html(response.schedule_html);

				$('.panel-two').find('.step-two-button-holder').html('<a href="javascript:editInvoiceSchedule();" class="btn btn-default btn-edit-schedule"><i class="fa fa-edit" aria-hidden="true"></i> Edit</a><div class="schedule-buttons hide"> <a href="javascript:cancelEditSchedule();" class="btn btn-danger btn-xs"> Cancel</a> <a href="javascript:setInvoiceSchedule({{ $invoice->id }});" class="btn btn-primary btn-xs btn-save"><i class="fa fa-save" aria-hidden="true"></i> Save</a></div>');

				if(response.panel_three_html) {
					$('.panel-three').removeClass('panel-default').addClass('panel-primary').html(response.panel_three_html);
				}

				$('input[type="checkbox"]').iCheck({
				     checkboxClass: 'icheckbox_minimal-blue',
				     increaseArea: '20%' // optional
				});

				$(".chosen-select").chosen({width: "100%"});

			    $('.datepicker').datepicker({
			        autoclose: true,
			        format: 'yyyy-mm-dd'
			    });

			    {{-- Trigger Timepicker --}}
				$('input[name=trigger_time]').timepicker({
			        minuteStep: 5,
			        showInputs: false,
			        disableFocus: true
			    });

			    setFrequencyAssociates(response.frequency);

				cancelEditSchedule();
			}
		},
		complete:function(data, status) {
			btnSave.attr('href', btnSaveHref).text(btnSaveText);
		},
		error:function(xhr, ajaxOptions, thrownError) {
			alert(xhr.status + ' ' + thrownError);
		}
	});
}

function editInvoiceSchedule()
{
	if($('.panel-three').hasClass('panel-primary')) {
		$('.panel-three').removeClass('panel-primary').addClass('panel-default');
	}
	$('.panel-two').removeClass('panel-default').addClass('panel-primary');
	$('.panel-two .step-two').text(2);
	$('.invoice-schedule-info,.panel-four').addClass('hide');
	$('#invoice-schedule-form').removeClass('hide');
	$('.btn-edit-schedule').addClass('hide');
	$('.schedule-buttons').removeClass('hide');
}

function cancelEditSchedule()
{
	@if($schedule && !$invoice_sent_option)
		$('.panel-three').removeClass('panel-default').addClass('panel-primary');
	@endif
	$('.panel-two').removeClass('panel-primary').addClass('panel-default');
	$('.panel-two .step-two').html('<i class="fa fa-check" aria-hidden="true"></i>');
	$('.invoice-schedule-info').removeClass('hide');
	$('#invoice-schedule-form').addClass('hide');
	$('.btn-edit-schedule,.panel-four').removeClass('hide');
	$('.schedule-buttons').addClass('hide');
}


function openCC()
{
	$('.cc-row').toggleClass('hide');
}

function openAutomaticArea(invoice_sent_option_id=null)
{
	$('.sent-option-button-area,.btn-manual,.automatic-send-area').removeClass('hide');
	$('.sent-media-area,.btn-automatic,.manual-send-area,.sent-option-info,.panel-four').addClass('hide');
	$('#invoice-send-form input[name=sent_media]').val('automatic');
	
	$('.panel-three').removeClass('panel-default').addClass('panel-primary');
	$('.panel-three .step-no').text(3);
 	$('.panel-three').find('.sent-option-info,.btn-edit').addClass('hide');
 	$('.panel-three').find('.btn-cancel,.btn-save').removeClass('hide');

	if(invoice_sent_option_id) {
		{{--  
		// $('.panel-three').removeClass('panel-default').addClass('panel-primary');
		// $('.panel-three .step-no').text(3);
	 // 	$('.panel-three .btn-edit').addClass('hide');
	 // 	$('.panel-three .btn-cancel,.panel-three .btn-save').removeClass('hide');
	 // 	$('#invoice-send-form #ccs').val('{{$invoice_sent_option->ccs }}');
	 // 	$('#invoice-send-form #message').val('{{$invoice_sent_option->message }}');
	 // 	$('#invoice-send-form input[name=invoice_sent_option_id]').val({{$invoice_sent_option->id}});
	 // 	$('#invoice-send-form input[name=is_attached]').iCheck('{{ $invoice_sent_option->is_attached ? 'check' : 'uncheck'}}');
	 // 	$('#invoice-send-form input[name=copy_myself]').iCheck('{{ $invoice_sent_option->copy_myself ? 'check' : 'uncheck'}}');
	 --}}
	 setSentOptionInfoUsingAjax(invoice_sent_option_id);
	}else {
		$('#invoice-send-form input[type=checkbox]').iCheck('uncheck');
		$('#invoice-send-form input[name=ccs]').tagsinput('removeAll');
		$('#invoice-send-form input[name=invoice_sent_option_id]').val('');
	}
}

function openManualArea(invoice_sent_option_id=null)
{
	$('.sent-option-button-area,.btn-automatic,.manual-send-area').removeClass('hide');
	$('.sent-media-area,.btn-manual,.automatic-send-area,.sent-option-info,.panel-four').addClass('hide');
	$('#invoice-send-form input[name=sent_media]').val('manual');

	$('.panel-three').removeClass('panel-default').addClass('panel-primary');
	$('.panel-three .step-no').text(3);
 	$('.panel-three').find('.sent-option-info,.btn-edit').addClass('hide');
 	$('.panel-three').find('.btn-cancel,.btn-save').removeClass('hide');

	if(invoice_sent_option_id) {
		setSentOptionInfoUsingAjax(invoice_sent_option_id);
	}else{
		$('#invoice-send-form input[type=checkbox]').iCheck('uncheck');
		$('#invoice-send-form input[name=ccs]').tagsinput('removeAll');
		$('#invoice-send-form input[name=invoice_sent_option_id]').val('');
	}	
}

function setSentOptionInfoUsingAjax(invoice_sent_option_id)
{
	$.ajax({
		url:'{{ url('recurring-invoices/fetch-invoice-sent-option') }}/'+invoice_sent_option_id,
		method:'POST',
		success:function(response) {
			if(response.type  == 'error') {
				toastMsg(response.message, response.type);
			}else {
			 	$('#invoice-send-form #ccs').val(response.ccs);
			 	$('#invoice-send-form input[name=ccs]').tagsinput();
			 	$('#invoice-send-form #message').val(response.message);
			 	$('#invoice-send-form input[name=invoice_sent_option_id]').val(response.id);
			 	$('#invoice-send-form input[name=is_attached]').iCheck(response.is_attached ? 'check' : 'uncheck');
			 	$('#invoice-send-form input[name=copy_myself]').iCheck(response.copy_myself ? 'check' : 'uncheck');
			 	$('#invoice-send-form input[name=invoice_sent_option_id]').val(response.id);
			}
		},
		error:function(xhr, ajaxOptions, thrownError) {
			alert(xhr.status + ' ' +thrownError);
		}
	});
}

function saveSentOption(invoice_sent_option_id=null)
{
	var sent_media = $('#invoice-send-form input[name=sent_media]').val();
	if(!sent_media) {
		alert('please set sent media');

		return;
	}else {
		if($.inArray(sent_media, ['automatic', 'manual']) < 0) {
			alert('Sent media must be either automatic or manual');

			return;
		}

		var btnSave = $('.panel-three').find('.btn-save');
		var btnSaveText = btnSave.text();
		var btnSaveHref = btnSave.attr('href');
		btnSave.attr('href', '#').html(spinLoader);

		$.ajax({
			url :'{{ url('recurring-invoices/save-sent-option') }}',
			method:'POST',
			data:$('#invoice-send-form').serialize(),
			success:function(response) {
				toastMsg(response.message, response.type);

				if(response.type == 'success') {
					setTimeout(function() {
						$('.panel-three').removeClass('panel-primary').addClass('panel-default').html(response.panel_three_html);
						
						if($('.panel-four').length) {
							$('.panel-four').remove();
						}
						if(response.panel_four_html) {
							$(response.panel_four_html).insertAfter('.panel-three');
						}

						$('input[type="checkbox"]').iCheck({
						     checkboxClass: 'icheckbox_minimal-blue',
						     increaseArea: '20%' // optional
						});
						$('#invoice-send-form input[name=ccs]').tagsinput();
					}, 1500);
				}
			},
			complete:function() {
				btnSave.attr('href', btnSaveHref).text(btnSaveText);
			},
			error:function(xhr, ajaxOptions, thrownError) {
				alert(xhr.status + ' ' + thrownError);
			}
		});
	}
}

function cancelEditSentOption()
{
	$('.panel-three').find('.step-no').html('{!! $invoice_sent_option ? '<i class="fa fa-check" aria-hidden="true"></i>': 3 !!}');
	$('.panel-three').removeClass('panel-primary').addClass('panel-default');
	$('.panel-three').find('.btn-cancel,.btn-automatic,.btn-manual,.btn-save,.manual-send-area,.automatic-send-area').addClass('hide');
	$('.panel-three').find('.btn-edit,.sent-option-info').removeClass('hide');
	$('.panel-four').removeClass('hide');
}

function approveInvoice(invoiceId) 
{
	$('#ajaxloader').removeClass('hide');
	$.ajax({
		url:'{{ url('recurring-invoices/approve') }}/'+invoiceId,
		method:'POST',
		success:function(response) {
			toastMsg(response.message, response.type);
			if(response.type == 'success') {
				setTimeout(function() {
					$('.panel-four').remove();
					$('.invoice-status-wrapper').html(response.invoice_status);
					$('.total-invoices').text(response.total_invoices);
				}, 1000);
			}
		},
		complete:function() {
			$('#ajaxloader').addClass('hide');
		},
		error:function(xhr, ajaxOptions, thrownError) {
			alert(xhr.status + ' ' + thrownError);
		}
	});
}

(function() {
	@if($schedule)
		var frequency = '{{ $schedule->frequency }}';
		setFrequencyAssociates(frequency);
	@endif

	$('.customer-profile').popover({
		content:'{!! $customer_profile_html !!}',
		html:true,
		placement:"bottom",
	});

	$(".form-disabled :checkbox").bind("click", false);

	$('input[type="checkbox"]').iCheck({
	     checkboxClass: 'icheckbox_minimal-blue',
	     increaseArea: '20%' // optional
	});

	$('input[type="radio"]').iCheck({
	     radioClass: 'iradio_minimal-blue',
	     increaseArea: '20%' // optional
	});

	{{-- Trigger Timepicker --}}
	$('input[name=trigger_time]').timepicker({
        minuteStep: 5,
        showInputs: false,
        disableFocus: true
    });
})();
</script>
@endsection