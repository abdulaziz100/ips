{{-- Panel --}}
@php 
    $owner = \App\Company::getOwner();
    $company = $owner->companies()->first();
@endphp
<div class="panel panel-default panel-arrow">
  <div class="panel-heading">
    <h4 class="panel-title">
      <a class="btn-collapsed collapsed block text-muted" data-toggle="collapse" href="#collapse1">Business Address and Contact Details, Title, Summary, and Logo</a>
    </h4>
  </div>
  <div id="collapse1" class="panel-collapse collapse">
    <div class="panel-body">    
        <div class="row">
            <div class="col-md-4">
                <img class="img-fluid" src="{{ getCompanyLogoPath($company->id) }}" alt="Company Logo">
            </div>
            <div class="col-md-6 offset-md-2 text-right">
                <div class="form-group">
                    {!! Form::text('title',old('title') ? old('title') : (!empty($invoice->title) ? $invoice->title : getOption($company->id, 'invoice_title', 'Invoice')),['class'=>'form-control text-right','placeholder'=>'Title']) !!}
                </div>
                <div class="form-group">
                    {!! Form::text('invoice_summary',old('invoice_summary') ? old('invoice_summary') : (!empty($invoice->invoice_summary) ? $invoice->invoice_summary : getOption($company->id, 'invoice_subheading')),['class'=>'form-control text-right','placeholder'=>'Summary (e.g. project name, description of invoice)']) !!}
                </div>

                <div class="company-info text-right margin-top-20">
                    <h4 class="bold">{{ $company->name }}</h4>
                    @if($company->address)
                    <span>{{ $company->address }} <br></span>
                    @endif
                    @if($company->state || $company->city || $company->zip_code)
                    <span>{{ ($company->state ? $company->state .', ' : '') . ($company->city ? $company->city .' ' : '') .($company->zip_code ? $company->zip_code : '') }}</span> <br>
                    @endif

                    <span>{{ $company->country_obj->name }}</span> <br>
                    @if($company->primary_phone)
                        <span>{{ $company->primary_phone }}</span> <br>
                    @endif

                    @if($company->alt_phone)
                        <span>{{ $company->alt_phone }}</span> <br>
                    @endif

                    @if($company->website)
                        <span>{{ $company->website }}</span> <br>
                    @endif
                </div>
            </div>
        </div>
    </div>
  </div>
</div>
{{-- /Panel --}}

<div class="panel panel-success">
    <div class="panel-body">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="indentifier" class="control-label"> Cutomer {!! validation_error($errors->first('customer_profile_id'),'customer_profile_id') !!} </label>
                    {!! Form::select('customer_profile_id',$customers, old('customer_profile_id'),['class'=>'form-control chosen-select' ,'id' => 'customer_profile_id']) !!}
                </div>
                <a class="bold" href="javascript:addCustomer();"><i class="fa fa-plus-circle" aria-hidden="true"></i> Add a new customer</a>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="identifier" class="control-label"> Invoice Identifier  </label>
                    <span>Auto-generated</span>
                </div>
                <div class="form-group">
                    {!! Form::label('po_number','P.O/S.O Number') !!}
                    {!! Form::text('po_number', old('po_number'),['class'=>'form-control','placeholder'=>'P.O/S.O Number']) !!}
                </div>
                <div class="form-group">
                    <label for="invoice_date" class="control-label"> Invoice Date </label>
                    <span>Auto-generated</span>
                </div>
                <div class="form-group">
                    <label for="payment_due" class="control-label"> Payment Due {!! validation_error($errors->first('payment_due'),'payment_due') !!} </label>
                    {!! Form::select('payment_due', config('constants.payment_due'), old('payment_due') ? old('payment_due') :(!empty($diff) ? $diff : date('Y-m-d')),['class'=>'form-control chosen-select']) !!}
                </div>
            </div>
        </div>

        <div class="margin-top-20">
            <div class="table-responsive">
                <table class="table table-hover table-striped table-vertical-middle">
                    <thead>
                        <tr class="success">
                            <th>Item</th>
                            <th width="40%"></th>
                            <th width="10%">Quantity</th>
                            <th width="12%">Price</th>
                            <th width="8%">Vat(%)</th>
                            <th width="12%" class="text-right">Amount</th>
                            <th width="5"></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr id="masterRow">
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td class="text-right"></td>
                            <td><a href="#" class="btn btn-xs btn-danger disabled"><i class="fa fa-trash-o"></i></a></td>
                        </tr>

                        @if(!empty($invoice_details))
                            @foreach($invoice_details as $invoice_detail)
                            <tr>
                                <td>
                                    {{ $invoice_detail->service_name }}
                                    {!! Form::hidden('svc_id[]', $invoice_detail->service_id) !!}
                                </td>
                                <td>
                                    {!! Form::text('service_description[]',$invoice_detail->service_description,['class'=>'form-control','placeholder'=>'Enter item description']) !!}
                                </td>
                                <td>
                                    {!! Form::number('quantity[]',$invoice_detail->quantity,['class'=>'form-control','placeholder'=>'Enter Quantity', 'min' => 1, 'onchange' => 'updateInvoiceInfo(this);','step' => 'any']) !!}
                                </td>
                                <td>
                                    {!! Form::number('rate[]',$invoice_detail->rate,['class'=>'form-control','placeholder'=>'Enter Price', 'min' => 0, 'onchange' => 'updateInvoiceInfo(this);' ,'step' => 'any']) !!}
                                </td>
                                <td>
                                    {!! Form::number('vat[]',$invoice_detail->vat,['class'=>'form-control','placeholder'=>'Enter Vat', 'min' => 0, 'onchange' => 'updateInvoiceInfo(this);','step' => 'any']) !!}
                                </td>
                                <td class="text-right">
                                    ৳{{ number_format($invoice_detail->amount, 2) }}
                                </td>
                                <td><a class="btn btn-xs btn-danger" onclick="javascript:removeRow(this);"><i class="fa fa-trash-o"></i></a></td>
                            </tr>
                            @endforeach
                        @endif

                        <tr id="add-item-row">
                            <td colspan="7" align="center">
                                <a class="bold text-decoration-none item-add-link" href="#/"><i class="fa fa-plus-circle" aria-hidden="true"></i> Add an Item</a>
                                <div class="service-holder hide">
                                    <div class="form-group">
                                        {!! Form::select('service_id',$services, old('service_id'),['class'=>'form-control chosen-select', 'onchange' => 'addService();', 'id' => 'service_id']) !!}
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                    <tfoot>
                        <tr id="sub-total-row">
                            <td colspan="5" class="text-right">Subtotal</td>
                            <td id="sub-total-column" class="text-right">{{ !empty($invoice) ? '৳'. number_format($invoice->sub_total,2) : '' }}</td>
                            <td></td>
                        </tr>
                        @if(!empty($invoice))
                        <tr id="total-vat-row">
                            <td colspan="5" class="text-right">Total Vat</td>
                            <td class="text-right">৳{{ number_format($invoice->vat_total, 2) }}</td>
                        </tr>
                        @endif
                        <tr id="total-row">
                            <td colspan="5" class="text-right">Total</td>
                            <td id="total-column" class="text-right">{{ !empty($invoice) ? '৳'. number_format($invoice->total,2) : '' }}</td>
                            <td></td>
                        </tr>
                    </tfoot>
                </table>
            </div>

            <div class="form-group">
                {!! Form::label('notes','Notes') !!}
                {!! Form::textarea('notes',old('notes'),['class'=>'form-control','placeholder'=>'Enter notes that are visible to your customer', 'size' => '30x3']) !!}
            </div>
        </div>
    </div>
</div>

{{-- Panel --}}
<div class="panel panel-default panel-arrow">
  <div class="panel-heading">
    <h4 class="panel-title">
      <a class="btn-collapsed collapsed block text-muted" data-toggle="collapse" href="#invoice-footer">Footer</a>
    </h4>
  </div>
  <div id="invoice-footer" class="panel-collapse collapse">
    <div class="panel-body">
        <div class="form-group">
            {!! Form::textarea('invoice_footer',old('invoice_footer') ? old('invoice_footer') :(!empty($invoice->invoice_footer) ? $invoice->invoice_footer:getOption($company->id, 'invoice_footer')),['class'=>'form-control','placeholder'=>'Enter a footer for this invoice (e.g. tax information, thank you note)', 'size' => '30x3']) !!}
        </div>
    </div>
  </div>
</div>
{{-- /Panel --}}

@section('custom-script') 
<script>
$('.item-add-link').on('click', function() {
    $('.service-holder').removeClass('hide');
    setTimeout(function(){ 
        $('#service_id').trigger("chosen:open"); }, 
    10);

    $(this).addClass('hide');
});

function addService()
{
    var serviceId = $('select[name=service_id]').val();

    if(serviceId) {
        $.ajax({
            url:'{{ url('invoices/fetch-service') }}/'+serviceId,
            method:'POST',
            dataType:'JSON',
            success:function(data) {
                if(data) {
                    var servicePrice = data.regular_price;
                    if(data.promotional_price) {
                        var servicePrice = data.promotional_price;
                    }
                    servicePrice = parseFloat(servicePrice).toFixed(2);

                    var rowObject = $("#masterRow").clone();
                    
                    rowObject[0].cells[0].innerHTML = data.title+'<input type="hidden" name="svc_id[]" value="'+data.id+'">';
                    rowObject[0].cells[1].innerHTML = '<input type="text" name="service_description[]" class="form-control" placeholder="Enter item description" value="'+data.description+'"/>';
                    rowObject[0].cells[2].innerHTML = '<input type="number" name="quantity[]" class="form-control" placeholder="Enter Quantity" value="1" min="1" onchange="updateInvoiceInfo(this);" step="any"/>';
                    rowObject[0].cells[3].innerHTML = '<input type="number" name="rate[]" class="form-control" placeholder="Enter Price" value="'+servicePrice+'" min="0" onchange="updateInvoiceInfo(this);" step="any"/>';
                    rowObject[0].cells[4].innerHTML = '<input type="number" name="vat[]" class="form-control" placeholder="%" value="0" min="0" max="100" onchange="updateInvoiceInfo(this);" step="any"/>';
                    rowObject[0].cells[5].innerText = servicePrice;

                    rowObject[0].cells[6].innerHTML = '<a class="btn btn-xs btn-danger" onclick="javascript:removeRow(this);"><i class="fa fa-trash-o"></i></a>';

                    var rowHtml = rowObject.html();
                    $('<tr>'+ rowHtml + '</tr>').insertBefore('#add-item-row');

                    calculateInvoice();
                }
            },
            complete:function() {
                $(".service-holder").addClass('hide');
                $('.item-add-link').removeClass('hide');
                $('select[name=service_id]').val('').trigger('chosen:updated');
            },
            error:function(xhr, ajaxOptions, thrownError) {
                alert(xhr.status + ' ' +thrownError);
            }
        });
    }
}

function removeRow(element) {

    var rowCount = $('tbody tr').length;
    if (rowCount > 2) {
        $(element).parents("tr").remove();
    }
    calculateInvoice();
}

function calculateInvoice()
{
    var totalQuantity = 0;
    var subTotal = 0;
    var totalVatAmount = 0;
    var total = 0;

    $('tbody tr').each(function() {
        var quantity = parseFloat($(this).find('input[name^=quantity]').val());
        var price = parseFloat($(this).find('input[name^=rate]').val());
        var vat = parseFloat($(this).find('input[name^=vat]').val());

        if(quantity && price) {
            totalQuantity += quantity;
            subTotal += quantity*price;
            if(Math.floor(vat) == vat && $.isNumeric(vat)) {
                totalVatAmount += parseFloat((quantity*price*vat)/100);
            }
            var amount = parseFloat(quantity*price).toFixed(2);
            $(this).find('td:nth-child(6)').text('৳' + amount);
        }
    });


    if(totalVatAmount) {
        if($('#total-vat-row').length) {
            $('#total-vat-row').remove();
        }

        $('<tr id="total-vat-row"><td colspan="5" class="text-right">Total Vat</td><td class="text-right">৳'+parseFloat(totalVatAmount).toFixed(2)+'</td></tr>').insertBefore('#total-row');
    }else {
        if($('#total-vat-row').length) {
            $('#total-vat-row').remove();
        }
    }
    total = parseFloat(subTotal) + parseFloat(totalVatAmount);
    $('#sub-total-column').text('৳' + parseFloat(subTotal).toFixed(2));
    $('#total-column').text('৳' + parseFloat(total).toFixed(2));
}

function updateInvoiceInfo(obj)
{
    var elemName = $(obj).attr('name');
    var elemValue = parseFloat($(obj).val());
    if(isNaN(elemValue)) {
        alert('Please insert number');
        $(obj).parent().addClass('has-error');
        return;
    }else {
        $(obj).parent().removeClass('has-error');
    }

    calculateInvoice();
}

function addCustomer()
{
    $("#customer-form").trigger('reset');
    $('#currency').val('BDT').trigger('chosen:updated');
    $('#country').val('BD').trigger('chosen:updated');

    $('#customerModal').modal("show");
}

function addName()
{
    if($('input[name=name]').length) {
        var name = $('input[name=name]').val();
        $('input[name=contact_person_name]').val(name);
    }
}

function addEmail()
{
    if($('input[name=email]').length) {
        var email = $('input[name=email]').val();
        $('input[name=contact_person_email]').val(email);
    }
}

function saveCustomer()
{
    $(".validation-error").text('*');
    $("#ajaxloader").removeClass('hide');
    $.ajax({
        url:'{{ url('invoices/save-customer') }}',
        method:'POST',
        data:$('#customer-form').serialize(),
        dataType:'JSON',
        success:function(response) {
            if(response.status == 400) {
                // validation error
                $.each(response.error ,function(index, value) {
                    $("#ve-"+index).html('['+value+']');
                });
            }else if(response.status == 404) {
                toastMsg(response.message, response.type);
            }else {
                $('#recurring-invoice-wrapper #customer_profile_id').html(response.options).trigger("chosen:updated");
                $('#recurring-invoice-wrapper #customer_profile_id').val(response.customer_profile_id).trigger("chosen:updated");
                $('#customerModal').modal('hide');
            }
            $("#ajaxloader").addClass('hide');
        },
        error:function(xhr, ajaxOptions, thrownError) {
            alert(xhr.status + ' ' + thrownError);
        }
    });
}

$(document).mouseup(function(e) 
{
    var container = $(".service-holder");

    // if the target of the click isn't the container nor a descendant of the container
    if (!container.is(e.target) && container.has(e.target).length === 0) 
    {
        container.addClass('hide');
        $('.item-add-link').removeClass('hide');
    }
});

</script>
@endsection