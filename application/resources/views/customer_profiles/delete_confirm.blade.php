@extends('layouts.master')

@section('title') Delete Confirmation @endsection

@section('content')
<div class="row">
	<div class="col-sm-12">
		<div class="card m-b-30">
			<div class="card-header">
				<h4 class="card-title">Delete Confirmation</h4>
			</div>
			
			<div class="card-body">
				<h4>All these following information will be deleted along with the customer record.</h4>
				<div class="margin-top-20">
				{{-- Delete --}}
					<a href="#" data-id="{{$customer_profile->id}}" data-action="{{ url('customer-profiles/delete') }}" data-message="Are you sure, You want to delete this customer?" class="btn btn-danger btn-lg alert-dialog" title="Delete customer"><i class="fa fa-trash white"></i> Delete Customer</a>
					<a href="{{ url('customer-profiles/list') }}" class="btn btn-default btn-lg" title="Delete customer"><i class="fa fa-reply white"></i> No</a>
				</div>

				<div class="table-responsive margin-top-20">
					<table class="table table-bordered training-table">
						<thead>
							<th>Invoice ID</th>
							<th>P.O Number</th>
							<th>Total Amount</th>
							<th>Invoice Date</th>
							<th>Payment Due</th>
							<th width="10%">Status</th>
						</thead>
						<tbody>
						@forelse($invoices as $invoice)
							<tr>
								<td>{{ strtoupper($invoice->identifier) }}</td>
								<td>{{ $invoice->po_number ? $invoice->po_number: 'N/A' }}</td>
								<td>{{ number_format($invoice->total,2) }}</td>
								<td>{{ Carbon::parse($invoice->invoice_date)->format('d M,Y') }}</td>
								<td>{{ Carbon::parse($invoice->payment_due)->format('d M,Y') }}</td>
								<td>{!! $invoice->getStatus() !!}</td>
								
							</tr>
						@empty
							<tr>
								<td colspan="6" align="center">No Records Found</td>
							</tr>
						@endforelse
						</tbody>
					</table>
				</div>
			</div><!-- /.card-body -->
		</div>
	</div>
</div>
	
@endsection

@section('custom-style')

@endsection

@section('custom-script')

@endsection