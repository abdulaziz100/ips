<fieldset>
	<legend>Billing Info</legend>

	<div class="form-group">
		{!! Form::label('currency','Currency') !!} 
		{!! Form::select('currency',config('currencies'), old('currency') ? old('currency') : (!empty($customerProfile->currency) ? $customerProfile->currency : 'BDT'),['class'=>'form-control chosen-select']) !!}
	</div>
	<div class="form-group">
		{!! Form::label('address','Address') !!} 
		{!! Form::textarea('address',old('address'),['class'=>'form-control','placeholder' =>'Address', 'size' => '30x2']) !!}
	</div>
	<div class="form-group">
		<label for="country" class="control-label"> Country {!! validation_error($errors->first('country'),'country') !!} </label>
		{!! Form::select('country',$countries,old('country') ? old('country'):(!empty($customerProfile->country) ? $customerProfile->country : 'BD'),['class'=>'form-control chosen-select','placeholder'=>'Select a Country', 'id' => 'country']) !!}
	</div>
	<div class="form-group">
		{!! Form::label('city','City') !!} 
		{!! Form::text('city',old('city'),['class'=>'form-control','placeholder' =>'City']) !!}
	</div>
	<div class="form-group">
		{!! Form::label('state','State/Province') !!} 
		{!! Form::text('state',old('state'),['class'=>'form-control','placeholder' =>'State/Province']) !!}
	</div>
	<div class="form-group">
		{!! Form::label('area','Area') !!} 
		{!! Form::text('area',old('area'),['class'=>'form-control','placeholder' =>'Area']) !!}
	</div>
	<div class="form-group">
		{!! Form::label('zip_code','Zip/Postal Code') !!} 
		{!! Form::text('zip_code',old('zip_code'),['class'=>'form-control','placeholder' =>'Zip/Postal Code']) !!}
	</div>
</fieldset>