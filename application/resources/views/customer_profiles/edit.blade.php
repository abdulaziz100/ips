@extends('layouts.master')

@section('title') Edit Customer @endsection

@section('content')
<div class="row">
	<div class="col-sm-12">
		<div class="card m-b-30"> 
			<div class="card-header">
				<h4 class="card-title">Edit Customer</h4> 
			</div>

			{!! Form::model($customerProfile,array('url' => 'customer-profiles', 'role' => 'form')) !!}
			<div class="card-body">
				@include('customer_profiles.form')
			</div><!-- /.card-body -->
			<div class="card-footer">
			{!! Form::hidden('customer_profile_id',$customerProfile->id) !!}
				<button class="btn btn-info">
					<i class="fa fa-save"></i> Update
				</button>
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
	
@endsection
