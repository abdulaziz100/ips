<fieldset>
	<legend>Basic Info</legend>
	<div class="form-group">
		<label for="name" class="control-label"> Name/Company Name {!! validation_error($errors->first('name'),'name') !!} </label>
		{!! Form::text('name',old('name'),['class'=>'form-control','placeholder' =>'Name/Company Name', 'id' => 'name', 'onchange' => 'addName();']) !!}
	</div>
	<div class="form-group">
		<label for="email" class="control-label"> Email {!! validation_error($errors->first('email'),'email', true) !!} </label>
		{!! Form::email('email',old('email'),['class'=>'form-control','placeholder' =>'Email', 'onchange' => 'addEmail();']) !!}
	</div>
	<div class="form-group">
		<label for="primary_phone" class="control-label"> Primary Phone {!! validation_error($errors->first('primary_phone'),'primary_phone') !!} </label>
		{!! Form::text('primary_phone',old('primary_phone'),['class'=>'form-control','placeholder' =>'Primary Phone', 'id' => 'primary_phone']) !!}
	</div>
	<div class="form-group">
		<label for="alt_phone" class="control-label"> Alternate Phone </label>
		{!! Form::text('alt_phone',old('alt_phone'),['class'=>'form-control','placeholder' =>'Alternate Phone', 'id' => 'alt_phone']) !!}
	</div>
	<div class="form-group">
		<label for="contact_person_name" class="control-label"> Contact Person Name </label>
		{!! Form::text('contact_person_name',old('contact_person_name'),['class'=>'form-control','placeholder' =>'Contact Person Name', 'id' => 'contact_person_name']) !!}
	</div>
	<div class="form-group">
		{!! Form::label('contact_person_email','Contact Person Email') !!} 
		{!! Form::email('contact_person_email',old('contact_person_email'),['class'=>'form-control','placeholder' =>'Contact Person Email']) !!}
	</div>
	<div class="form-group">
		{!! Form::label('customer_since','Customer Since') !!} 
		{!! Form::text('customer_since',old('customer_since'),['class'=>'form-control datepicker','placeholder' =>'Customer Since']) !!}
	</div>
</fieldset>