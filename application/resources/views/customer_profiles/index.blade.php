@extends('layouts.master')

@section('title') List of Customers @endsection

@section('content')
<div class="row">
	<div class="col-sm-12">
		<div class="card m-b-30">
			<div class="card-header">
				<h4 class="card-title">List of Customers 
					<a href="{{ url('customer-profiles/create')}}" class="btn btn-danger btn-xs pull-right"><i class="fa fa-plus-circle"></i> Add New</a>
				</h4>
			</div>
			
			<div class="card-body">

				{!! Form::open(['url'=>Request::url(), 'role' => 'form']) !!}
                <div class="row">
                    <div class="col-sm-11 padding-right-0">
                        <div class="form-group">
                            {!! Form::text('search_item',Request::get('search_item') ? Request::get('search_item'):'',['class'=>'form-control','placeholder'=>'Search the customers by their name,email or phone and hit Enter']) !!}
                        </div>
                    </div>
                    <div class="col-sm-1">
                        <div class="form-group">
                            <a href="{{Request::url()}}" class="btn btn-default float-right" title="Refresh"><i class="fa fa-refresh" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}

				<div class="table-responsive">
					<table class="table table-bordered training-table">
						<thead>
							<th>Name</th>
							<th>Email</th>
							<th>Phone</th>
							<th>City</th>
							<th width="10%">Country</th>
							<th width="12%">Created at</th>
							<th width="14%">Action</th>
						</thead>
						<tbody>
						@forelse($customer_profiles as $customer_profile)
							<tr>
								<td>{{ $customer_profile->name }}</td>
								<td>{{ $customer_profile->email ? $customer_profile->email: 'N/A' }}</td>
								<td>{{ $customer_profile->primary_phone }}</td>
								<td>{{ $customer_profile->city ? $customer_profile->city : 'N/A' }}</td>
								<td>{{ $customer_profile->country_obj->name }}</td>
								<td>{{ $customer_profile->created_at->format('d M, Y')}}</td>
								<td>
					                {{-- Edit --}}
	                                <a href="{{url('customer-profiles/'.$customer_profile->id.'/edit')}}" class="btn btn-default btn-xs" title="Edit this information"><i class="fa fa-pencil"></i></a>
	                                {{-- Activate/Deactivate --}}
	                                @if($user = $customer_profile->user)
                                    <a href="#" data-id="{{$user->id}}" data-action="{{ url('users/change-active') }}" data-message="Are you sure, You want to {{$user->active ? 'deactivate' : 'activate' }} this user?" class="btn btn-xs btn-{{$user->active ? 'danger':'info'}} alert-dialog" title="{{$user->active ? 'Deactivate User':'Activate User'}}"><i class="fa fa-{{$user->active ? 'ban':'check'}}"></i></a>
                                    @else
									<a href="#" class="btn btn-xs btn-danger disabled" title="User not found"><i class="fa fa-ban"></i></a>
									@endif
									{{-- Change Status --}}
									<a href="#" data-id="{{$customer_profile->id}}" data-action="{{ url('customer-profiles/change-active') }}" data-message="Are you sure, You want to change customer status to {{$customer_profile->active ? 'inactive' : 'active' }}?" class="btn btn-xs btn-{{$customer_profile->active ? 'warning':'success'}} alert-dialog" data-toggle="tooltip" title="Change customer status to {{$customer_profile->active ? 'inactive':'active'}}"><i class="fa fa-{{$customer_profile->active ? 'level-down':'level-up'}}"></i></a>
	                                {{-- Delete --}}
									<a href="{{ url('customer-profiles/delete-confirm/'.$customer_profile->id) }}" class="btn btn-danger btn-xs" title="Delete customer" target="_blank"><i class="fa fa-trash white"></i></a>
								</td>
							</tr>
						@empty
							<tr>
								<td colspan="7" align="center">No Records Found</td>
							</tr>
						@endforelse
						</tbody>
					</table>
				</div>
			</div><!-- /.card-body -->

			@if($customer_profiles->total() > 10)
			<div class="card-footer">
				<div class="row">
					<div class="col-md-4">
						{{ $customer_profiles->paginationSummery }}
					</div>
					<div class="col-md-8 text-right">
						<div class="float-right">
							{!! $customer_profiles->links() !!}
						</div>
					</div>
				</div>
			</div>
			@endif
		</div>
	</div>
</div>
	
@endsection

@section('custom-style')

@endsection

@section('custom-script')

@endsection