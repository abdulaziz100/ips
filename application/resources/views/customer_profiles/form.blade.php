<div class="row">
	<div class="col-md-6">
		@include('customer_profiles.basic_info')
	</div>
	<div class="col-md-6">
		@include('customer_profiles.billing_info')
	</div>
</div>

@if(empty($customerProfile) || (!empty($customerProfile) && !$customerProfile->user))
{{-- Login info --}}
<fieldset id="login-info">
    <legend>Login Information</legend>
    <div class="row">
        <div class="col-sm-6">
            <div class="web-access-wrapper">
                <div class="form-group">
                    <label>{!! Form::checkbox('enable_web_access', 1, null,['onclick' => 'openUserInfoSection();']) !!} Enable Web Access</label>
                </div>
                <div class="user-info hide">
                    <div class="form-group">
                        <label for="customer_email" class="control-label">Email</label>
                        {!! Form::email('customer_email', null, ['class'=>'form-control', 'placeholder' => 'Email', 'disabled']) !!}
                    </div>
                    <div class="form-group">
                        <label for="password" class="control-label">Password {!! validation_error($errors->first('password'),'password') !!}</label>
                        {!! Form::password('password',['class'=>'form-control', 'placeholder' => 'Password']) !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</fieldset>
{{-- /Login info --}}
@endif


@section('custom-script')
<script>
function openUserInfoSection()
{
    if($('input[name=enable_web_access]').is(':checked')) {
        $('.user-info').removeClass('hide');
    }else {
        $('.user-info').addClass('hide');
    }
}

function addName()
{
	if($('input[name=name]').length) {
		var name = $('input[name=name]').val();
		$('input[name=contact_person_name]').val(name);
	}
}

function addEmail()
{
	if($('input[name=email]').length) {
		var email = $('input[name=email]').val();
		$('input[name=contact_person_email]').val(email);
		$('input[name=customer_email]').val(email);
	}
}

(function() {
	@if(old('email'))
        addEmail();
    @endif

	@if(old('enable_web_access'))
        openUserInfoSection();
    @endif
})();
</script>
@endsection