@extends('layouts.master')

@section('title') Categoy @endsection

@section('content')
<div class="row">
	<div class="col-md-6">
		<div class="card m-b-30">
		
			<div class="card-header">
				<h4 class="card-title">Create/Edit Category</h4> 
			</div>
			@if(empty($category->id)) 
				{!! Form::open(array('url' => 'categories', 'role' => 'form')) !!}
			@else
				{!! Form::model($category, array('url' => 'categories', 'role' => 'form')) !!}
			@endif
				<div class="card-body">
					@include('categories.form')
				</div><!-- /.card-body -->
				<div class="card-footer">
					@if(!empty($category->id))
					{!! Form::hidden('category_id', $category->id) !!}
					@endif
					<button class="btn btn-info">
						<i class="fa fa-save"></i> {{empty($category->id) ? 'Save' : 'Update'}}
					</button>
				</div>
			{!! Form::close() !!}
		</div>
	</div>
	<div class="col-md-6">
		<div class="card m-b-30">
			<div class="card-header">
				<h4 class="card-title">List of Categories 
					<a href="{{ url('categories/list')}}" class="btn btn-danger btn-xs pull-right"><i class="fa fa-plus-circle"></i> Add New</a>
				</h4>
			</div>
			<div class="card-body">
				{!! Form::open(['url' => Request::url(), 'role'=>'form']) !!}
					<div class="row">
						<div class="col-sm-6">
							<div class="form-group">
                                {!! Form::text('title',Request::input('title') ? Request::input('title') :'',['class'=>'form-control','placeholder'=>'Title']) !!}
                            </div>
						</div>
						<div class="col-sm-3">
							<div class="form-group margin-top-25">
								<button class="btn btn-default btn-sm"><i class="fa fa-search" aria-hidden="true"></i> Search</button>
							</div>
						</div>
						<div class="col-sm-3">
							<div class="form-group margin-top-25 pull-right">
								<a href="{{ url('categories/list') }}" class="btn btn-info btn-sm">Reset</a>
							</div>
						</div>
					</div>
				{!! Form::close() !!}
				<div class="table-responsive">
					<table class="table table-bordered">
						<thead>
							<tr>
								<th>Title</th>
								<th>Parent Category</th>
								<th width="24%">Created at</th>
								<th width="16%">Action</th>
							</tr>
						</thead>
						<tbody>
						@forelse($categories as $category)
							<tr>
								<td>{{$category->title}}</td>
								<td>{{ $category->parent ? $category->parent->title : 'N/A' }}</td>
								<td>{{ $category->created_at->format('d M, Y')}}</td>
								<td>
					                {{-- Edit --}}
	                                <a href="{{url('categories/'.$category->id.'/edit')}}" class="btn btn-default btn-xs" title="Edit this category"><i class="fa fa-pencil"></i></a>
	                                {{-- Delete --}}
									<a href="#" data-id="{{$category->id}}" data-action="{{ url('categories/delete') }}" data-message="Are you sure, You want to delete this category?" class="btn btn-danger btn-xs alert-dialog" title="Delete Category"><i class="fa fa-trash white"></i></a>
								</td>
							</tr>
						@empty
							<tr>
								<td colspan="4" align="center">No Records Found</td>
							</tr>
						@endforelse
						</tbody>
					</table>
				</div>
			</div><!-- /.card-body -->

			@if($categories->total() > 10)
			<div class="card-footer">
				<div class="row">
					<div class="col-sm-4">
						{{ $categories->paginationSummery }}
					</div>
					<div class="col-sm-8 text-right">
						<div class="float-right">
							{!! $categories->links() !!}
						</div>
					</div>
				</div>
			</div>
			@endif
		</div>
	</div>
</div>
	
@endsection

@section('custom_style')
{{-- Select2 --}}
<link href="{{ url('assets/plugins/select2/select2.min.css') }}" rel="stylesheet" type="text/css">
@endsection

@section('custom_script')
{{-- Select2 --}}
<script src="{{ url('assets/plugins/select2/select2.full.min.js') }}"></script>
<script type="text/javascript">
    {{-- Initialize Select2 --}}
    $('select').select2();
</script>
@endsection