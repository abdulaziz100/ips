<div class="form-group">
	<label for="title" class="control-label"> Title {!! validation_error($errors->first('title'),'title') !!} </label>
	{!! Form::text('title',old('title'),['class'=>'form-control','placeholder' =>'Title', 'id' => 'title']) !!}
</div>
<div class="form-group">
	{!! Form::label('description','Description') !!} 
	{!! Form::textarea('description',old('description'),['class'=>'form-control','placeholder' =>'Description', 'size' => '30x2']) !!}
</div>
<div class="form-group">
	{!! Form::label('parent_id','Parent Category') !!} 
	{!! Form::select('parent_id',$parent_categories, old('parent_id') ? old('parent_id') : (!empty($category) && $category->parent_id ? $category->parent_id : ''),['class'=>'form-control chosen-select']) !!}
</div>

