@extends('layouts.master')

@section('title') Dashboard @endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif

            @if(!isSuperAdmin())
            
            <div class="row">
                
                <div class="col-md-6">
                    <div class="card m-b-30">
                        <div class="card-body">
                            <h4 class="mt-0 header-title">Overdue Invoices & Bills</h4>
                        
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>Customer</th>
                                            <th class="text-right">Amount Due</th>
                                            <th width="27%">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @forelse($overdue_invoices as $invoice)
                                        <tr>
                                            <td>{{ $invoice->customer_profile->name }}</td>
                                            <td class="text-right">৳{{ number_format($invoice->getAmountDue(),2) }}</td>
                                            <td>
                                            @if($invoice->customer_profile->email)
                                                <a href="javascript:openRemindInvoiceModal({{ $invoice->id }});" class="btn btn-info btn-xs">Send a reminder</a>
                                            @else
                                                <a href="#/" class="btn btn-info btn-xs disabled">Send a reminder</a>
                                            @endif
                                            </td>
                                        </tr>
                                    @empty
                                         <tr>
                                            <td colspan="3" align="center">
                                                No Data Found!
                                            </td>
                                        </tr>
                                    @endforelse
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <td colspan="3" align="center">
                                                <a href="{{ url('invoices/list') }}?status=overdue" target="_blank">See all overdue invoices</a>
                                            </td>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="card m-b-30">
                        <div class="card-body">
                            <h4 class="mt-0 header-title">Partial or Paid Invoices & Bills</h4>
                        
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th width="10%">ID</th>
                                            <th>Customer</th>
                                            <th class="text-right">Amount Due</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @forelse($paid_invoices as $invoice)
                                        <tr>
                                            <td>{{ strtoupper($invoice->identifier) }}</td>
                                            <td>{{ $invoice->customer_profile->name }}</td>
                                            <td class="text-right">৳{{ number_format($invoice->getAmountDue(),2) }}</td>
                                        </tr>
                                    @empty
                                         <tr>
                                            <td colspan="3" align="center">
                                                No Data Found!
                                            </td>
                                        </tr>
                                    @endforelse
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <td colspan="3" align="center">
                                                <a href="{{ url('invoices/list') }}" target="_blank">See all  invoices</a>
                                            </td>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            @endif

            <div class="row mt-20">
                @if(isSuperAdmin())
                <div class="col-md-6">
                    <div class="card m-b-30">
                        <div class="card-body">
                            <h4 class="mt-0 header-title">Recent Owners</h4>
                        
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Email</th>
                                            <th width="20%">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @forelse($owners as $owner)
                                        <tr>
                                            <td>{{ $owner->name }}</td>
                                            <td>{{ $owner->email }}</td>
                                            <td>
                                                {{-- Activate/Deactivate --}}
                                                <a href="#" data-id="{{$owner->id}}" data-action="{{ url('users/change-active') }}" data-message="Are you sure, You want to {{$owner->active ? 'deactivate' : 'activate' }} this user?" class="btn btn-xs btn-{{$owner->active ? 'danger':'info'}} alert-dialog" title="{{$owner->active ? 'Deactivate User':'Activate User'}}"><i class="fa fa-{{$owner->active ? 'ban':'check'}}"></i></a>

                                                {{-- Delete --}}
                                                <a href="#" data-id="{{$owner->id}}" data-action="{{ url('users/delete') }}" data-message="Are you sure, You want to delete this user?" class="btn btn-danger btn-xs alert-dialog" title="Delete User"><i class="fa fa-trash white"></i></a>
                                            </td>
                                        </tr>
                                    @empty
                                         <tr>
                                            <td colspan="3" align="center">
                                                No Data Found!
                                            </td>
                                        </tr>
                                    @endforelse
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <td colspan="3" align="center">
                                                <a href="{{ url('owners') }}" target="_blank">See all owners</a>
                                            </td>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                @endif

                @if(!isCustomer())
                <div class="col-md-6">
                    <div class="card m-b-30">
                        <div class="card-body">
                            <h4 class="mt-0 header-title">Recent Customers</h4>
                        
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Email</th>
                                            <th width="20%">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @forelse($customer_profiles as $customer_profile)
                                        <tr>
                                            <td>{{ $customer_profile->name }}</td>
                                            <td>{{ $customer_profile->email ? $customer_profile->email : 'N/A' }}</td>
                                            <td>
                                                {{-- Activate/Deactivate --}}
                                                <a href="#" data-id="{{$customer_profile->id}}" data-action="{{ url('users/change-active') }}" data-message="Are you sure, You want to {{$customer_profile->active ? 'deactivate' : 'activate' }} this user?" class="btn btn-xs btn-{{$customer_profile->active ? 'danger':'info'}} alert-dialog" title="{{$customer_profile->active ? 'Deactivate User':'Activate User'}}"><i class="fa fa-{{$customer_profile->active ? 'ban':'check'}}"></i></a>

                                                {{-- Delete --}}
                                                <a href="#" data-id="{{$customer_profile->id}}" data-action="{{ url('users/delete') }}" data-message="Are you sure, You want to delete this user?" class="btn btn-danger btn-xs alert-dialog" title="Delete User"><i class="fa fa-trash white"></i></a>
                                            </td>
                                        </tr>
                                    @empty
                                         <tr>
                                            <td colspan="3" align="center">
                                                No Data Found!
                                            </td>
                                        </tr>
                                    @endforelse
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <td colspan="3" align="center">
                                                <a href="{{ url('customer-profiles') }}" target="_blank">See all Customers</a>
                                            </td>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                @endif
            </div>
        </div>
    </div>
</div>

@include('invoices.invoice_remind_modal')
@include('invoices.invoice_remind_success_modal')

@endsection

@section('custom-script')
<script>
function openRemindInvoiceModal(id) 
{
    $('#ajaxloader').removeClass('hide');
    $.ajax({
        url:'{{ url('invoices/fetch-invoice') }}/'+id,
        method:'POST',
        dataType:'JSON',
        success:function(response) {
            if(response.type == 'error') {
                toastMsg(response.message, response.type);
            }else {

                $('#invoiceRemindModal .mail-from').text(response.owner_email);
                $('#invoiceRemindModal .mail-to').text(response.customer_email);
                $('#invoiceRemindModal input[name=invoice_id]').val(response.id);
                $('#invoiceRemindModal .btn-preview').attr('href', '{{ url('invoice-reminder-preview') }}/'+response.identifier);
            }
        },
        complete:function() {
            $('#ajaxloader').addClass('hide');
            $('#invoiceRemindModal').modal();
        },
        error:function(xhr, ajaxOptions, thrownError) {
            alert(xhr.status + ' ' + thrownError);
        }
    });
}

function sendInvoiceReminder()
{
    $('#ajaxloader').removeClass('hide');
    
    $.ajax({
        url:'{{ url('invoices/send-reminder') }}',
        method:'POST',
        data:$('#invoice-reminder-form').serialize(),
        success:function(response) {
            toastMsg(response.message, response.type);

            if(response.type == 'success') {
                $('#invoiceRemindModal').modal('hide');

                setTimeout(function() {
                    $('#invoiceRemiderSuccessModal').modal('show');
                }, 1000);
            }
        },
        complete:function() {
            $('#ajaxloader').addClass('hide');
        },
        error:function(xhr, ajaxOptions, thrownError) {
            alert(xhr.status + ' ' + thrownError);
        }
    })
}
</script>
@endsection
