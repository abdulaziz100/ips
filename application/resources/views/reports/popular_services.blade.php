@extends('layouts.master')

@section('title') Popular Services @endsection 

@section('content')
<div class="row">
    <div class="col-sm-12">
        <div class="card m-b-30">
        	<div class="card-header">
                <h4 class="card-title">Popular Services</h4> 
            </div>

        	<div class="card-body">
        		
        		<div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Service Name</th>
                                <th width="14%" class="text-right">Service Price</th>
                                <th width="14%" class="text-right">Number of Order</th>
                                <th width="20%" class="text-right">Customer Order Amount</th>
                            </tr>
                        </thead>
                        <tbody>  
                        @forelse($services as $service)
                            <tr>
                                <td>{{ $service->title }}</td>
                                <td class="text-right">{{ number_format($service->promotional_price,2) }}</td>
                                <td class="text-right">{{ $service->total_products }}</td>
                                <td class="text-right">{{ number_format(($service->total_products * $service->promotional_price), 2) }}</td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="7" align="center">No Data Found!</td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                </div>

                @if(!empty($services) && $services->isNotEmpty()) 
                <div class="btn-group margin-top-20">
                	<a href="{{ url('reports/popular-services/print') }}" class="btn btn-primary" target="_blank"><i class="fa fa-print" aria-hdden="true"></i> Print</a>
                	<a href="{{ url('reports/popular-services/pdf') }}" class="btn btn-primary" target="_blank"><i class="fa fa-file-pdf-o" aria-hidden="true"></i> PDF</a>
                </div>
                @endif
        	</div>
        </div>	
    </div>
</div>  	
@endsection

@section('custom-style')

@endsection

@section('custom-script')

@endsection
