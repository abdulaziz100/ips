@extends('layouts.master')

@section('title') Customerwise Income Report @endsection 

@section('content')
<div class="row">
    <div class="col-sm-12">
        <div class="card m-b-30">
            <div class="card-header">
                <h4 class="card-title">Customerwise Income Report</h4>
            </div>
            <div class="card-body">
                {!! Form::open(array('url' => Request::url(), 'role' => 'form','id'=>'income-report-form')) !!}
                    <div class="row">
                        @if(isSuperAdmin() || isOwner() || isAgent())
                        <div class="col-md-4">
                            <div class="form-group">
                                {!! Form::select('customer_profile_id',$customer_profiles, Request::input('customer_profile_id'),['class'=>'form-control chosen-select']) !!}
                            </div>
                        </div>
                        @endif
                        <div class="{{ isCustomer() ? 'col-sm-offset-4 ' :'' }}col-sm-4">
                            <div class="form-group">
                                <div id="reportrange" class="pull-right" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
                                    <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
                                    <span></span> <b class="caret"></b>
                                </div>
                            </div>
                            {!! Form::hidden('date_range', Request::input('date_range')) !!}
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group pull-right">
                                <button class="btn btn-default"> Generate Report</button>
                            </div>
                        </div>
                    </div>
                {!! Form::close() !!}
                
                @if(!empty($from_date) && !empty($to_date))
                    <h3><strong>Customerwise Income Report from {{ $from_date }} to {{ $to_date }}</strong></h3>
                @endif

                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Email</th>
                                <th width="13%">Member Since</th>
                                <th width="4%" class="text-center"><span data-toggle="tooltip" title="Total Number of Invoices">T.N.I</span></th>
                                <th width="10%" class="text-right"><span data-toggle="tooltip" title="Total Invoice Amount">T.I.A</span></th>
                                <th width="10%" class="text-right"><span data-toggle="tooltip" title="Total Invoice Paid Amount">T.I.P.A</span></th>
                                <th width="10%" class="text-right"><span data-toggle="tooltip" title="Total Invoice Due Amount">T.I.D.A</span></th>
                            </tr>
                        </thead>
                        <tbody>  
                        @forelse($invoices as $invoice)
                            <tr>
                                <td>{{ $invoice->name }}</td>
                                <td>{{ $invoice->email }}</td>
                                <td class="text-center">{{ Carbon::parse($invoice->created_at)->format('d M, Y') }}</td>
                                <td class="text-center">{{ $invoice->total_invoice }}</td>
                                <td class="text-right">{{ number_format(round($invoice->total_amount, 2),2) }}</td>
                                <td class="text-right">{{number_format(round($invoice->total_paid_amount, 2),2) }}</td>
                                <td class="text-right">{{number_format(round(($invoice->total_amount - $invoice->total_paid_amount), 2),2) }}</td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="5" align="center">No Data Found!</td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                </div>
         
                <div class="btn-group margin-top-20">
                    <a href="{{ url('reports/customerwise-income/print').(count(Request::all()) ? '?customer_profile_id='.Request::input('customer_profile_id').'&date_range='.Request::input('date_range') : '') }}" class="btn btn-primary" target="_blank"><i class="fa fa-print" aria-hdden="true"></i> Print</a>
                    <a href="{{ url('reports/customerwise-income/pdf').(count(Request::all()) ? '?customer_profile_id='.Request::input('customer_profile_id').'&date_range='.Request::input('date_range') : '') }}" class="btn btn-primary" target="_blank"><i class="fa fa-file-pdf-o" aria-hidden="true"></i> PDF</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('custom-style')
{{-- Daterange Picker --}}
{!! Html::style($assets . '/plugins/daterangepicker/daterangepicker-bs3.css') !!}
@endsection

@section('custom-script')
{{-- Date rangepicker --}}
{!! Html::script($assets . '/plugins/daterangepicker/moment.min.js') !!}
{!! Html::script($assets . '/plugins/daterangepicker/daterangepicker.js') !!}

<script>
(function() {

	@if(!empty($from_date) && !empty($to_date))
	var start = moment('{{ $from_date }}');
	var end = moment('{{ $to_date }}');
	@else
	var start = moment().subtract(7, 'days');
    var end = moment();
    @endif

    function cb(start, end) {
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        $('input[name=date_range]').val(start.format('YYYY-MM-DD')+ ' - ' + end.format('YYYY-MM-DD'));
    }

    $('#reportrange').daterangepicker({
        startDate: start,
        endDate: end,
        ranges: {
           'Today': [moment(), moment()],
           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
           'This Month': [moment().startOf('month'), moment().endOf('month')],
           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    }, cb);

	cb(start, end);
})();
</script>
@endsection
