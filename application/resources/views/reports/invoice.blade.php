@extends('layouts.master')

@section('title') Invoice Report @endsection 

@section('content')
<div class="row">
    <div class="col-sm-12">
        <div class="card m-b-30">
        	<div class="card-header">
                <h4 class="card-title">Invoice Report</h4> 
            </div>

        	<div class="card-body">
        		{!! Form::open(array('url' => Request::url(), 'role' => 'form','id'=>'income-report-form')) !!}
        			<div class="row">
        				<div class="offset-sm-4 col-sm-4">
        					<div id="reportrange" class="pull-right" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
        					    <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
        					    <span></span> <b class="caret"></b>
        					</div>
        					{!! Form::hidden('date_range', Request::input('date_range')) !!}
        				</div>
        				<div class="col-sm-4">
        					<div class="form-group pull-right">
        						<button class="btn btn-default"> Generate Report</button>
        					</div>
        				</div>
        			</div>
        		{!! Form::close() !!}
        		
        		@if(!empty($from_date) && !empty($to_date))
        		<h3><strong>Invoice Report from {{ $from_date }} to {{ $to_date }}</strong></h3>
        		@endif
        		<div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th width="12%">Date</th>
                                <th width="14%">Invoice ID</th>
                                <th>Customer</th>
                                <th width="9%" align="right">Amount</th>
                                <th width="6%">Status</th>
                            </tr>
                        </thead>
                        <tbody>  
                        @forelse($invoices as $invoice)
                            <tr>
                                <td>{{ Carbon::parse($invoice->invoice_date)->format('d M, Y') }}</td>
                                <td>{{ strtoupper($invoice->identifier) }}</td>
                                <td>{{ $invoice->customer_profile->name }}</td>
                                <td>{{ number_format($invoice->total,2) }}</td>
                                <td>{{ ucfirst($invoice->status) }}</td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="7" align="center">No Data Found!</td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                </div>

                @if(!empty($invoices) && $invoices->isNotEmpty()) 
                <div class="btn-group margin-top-20">
                	<a href="{{ url('reports/invoice/print').(count(Request::all()) ? '?date_range='.Request::input('date_range') : '') }}" class="btn btn-primary" target="_blank"><i class="fa fa-print" aria-hdden="true"></i> Print</a>
                	<a href="{{ url('reports/invoice/pdf').(count(Request::all()) ? '?date_range='.Request::input('date_range') : '') }}" class="btn btn-primary" target="_blank"><i class="fa fa-file-pdf-o" aria-hidden="true"></i> PDF</a>
                </div>
                @endif
        	</div>
        </div>	
    </div>
</div>  	
@endsection

@section('custom-style')
{{-- Daterange Picker --}}
{!! Html::style($assets . '/plugins/daterangepicker/daterangepicker-bs3.css') !!}
@endsection

@section('custom-script')
{{-- Date rangepicker --}}
{!! Html::script($assets . '/plugins/daterangepicker/moment.min.js') !!}
{!! Html::script($assets . '/plugins/daterangepicker/daterangepicker.js') !!}

<script>
(function() {

	@if(!empty($from_date) && !empty($to_date))
	var start = moment('{{ $from_date }}');
	var end = moment('{{ $to_date }}');
	@else
	var start = moment().subtract(7, 'days');
    var end = moment();
    @endif

    function cb(start, end) {
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        $('input[name=date_range]').val(start.format('YYYY-MM-DD')+ ' - ' + end.format('YYYY-MM-DD'));
    }

    $('#reportrange').daterangepicker({
        startDate: start,
        endDate: end,
        ranges: {
           'Today': [moment(), moment()],
           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
           'This Month': [moment().startOf('month'), moment().endOf('month')],
           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    }, cb);

	cb(start, end);
})();
</script>
@endsection
