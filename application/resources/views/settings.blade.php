@extends('layouts.master')

@section('title') Settings @endsection

@section('content')
<div class="row">
	<div class="col-sm-12">
		<div class="card m-b-30"> 
			<div class="card-header">
				<h4 class="card-title">General settings</h4>
			</div>

			{!! Form::open(array('url' => 'settings', 'role' => 'form', 'files' => true)) !!}
			<div class="card-body">
				<div class="form-group">
		            <label for="logo" class="control-label">Company Logo {!! validation_error($errors->first('logo'),' logo', true) !!}</label> <br>
		            <div class="fileinput {{empty($options['logo']) ? 'fileinput-new':'fileinput-exists'}}" data-provides="fileinput">
		               <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
		                  <img alt="Invoice Logo" src="{{ $assets . '/images/avatar/profile.svg' }}">
		               </div>
		               <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;">
		                  @if(!empty($options['logo']) && @getimagesize(url($options['logo'])))
		                    <img src="{{url($options['logo'])}}" alt="Employee logo">
		                  @endif
		               </div>
		               <div>
		                  <span class="btn btn-default btn-file">
		                  <span class="fileinput-new">Select logo</span>
		                  <span class="fileinput-exists">Change</span>
		                  <input type="file" name="logo" id='logo'>
		                  </span>
		                  <a href="#" class="btn btn-default btn-remove fileinput-exists" data-dismiss="fileinput">Remove</a>
		               </div>
		            </div><!-- end fileinput -->
		        </div>
		        <div class="row margin-top-20">
		        	<div class="col-md-6">
		        		<div class="form-group">
		        			<label for="payment_terms" class="control-label"> Payment Terms {!! validation_error($errors->first('payment_terms'),'payment_terms') !!} </label>
		        			{!! Form::select('payment_terms',config('constants.payment_due'),old('payment_terms') ? old('payment_terms') : (!empty($options['payment_terms']) ? $options['payment_terms'] : ''),['class'=>'form-control chosen-select']) !!}
		        		</div>
		        	</div>
		        	<div class="col-md-6">
		        		<div class="form-group">
		        			<label for="invoice_title" class="control-label">Default Title {!! validation_error($errors->first('invoice_title'),'invoice_title') !!} </label>
		        			{!! Form::text('invoice_title',old('invoice_title') ? old('invoice_title'): (!empty($options['invoice_title']) ? $options['invoice_title'] : 'Invoice'),['class'=>'form-control','placeholder'=>'Default Title', 'id' => 'invoice_title']) !!}
		        			<small class="text-muted">The default title for all invoices. You can change this on each invoice.</small>
		        		</div>
		        	</div>
		        </div>

		        <div class="row margin-top-20">
		        	<div class="col-md-6">
		        		<div class="form-group">
		        			<label for="invoice_subheading" class="control-label"> Default subheading </label>
		        			{!! Form::text('invoice_subheading',old('invoice_subheading') ? old('invoice_subheading'):(!empty($options['invoice_subheading']) ? $options['invoice_subheading'] : ''),['class'=>'form-control','placeholder'=>'Default subheading', 'id' => 'invoice_subheading']) !!}
		        			<small class="text-muted">This will be displayed below the title of each invoice. Useful for things like ABN numbers.</small>
		        		</div>
		        	</div>
		        	<div class="col-md-6">
		        		<div class="form-group">
		        			<label for="invoice_footer" class="control-label"> Default footer </label>
		        			{!! Form::text('invoice_footer',old('invoice_footer') ?old('invoice_footer'):(!empty($options['invoice_footer']) ? $options['invoice_footer'] : ''),['class'=>'form-control','placeholder'=>'Default footer', 'id' => 'invoice_footer']) !!}
		        			<small class="text-muted">This will be displayed at the bottom of each invoice.</small>
		        		</div>
		        	</div>
		        </div>
		        <div class="row margin-top-20">
		        	<div class="col-md-6">
		        		<div class="form-group">
		        			<label for="invoice_date_format" class="control-label"> Date Format {!! validation_error($errors->first('invoice_date_format'),'invoice_date_format') !!}</label>
		        			{!! Form::select('invoice_date_format',config('constants.invoice_date_format'), old('invoice_date_format') ? old('invoice_date_format') : (!empty($options['invoice_date_format']) ? $options['invoice_date_format'] : ''),['class'=>'form-control chosen-select', 'id' => 'invoice_date_format']) !!}
		        			<small class="text-muted">The date format you select here will be reflected on all Invoices, payment's receipts.</small>
		        		</div>
		        	</div>
		        </div>
			</div><!-- /.card-body -->
			<div class="card-footer">
				{!! Form::hidden('file_remove', 'false') !!}
				<button class="btn btn-info">
					<i class="fa fa-save"></i> Save
				</button>
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
	
@endsection

@section('custom-style')
{{-- Jasny Bootstrap --}}
{!! Html::style($assets. '/plugins/jasny-bootstrap/jasny-bootstrap.min.css') !!}
@endsection

@section('custom-script')
{{-- Jasny Bootstrap --}}
{!! Html::script($assets. '/plugins/jasny-bootstrap/jasny-bootstrap.min.js') !!}

<script>
	(function() {
		$('.btn-remove').on('click', function() {
	        $('input[name=file_remove]').val(true);
	    });
	})();
</script>
@endsection