<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>
      @hasSection ('title')
          @yield('title') - {{env('APP_NAME')}}
      @else
          {{ env('APP_NAME') }}
      @endif
    </title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link media="all" rel="stylesheet" href="{{ url('assets') }}/css/bootstrap.min.css" >
    <link media="all" rel="stylesheet" href="{{ url('assets') }}/css/custom.css">

    @yield('custom-style')

  </head>
  <body>
    <div class="container">
      @yield('content')
    </div>
  </body>
</html>
