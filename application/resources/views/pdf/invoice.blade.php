@extends('pdf.master')

@section('title') Invoice @endsection 

@section('content')
{{-- Panel Invoice --}}
<div class="panel panel-default panel-invoice margin-top-40">
    <div class="panel-body">

        {{-- Table --}}
        <table class="table">
            <tbody>
                <tr>
                    <td width="30%">
                        <img width="200" src="{{ getCompanyLogoPath($company->id) }}" alt="Company Logo">
                    </td>
                    <td width="50%" align="right">
                        <h1 class="no-margin">{{ $invoice->title }}</h1>
                        @if($invoice->invoice_summary)
                            <span class="text-muted">{{ $invoice->invoice_summary }}</span>
                        @endif

                        <div class="company-info text-right margin-top-20">
                            <strong>{{ $company->name }}</strong> <br>
                            @if($company->address)
                            <span>{{ $company->address }} <br></span>
                            @endif
                            @if($company->state || $company->city || $company->zip_code)
                            <span>{{ ($company->state ? $company->state .', ' : '') . ($company->city ? $company->city .' ' : '') .($company->zip_code ? $company->zip_code : '') }}</span> <br>
                            @endif

                            <span>{{ $company->country_obj->name }}</span> <br>

                            @if($company->primary_phone || $company->alt_phone || $company->website)
                                
                                <div class="margin-top-20">
                                @if($company->primary_phone)
                                    <span>Phone: {{ $company->primary_phone }}</span> <br>
                                @endif

                                @if($company->alt_phone)
                                    <span>Alt. Phone: {{ $company->alt_phone }}</span> <br>
                                @endif

                                @if($company->website)
                                    <span>{{ $company->website }}</span> <br>
                                @endif
                                </div>
                            @endif
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>
        {{-- /Table --}}

        <hr>

        {{-- Table --}}
        <table class="table">
            <tr>
                <td width="45%">
                    <div class="details-info">
                        <span>Bill To</span> <br>
                        <strong>{{ $invoice->customer_profile->name }} </strong><br><br>
                        @if($invoice->customer_profile->address)
                            {{ $invoice->customer_profile->address }} <br>
                        @endif

                        @if($invoice->customer_profile->email)
                            {{ $invoice->customer_profile->email }} <br>
                        @endif
                    </div>
                </td>
                <td width="55%" align="right">
                    <table class="table table-no-border table-sm">
                        <tbody>
                            <tr>
                                <th width="50%" class="text-right">Invoice Number:</th>
                                <td width="50%">{{ strtoupper($invoice->identifier) }}</td>
                            </tr>
                            @if($invoice->po_number)
                            <tr>
                                <th width="50%" class="text-right">P.O/S.O Number:</th>
                                <td width="50%">{{ $invoice->po_number }}</td>
                            </tr>
                            @endif
                            <tr>
                                <th width="50%" class="text-right">Invoice Date:</th>
                                <td width="50%">{{ Carbon::parse($invoice->invoice_date)->format(getOption($company->id, 'invoice_date_format', 'Y-m-d'))  }}</td>
                            </tr>
                            <tr>
                                <th width="50%" class="text-right">Payment Due:</th>
                                <td width="50%">{{ Carbon::parse($invoice->payment_due)->format(getOption($company->id, 'invoice_date_format', 'Y-m-d'))  }}</td>
                            </tr>
                            <tr class="bg-gray">
                                <th width="50%" class="text-right">Amount Due(BDT):</th>
                                <td width="50%">{{ number_format($invoice->getAmountDue(), 2) }}</td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </table>
        {{-- /Table --}}

        <table class="table table-borded table-striped table-hover margin-top-20">
            <thead class="thead-dark">
                <tr>
                    <th>Items</th>
                    <th width="12%" class="text-center">Qauntity</th>
                    <th width="12%" class="text-right">Price</th>
                    <th width="12%" class="text-right">Amount</th>
                </tr>
            </thead>
            <tbody>
            @forelse($invoice_details as $invoice_detial)
                <tr>
                    <td>
                        <strong>{{ $invoice_detial->service_name }}</strong>
                        @if($invoice_detial->service_description)
                        <p>{{ $invoice_detial->service_description }}</p>
                        @endif
                    </td>
                    <td class="text-center">{{ $invoice_detial->quantity }}</td>
                    <td class="text-right">{{ number_format($invoice_detial->rate, 2) }}</td>
                    <td class="text-right">{{ number_format($invoice_detial->amount, 2) }}</td>
                </tr>
            @empty
                <tr>
                    <td colspan="4" align="center">You have not added any items.</td>
                </tr>
            @endforelse
            </tbody>
            <tfoot>
                @if($invoice->vat_total)
                <tr>
                    <td colspan="3" align="right"><strong>Sub Total:</strong></td>
                    <td class="text-right">{{ number_format($invoice->sub_total, 2) }}</td>
                </tr>
                <tr>
                    <td colspan="3" align="right"><strong>Vat:</strong></td>
                    <td class="text-right">{{ number_format($invoice->vat_total, 2) }}</td>
                </tr>
                @endif
                <tr>
                    <td colspan="3" align="right"><strong>Total:</strong></td>
                    <td class="text-right">{{ number_format($invoice->total, 2) }}</td>
                </tr>
                @if(!empty($payments) && $payments->isNotEmpty())
                @foreach($payments as $payment)
                <tr class="payment-row">
                    <td colspan="3" align="right">Payment on {{ Carbon::parse($payment->payment_date)->format(getOption($company->id, 'invoice_date_format', 'Y-m-d')) }} using {{ str_replace('-', ' ', $payment->payment_mood) }}:</td>
                    <td class="text-right">{{ number_format($payment->amount, 2) }}</td>
                </tr>
                @endforeach
                @endif
                <tr>
                    <td colspan="3" align="right"><strong>Amount Due(BDT):</strong></td>
                    <td class="text-right">{{ number_format($invoice->getAmountDue(), 2) }}</td>
                </tr>
            </tfoot>
        </table>
    
        @if($invoice->notes)
            <p class="margin-top-20"><strong>Notes:</strong> <br>{{ $invoice->notes }}</p>
        @endif
        @if($invoice->invoice_footer)
            <p class="text-center margin-top-20">{{ $invoice->invoice_footer }}</p>
        @endif   
    </div>
</div>
{{-- /Panel Invoice --}}
@endsection

@section('custom-style')
<style>
    .container {
        max-width: 973px;
    }
    .panel {
        border:0;
    }
    .panel-body{
        padding: 5px;
    }
    hr {
        margin: 10px 0;
    }
    .table {
        margin: 0;
    }
    table td {
        border-top: 1px solid transparent !important;
    }
    .well {
        margin-bottom: 0;
    }
</style>
@endsection
 