@extends('pdf.master')

@section('title') Popular Services @endsection

@section('content') 
<div class="panel panel-default margin-top-20">
    <div class="panel-heading">
        <div class="panel-title">Report of Poplular Services</div>
    </div>
    <div class="panel-body">
	    <table class="table table-bordered">
	        <thead>
	            <tr>
	                <th>Service Name</th>
	                <th width="20%" class="text-right">Service Price</th>
	                <th width="20%" class="text-right">Number of Order</th>
	                <th width="34%" class="text-right">Customer Order Amount</th>
	            </tr>
	        </thead>
	        <tbody>  
	        @forelse($services as $service)
	            <tr>
	                <td>{{ $service->title }}</td>
	                <td class="text-right">{{ number_format($service->promotional_price,2) }}</td>
	                <td class="text-right">{{ $service->total_products }}</td>
	                <td class="text-right">{{ number_format(($service->total_products * $service->promotional_price), 2) }}</td>
	            </tr>
	        @empty
	            <tr>
	                <td colspan="3" align="center">No Data Found!</td>
	            </tr>
	        @endforelse
	        </tbody>
	    </table>
	</div>
</div>
@endsection

@section('custom-style')
	<style>
		.container {
        	max-width: 973px;
	    }
	    .table {
	        margin: 0;
	    }
	    table td {
	        border-top: 1px solid transparent !important;
	    }
	</style>
@endsection