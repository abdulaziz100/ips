<?php 

return [

	'app_name'	=> 'IPS',

	'user_type'	=> [
		'super-admin'	=> 'Super Admin',
		'owner'			=> 'Owner',
		'agent'			=> 'Agent',
		'customer'		=> 'Customer',
	],

	'type_of_business' => [
		'business_type_1'	=> 'Business Type 1',
		'business_type_2'	=> 'Business Type 2',
		'business_type_3'	=> 'Business Type 3',
	],

	'type_of_organization' => [
		'organization_type_1'	=> 'Organization Type 1',
		'organization_type_2'	=> 'Organization Type 2',
		'organization_type_3'	=> 'Organization Type 3',
	],

	'payment_due'	=> [
		0 	 => 'On Receipt',
		7  	 => 'Within 7 Days',
		14   => 'Within 14 Days',
		30   => 'Within 30 Days',
		45   => 'Within 45 Days',
		60   => 'Within 60 Days',
		90   => 'Within 90 Days',
	],

	'invoice_status'	=> [
		'draft'		=>	'Draft',
		'unsent'	=>	'Unsent',
		'sent'		=>	'Sent',
		'viewed'	=>  'Viewed',
		'partial'	=>  'Partial',
		'overdue'	=>  'Overdue',
		'paid'		=>	'Paid',
	],

	'recurring_invoice_status'	=> [
		'draft'		=>	'Draft',
		'active'	=>	'Active',
		'end'		=>	'End',
	],

	'invoice_status_class' => [
		'draft'		=> 'warning',
		'viewed'	=> 'warning',
		'unsent'	=> 'primary',
		'sent'		=> 'info',
		'active'	=> 'info',
		'overdue'	=> 'danger',
		'partial'	=> 'default',
		'paid'		=> 'success',
		'end'		=> 'success',
	],

	'payment_mood' => [
		'bank-payment'	 => 'Bank Payment',
		'cash'			 => 'Cash',
		'cheque'		 => 'Cheque',
		'mobile-banking' => 'Mobile Banking',
		'online-banking' => 'Online Banking',
		'other'			 => 'Other',
	],

	'frequency'	=> [
		'daily'		=> 'Daily',
		'weekly'	=> 'Weekly',
		'monthly'	=> 'Monthly',
		'yearly'	=> 'Yearly',
	],

	'day_of_week'	=> [
		0	=> 'Sunday',
		1	=> 'Monday',
		2	=> 'Tuesday',
		3	=> 'Wednesday',
		4	=> 'Thursday',
		5	=> 'Friday',
		6	=> 'Saturday',
	],

	'invoice_date_format'	=> [
		'Y-m-d'		=> date('Y-m-d'),
		'F d, Y'	=> date('F d, Y'),
		'd/m/Y'		=> date('d/m/Y'),
		'd F, Y'	=> date('d F, Y'),
	],

	'default_settings' => [
		'payment_terms'			=> 0,
		'invoice_title'			=> 'Invoice',
		'invoice_date_format'	=> 'Y-m-d',
	]
];