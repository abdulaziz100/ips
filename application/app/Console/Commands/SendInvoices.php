<?php

namespace App\Console\Commands;
ini_set('max_execution_time', 300000);
use Helpers\Classes\EmailSender;
use App\Helpers\Classes\InvoiceSender;
use App\Helpers\Classes\ReminderSender;
use App\Invoice;
use Carbon;
use Illuminate\Console\Command;

class SendInvoices extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'invoice:send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send invoices to users';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $recurring_invoices = Invoice::whereType('recurring')->whereStatus('active')->get();

        $invoice_no = 0;
        if($recurring_invoices->isNotEmpty()) {
            foreach($recurring_invoices as $recurring_invoice) {
                $invoice_schedule = $recurring_invoice->invoice_schedule;
                
                $frequency = $invoice_schedule->frequency;
                $day_of_month = $invoice_schedule->day_of_month;
                $day_of_week = $invoice_schedule->day_of_week;
                $month_of_year = $invoice_schedule->month;

                $startDate = Carbon::parse($invoice_schedule->start);
                $untilDate = Carbon::parse($invoice_schedule->until);
                $toDate = Carbon::now();

                $year = $toDate->year;
                $month = $toDate->month;
                $day = $toDate->day;
                $dayOfWeek = $toDate->dayOfWeek;

                $isStartDate = false;
                if($startDate->diffInDays($toDate) == 0) {
                    $isStartDate = true;
                }

                $isValidDate = false;

                if($invoice_schedule->until) {
                    $isValidDate = $toDate->between($startDate, $untilDate);
                    // $isValidDate = ($startDate->diffInDays($toDate, false) >= 0) && ($toDate->diffInDays($untilDate, false) >= 0);
                }else {
                    $isValidDate = $startDate->diffInDays($toDate, false) >= 0 ? true:false;
                }

                if($isValidDate) {
                    
                    if($frequency == 'daily') {
                        
                        InvoiceSender::send($recurring_invoice);
                            $invoice_no++;

                    // Check Frequency is weekly
                    }else if($frequency == 'weekly') {
                        
                        if($dayOfWeek == $day_of_week) {
                            InvoiceSender::send($recurring_invoice);
                            $invoice_no++;
                        }

                    // Check Frequency is monthly
                    }else if($frequency == 'monthly') {
                        
                        if($day == $day_of_month) {
                            InvoiceSender::send($recurring_invoice);
                            $invoice_no++;
                        }

                    // Check Frequency is yearly
                    }else if($frequency == 'yearly') {
                        
                        if(($month == $month_of_year) && ($day == $day_of_month)) {
                            InvoiceSender::send($recurring_invoice);
                            $invoice_no++;
                        }

                    }
                }

                // get next week date info
                $nextWeekDate = $toDate->addWeek();
                
                $nextWeekYear = $nextWeekDate->year;
                $nextWeekMonth = $nextWeekDate->month;
                $nextWeekDay = $nextWeekDate->day;
                $nextWeekDayOfWeek = $nextWeekDate->dayOfWeek;

                // Send reminder email
                $isNextWeekStartDate = false;
                if($startDate->diffInDays($nextWeekDate) == 0) {
                    $isNextWeekStartDate = true;
                }

                $isNextWeekValidDate = false;

                if($invoice_schedule->until) {
                    $isNextWeekValidDate = $nextWeekDate->between($startDate, $untilDate);
                    // $isNextWeekValidDate = ($startDate->diffInDays($nextWeekDate, false) >= 0) && ($toDate->diffInDays($untilDate, false) >= 0);
                }else {
                    $isNextWeekValidDate = $startDate->diffInDays($nextWeekDate, false) >= 0 ? true:false;
                }

                if($isNextWeekValidDate) {
                    
                    if($frequency == 'daily') {

                        ReminderSender::send($recurring_invoice);
                            
                    // Check Frequency is weekly
                    }else if($frequency == 'weekly') {
                        
                        if($nextWeekDayOfWeek == $day_of_week) {
                            ReminderSender::send($recurring_invoice);
                        }

                    // Check Frequency is monthly
                    }else if($frequency == 'monthly') {
                        
                        if($nextWeekDay == $day_of_month) {
                            ReminderSender::send($recurring_invoice);
                            
                        }

                    // Check Frequency is yearly
                    }else if($frequency == 'yearly') {
                        
                        if(($nextWeekMonth == $month_of_year) && ($nextWeekDay == $day_of_month)) {
                            ReminderSender::send($recurring_invoice);
                            
                        }
                    }
                }
            }

            // Send Invoices to owner
            $invoices = \DB::table('invoices as i')->join('recurring_invoice_logs as ril', 'i.id', '=', 'ril.generated_invoice_id')
            ->join('users as u', 'i.owner_id', '=', 'u.id') 
            ->where('ril.generated_date', date('Y-m-d'))
            ->where('generated_by', 'command')
            ->select('i.id', 'u.email')
            ->get();

            if($invoices->isNotEmpty()) {
                
                $arr = [];
                foreach($invoices as $invoice) {
                    $arr[$invoice->email][] = $invoice->id;
                }

                foreach($arr as $key=>$value) {
                    
                    $invoices = \App\Invoice::whereIn('id', $value)->get(['identifier', 'customer_profile_id', 'total']);

                    $data = [
                        'blade' => 'owner_invoice_info',
                        'body'  =>  [
                            'invoices' => $invoices,
                        ],
                        'toUser'    =>  $key,
                        'toUserName' =>  '',
                        'subject'   =>  env('APP_NAME') . ' Customer Invoice List',
                    ];

                    EmailSender::send($data);
                }
            }

            $this->info(($invoice_no ? $invoice_no : 'No') .'  Invoice Sent');
        }else {
            $this->error('Invoice schdule info not found!');
        }
    }
}
