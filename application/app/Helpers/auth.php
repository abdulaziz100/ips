<?php

function isSuperAdmin()
{
	if(!Auth::check()){
		return false;
	}

	return Auth::user()->user_type == 'super-admin' ? true : false;
}

function isOwner()
{
	if(!Auth::check()){
		return false;
	}

	return Auth::user()->user_type == 'owner' ? true : false;
}

function isCustomer(){
	if(!Auth::check()){
		return false;
	}

	return Auth::user()->user_type == 'customer' ? true : false;
}

function isAgent()
{
	if(!Auth::check()){
		return false;
	}

	return Auth::user()->user_type == 'agent' ? true : false;
}
