<?php 

namespace App\Helpers\Classes;
ini_set('max_execution_time', 300000);
use Helpers\Classes\EmailSender;

class ReminderSender {
	
	public static function send($recurring_invoice)
	{
        $owner = $recurring_invoice->owner;
        $company = $recurring_invoice->getCompanyOwner()->companies()->first();

        $data = [
            'blade' => 'recurring_invoice_reminder',
            'body'  =>  [
                'invoice'  => $recurring_invoice,
                'owner'    => $owner,
                'company'  => $company,
            ],
            'toUser'    =>  $owner->email,
            'toUserName' =>  $owner->name,
            'subject'   =>  'Reminder for Recurring Invoice #'.$recurring_invoice->identifier,
        ];

        EmailSender::send($data);
	}
}