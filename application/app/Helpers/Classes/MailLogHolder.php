<?php 

namespace App\Helpers\Classes;
ini_set('max_execution_time', 300000);
use App\MailLog;

class MailLogHolder {
	
	public static function add($data)
	{
		$mail_log = new MailLog;
		$mail_log->invoice_id = $data['invoice_id'];
		
		$content  = 'from:'.$data['fromName'] . '\n';
		$content .= 'to:'.$data['toUser'] . '\n';
		
		if(!empty($data['bcc_address'])) {
			$content .= 'bcc:'.$data['bcc_address'] . '\n';
		}

		if(!empty($data['ccs'])) {
			$content .= 'cc:'. implode(',', $data['ccs']) . '\n';
		}

		$content .= 'subject:'. $data['subject'] . '\n';

		if(!empty($data['body']['email_message'])) {
			$content .= 'message:'. $data['body']['email_message'] . '\n';
		}
		
		if(!empty($data['attach_file_path'])) {
			$content .= 'attachment:'. $data['attach_file_path'] . '\n';
		}
		$mail_log->content = $content;
		$mail_log->trigger_by = !empty($data['trigger_by']) ? $data['trigger_by']: 'cron';
		$mail_log->sent_by = !empty($data['sent_by']) ? $data['sent_by'] : null;
		$mail_log->created_at = \Carbon::now();

		if($mail_log->save()) {
			return true;
		}else {
			return false;
		}
	}
}