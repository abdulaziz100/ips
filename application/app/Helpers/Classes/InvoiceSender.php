<?php 

namespace App\Helpers\Classes;
ini_set('max_execution_time', 300000);
use Helpers\Classes\EmailSender;
use App\Helpers\Classes\MailLogHolder;
use App\Invoice;
use App\InvoiceDetail;
use App\RecurringInvoiceLog;
use PDF;

class InvoiceSender {
	
	public static function send($recurring_invoice, $generated_by='command')
	{
		// Insert invoice
        $invoice = new Invoice ;
        $invoice->customer_profile_id = $recurring_invoice->customer_profile_id;
        $invoice->owner_id = $recurring_invoice->owner_id;
        $invoice->title = $recurring_invoice->title;
        $invoice->invoice_summary = $recurring_invoice->invoice_summary;
        $invoice->identifier = Invoice::getIdentifier();
        $invoice->po_number = $recurring_invoice->po_number;
        $invoice->sub_total = $recurring_invoice->sub_total;
        $invoice->vat_total = $recurring_invoice->vat_total;
        $invoice->total = $recurring_invoice->total;
        $invoice->invoice_date = $recurring_invoice->invoice_date;
        $invoice->payment_due = $recurring_invoice->payment_due;
        $invoice->notes = $recurring_invoice->notes;
        $invoice->invoice_footer = $recurring_invoice->invoice_footer;
        $invoice->status = 'sent';
        $invoice->invoice_sent_status = 'sent';

        $invoice->created_by = $recurring_invoice->created_by;
        
        if($invoice->save()) { 

            $recurring_invoice_details = $recurring_invoice->invoice_details;

            // insert invoice details
            foreach($recurring_invoice_details as $recurring_invoice_detail) {

                $invoice_detail = new InvoiceDetail;
                $invoice_detail->invoice_id = $invoice->id;
                $invoice_detail->service_id = $recurring_invoice_detail->service_id;
                $invoice_detail->service_name = $recurring_invoice_detail->service_name;
                $invoice_detail->service_description = $recurring_invoice_detail->service_description;
                $invoice_detail->quantity = $recurring_invoice_detail->quantity;
                $invoice_detail->rate = $recurring_invoice_detail->rate;
                $invoice_detail->vat = $recurring_invoice_detail->vat;
                $invoice_detail->amount = $recurring_invoice_detail->amount;

                $invoice_detail->save();
            }

            // insert data into recurring invoice logs
            $recurring_invoice_log = new RecurringInvoiceLog;
            $recurring_invoice_log->recurring_invoice_id = $recurring_invoice->id; 
            $recurring_invoice_log->generated_invoice_id = $invoice->id; 
            $recurring_invoice_log->generated_date = date('Y-m-d');
            $recurring_invoice_log->generated_by = $generated_by == 'command' ? 'command':'manual';
            $recurring_invoice_log->save();  

            // Send email
            $invoice_sent_option = $recurring_invoice->invoice_sent_option;

            // check whether sent option media is automatic, if it is true then send invoice email
            if($invoice_sent_option->sent_media == 'automatic' && $invoice->customer_profile->active) {

                $owner   = $invoice->getCompanyOwner();
                $company = $owner->companies()->first();
                $is_recurring_invoice = $generated_by == 'command' ? true : false;
            
                $data = [
                    'blade' => 'invoice_email',
                    'body'  =>  [
                        'email_message' => $invoice_sent_option->message,
                        'invoice'  => $invoice,
                        'owner'    => $owner,
                        'company'  => $company,
                        'is_recurring_invoice' => $is_recurring_invoice
                    ],
                    'fromAddress' => $invoice_sent_option->from,
                    'fromName' => $company->name,
                    'toUser'    =>  $invoice_sent_option->to,
                    'toUserName' =>  '',
                    'subject'   =>  'Invoice #'.$invoice->identifier.' from '.$company->name,
                ];

                if($invoice_sent_option->ccs) {
                    $data['ccs'] = explode(',', $invoice_sent_option->ccs);
                }  

                if($invoice_sent_option->copy_myself)  {
                    $data['bcc_address'] = $invoice->owner->email;
                }

                if($invoice_sent_option->is_attached) {
                    
                    $pdf_folder = 'pdf/';

                    // check whether folder already exist if not, create folder
                    if(!file_exists($pdf_folder)) {
                        mkdir($pdf_folder, 0755, true);
                    }
                    $file_name = 'invoice_'.$invoice->id.'_'.date('Y-m-d').'.pdf';
                    $file_path = $pdf_folder . $file_name;

                    if(file_exists($file_path)) {
                        $file_name = 'invoice_'.$invoice->id.'_'.date('Y-m-d').'_'.mt_rand(100, 999).'.pdf';
                        $file_path = $pdf_folder . $file_name;
                    }

                    $invoice_details = $invoice->invoice_details;
                    
                    PDF::loadView('pdf.invoice', compact('invoice', 'invoice_details', 'company'))
                        ->save($file_path);

                    $data['attach_file_path'] = $file_path;
                    $data['attach_file_options'] = [
                        'as'    =>  $file_name,
                        'mime'  =>  'application/pdf'
                    ];
                }

                EmailSender::send($data);

                // Add data into mail_logs table
                $data['invoice_id'] = $invoice->id;
                $data['trigger_by'] = $generated_by == 'command' ? 'cron':'manual';
                $data['sent_by'] = $generated_by == 'command' ? null:$invoice->owner->id;

                MailLogHolder::add($data);
            }
        }
	}
}