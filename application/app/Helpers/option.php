<?php

use App\Option;

function getOption($company_id, $name, $default_value=''){
	
	$option = Option::whereCompanyId($company_id)->whereName($name)->first(['value']);

	return !empty($option->value) ? $option->value : $default_value;

}

function getAllOption($company_id){

	$options = Option::whereCompanyId($company_id)->pluck('value', 'name');

	return $options;
}

function addOrUpdateOption($company_id, $name, $value){

	$option = Option::whereCompanyId($company_id)->whereName($name)->first();

	if(!$option){
		$option = new Option();
		$option->company_id = $company_id;
		$option->name = $name;
	}
	$option->value = $value;

	return $option->save() ? true : false;
}

function deleteOption($name){
	$company_id = Auth::user()->company_id;

	$option = Option::whereCompanyId($company_id)->whereName($name)->first();

	if(!$option){
		//no option found in db to delete
		return false;
	}

	return $option->delete() ? true : false;
}

function setupDefaultSettings($company_id)
{
	$default_settings = config('constants.default_settings');
	foreach($default_settings as $key=>$value) {
		$option = new Option();
		$option->company_id = $company_id;
		$option->name = $key;
		$option->value = $value;
		$option->save();
	}

	return true;
}