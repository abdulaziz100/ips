<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MailLog extends Model
{
    public $timestamps = false;
}
