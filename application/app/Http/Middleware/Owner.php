<?php

namespace App\Http\Middleware;

use Closure;

class Owner
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!isSuperAdmin() && !isOwner()) {

            session()->flash('toast', toastMessage('You are allowed to view this page.', 'error'));

            return redirect('/home');
        }

        return $next($request);
    }
}
