<?php

namespace App\Http\Middleware;

use Closure;

class CompanyOwner
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $owner = \App\Company::getOwner();

        if($owner->companies->isEmpty()) {

            session()->flash('toast', toastMessage('You must create company to view this page', 'error'));

            return redirect('/home');
        }

        return $next($request);
    }
}
