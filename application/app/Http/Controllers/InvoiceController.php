<?php

namespace App\Http\Controllers;
ini_set('max_execution_time', 300);
use App\Invoice;
use Illuminate\Http\Request;

use Helpers\Classes\EmailSender;
use App\Helpers\Classes\MailLogHolder;
use App\Country;
use App\CustomerProfile;
use App\InvoiceDetail;
use App\Payment;
use App\Service;
use App\User;
use Auth, Carbon, Form, PDF, Validator;

class InvoiceController extends Controller
{
    /**
     * For user access control
     */
    public function __construct()
    {
        $this->middleware('agent');
        $this->middleware('company_owner');
    }

    /**
     * Display a listing of the resource.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function invoiceList(Request $request)
    {
        $query = Invoice::leftJoin('recurring_invoice_logs', 'invoices.id', '=', 'recurring_invoice_logs.generated_invoice_id')
            ->whereType('normal');

        $data = [];
        
        if(!isSuperAdmin()) {
            $query->whereCreatedBy($request->user()->id);
        }

        if($request->filled('customer_profile_id')) {
            
            $query->whereCustomerProfileId($request->customer_profile_id);
            $data['customer_profile_id'] = $request->customer_profile_id;
        }

        if($request->filled('status')) {
            
            if($request->status == 'overdue') {
                $query->where(function($query) {
                    $query->whereIn('status', ['unsent', 'sent', 'viewed', 'partial'])
                        ->where('payment_due', '<', date('Y-m-d'));
                });
            }else if(in_array($request->status, ['unsent', 'sent', 'viewed', 'partial'])) {
                $query->where(function($query) use($request) {
                    $query->whereStatus($request->status)
                        ->where('payment_due', '>=', date('Y-m-d'));
                });
            }else {
                $query->whereStatus($request->status);
            }

            $data['status'] = $request->status;
        }

        if($request->filled('date_range')) {
            
            $date_range  = explode(' - ',$request->date_range);
            $from_date = \Carbon::parse($date_range[0])->format('Y-m-d');
            $to_date = \Carbon::parse($date_range[1])->format('Y-m-d');
            $query->whereRaw('invoice_date >= "'.$from_date.'" AND invoice_date <="'.$to_date.'"');

            $data['date_range'] = $request->date_range;
        }

        if($request->filled('identifier')) {
            
            $query->where('identifier', 'LIKE', '%'. trim($request->identifier) .'%');
            
            $data['identifier'] = trim($request->identifier);
        }

        if($request->filled('recurring_invoice_id')) {
            
            $query->where('recurring_invoice_logs.recurring_invoice_id', $request->recurring_invoice_id);

            $data['recurring_invoice_id'] = trim($request->recurring_invoice_id);
        }

        $invoices = $query->select('invoices.*', 'recurring_invoice_logs.id as ril_id')->latest('id')->paginate(10);
        $invoices->paginationSummery = getPaginationSummery($invoices->total(), $invoices->perPage(), $invoices->currentPage());
        
        if($data) {
            $invoices->appends($data);
        }

        $customer_profiles = CustomerProfile::getDropDownList();
        $invoice_statuses = array_prepend(array_except(config('constants.invoice_status'), ['viewed']), 'Select status', '');

        if($request->ajax()) {
            return view('invoices.invoice_card_content', compact('invoices', 'customer_profiles', 'invoice_statuses', 'from_date', 'to_date'))->render();
        }
        

        return view('invoices.index', compact('invoices', 'customer_profiles', 'invoice_statuses', 'from_date', 'to_date'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $customers = CustomerProfile::getDropDownList();
        $services = Service::getDropDownList();
        $countries = Country::getDropDownList();
        $identifier = Invoice::getIdentifier();

        return view('invoices.create', compact('customers', 'services', 'countries', 'identifier'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $total_service_descriptions = count($request->service_description);
        $total_quantities = count($request->quantity);
        $total_prices = count($request->rate);
        $total_vats = count($request->vat);

        $except = ['service_description', 'quantity', 'price', 'vat'];

        if($total_service_descriptions == $total_quantities && $total_quantities == $total_prices && $total_prices == $total_vats) {
            
            $rules = [
                'customer_profile_id' => 'required|integer|min:1',
                'invoice_date'  => 'required|date|date_format:Y-m-d',
                'payment_due'  => 'required|date|after_or_equal:invoice_date|date_format:Y-m-d|',
            ];

            if(!$request->has('invoice_id')) {
                $rules = $rules + [
                    'identifier'    => 'required|string|max:20|unique:invoices',
                ];
            }

            if($request->has('svc_id') || $request->has('service_description') || $request->has('quantity') || $request->has('rate') || $request->has('vat')) {

                for ($i=0; $i < $total_vats; $i++) { 
                    $rules['svc_id.'.$i] = 'required|integer|min:1';
                    $rules['service_description.'.$i] = 'string|max:500|nullable';
                    $rules['quantity.'.$i] = 'required|integer|min:1';
                    $rules['rate.'.$i] = 'required|numeric|min:0';
                    $rules['vat.'.$i] = 'required|integer|min:0';
                }
            }

            $validator = Validator::make($request->all(), $rules);
            
            if($validator->fails()) {
                
                return back()->withInput($request->except($except))->withErrors($validator);
            }else {
                
                $total_quantity = $sub_total_price = $total_vat_amount = 0;

                $service_ids = $request->svc_id;
                $service_descriptions = $request->service_description;
                $quantities = $request->quantity;
                $rates = $request->rate;
                $vats = $request->vat;

                if($request->has('svc_id') || $request->has('service_description') || $request->has('quantity') || $request->has('rate') || $request->has('vat')) {
                    
                    for ($i=0; $i < $total_vats; $i++) { 
                        $total_quantity += $quantities[$i];
                        $sub_total_price += $quantities[$i]*$rates[$i];
                        $total_vat_amount += ($quantities[$i]*$rates[$i]*$vats[$i])/100;
                    }
                }

                // insert or update invoice
                $invoice = !$request->has('invoice_id') ? new Invoice : Invoice::findOrFail($request->invoice_id);
                $invoice_status = !empty($invoice->status) && $invoice->status != 'draft' ? $invoice->status : 'draft';
                $invoice->customer_profile_id = trim($request->customer_profile_id);
                $invoice->owner_id = $request->user()->id;
                $invoice->title = trim($request->title);
                $invoice->invoice_summary = trim($request->invoice_summary);
                if(!$request->has('invoice_id')) {
                    $invoice->identifier = trim($request->identifier);
                }
                $invoice->po_number = trim($request->po_number);
                $invoice->sub_total = $sub_total_price;
                $invoice->vat_total = $total_vat_amount;
                $invoice->total = $sub_total_price + $total_vat_amount;
                $invoice->invoice_date = trim($request->invoice_date);
                $invoice->payment_due = trim($request->payment_due);
                $invoice->notes = trim($request->notes);
                $invoice->invoice_footer = trim($request->invoice_footer);
                $invoice->status = $invoice_status;

                if(!$request->has('invoice_id')) {
                    $invoice->created_by = $request->user()->id;
                    $msg = 'added.';
                }else {
                    $invoice->updated_by = $request->user()->id;
                    $msg = 'updated.';
                }
                
                if($invoice->save()) {

                    if($request->has('invoice_id')) {
                        $invoice = Invoice::findOrFail($request->invoice_id);

                        $invoice->invoice_details()->forceDelete();
                    }
                    
                    if($request->has('svc_id') || $request->has('service_description') || $request->has('quantity') || $request->has('rate') || $request->has('vat')) {
                        
                        // add data to invoice details table

                        for ($i=0; $i < $total_vats; $i++) { 
                            $service = Service::findOrFail($service_ids[$i]);
                            $invoice_detail = new InvoiceDetail;
                            $invoice_detail->invoice_id = $invoice->id;
                            $invoice_detail->service_id = $service->id;
                            $invoice_detail->service_name = $service->title;
                            $invoice_detail->service_description = $service_descriptions[$i];
                            $invoice_detail->quantity = $quantities[$i];
                            $invoice_detail->rate = $rates[$i];
                            $invoice_detail->vat = $vats[$i];
                            $invoice_detail->amount = $quantities[$i]*$rates[$i];

                            $invoice_detail->save();
                        }
                    }

                    $message = toastMessage('Invoice has been '.$msg);
                }else{
                    $message = toastMessage('Invoice has not been '.$msg, 'error');
                }

                // redirect 
                $request->session()->flash('toast',$message);

                return redirect('invoices/list');
            }
        }else {
            session()->flash('toast', toastMessage('Please add item correctly', 'error'));

            return back()->withInput($request->except($except));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Invoice  $invoice
     * @return \Illuminate\Http\Response
     */
    public function show(Invoice $invoice)
    {
        $invoice_details = $invoice->invoice_details;
        $customer_profile = $invoice->customer_profile;
        $customer_profile_html = '<strong>'.$customer_profile->name.'</strong>';
        if($customer_profile->email) {
            $customer_profile_html .= '<br/>'.$customer_profile->email;
        }

        $customer_profile_html .= '<br/><br/><strong>Tel: </strong>'.$customer_profile->primary_phone;

        $owner = $invoice->getCompanyOwner() ;
        $company = $owner->companies()->first();
        $amount_due = $invoice->getAmountDue();
        $payments = $invoice->payments;

         return view('invoices.show', compact('invoice','invoice_details', 'customer_profile', 'customer_profile_html', 'owner', 'company', 'amount_due', 'payments'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Invoice  $invoice
     * @return \Illuminate\Http\Response
     */
    public function edit(Invoice $invoice)
    {
        $invoice_details = $invoice->invoice_details;
        $customers = CustomerProfile::getDropDownList();
        $services = Service::getDropDownList();
        $countries = Country::getDropDownList();

        return view('invoices.edit', compact('invoice','invoice_details', 'customers', 'services', 'countries'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
        if(Invoice::destroy($request->hdnResource)){
            $message = toastMessage('Invoice has been successfully removed.');
        }else{
            $message = toastMessage('Invoice has not been removed.','error');
        }

        // Redirect
        $request->session()->flash('toast',$message);

        return back();
    }

    /**
     * Get info of invoice
     *
     * @param \Illuminate\Http\Request $request
     * @param int id
     * @return \Illuminate\Http\Response
     */
    public function fetchInvoice(Request $request, $id)
    {
        $invoice = Invoice::find($id);

        if(!$invoice) {
            return response()->json([
                'type'  => 'error',
                'message'   => 'Invoice not found!',
            ]);
        }

        $invoice->owner_email = $invoice->owner->email;
        $invoice->customer_name = $invoice->customer_profile->name;
        $invoice->customer_email = $invoice->customer_profile->email;
        $invoice->amount_due = $invoice->getAmountDue();

        return json_encode($invoice);
    }

    /**
     * Get table row of service info
     *
     * @param \Illuminate\Http\Request $request
     * @param int service_id
     * @return \Illuminate\Http\Response
     */
    public function fetchService(Request $request, $service_id)
    {
        $service = Service::find($service_id);

        if($service) {
            return json_encode($service);
        }
    }

    /**
     * Store customer and make options of customer list 
     *
     * @param \Illuminate\Http\Request $request
     * @return \Iluminate\Http\Response
     */
    public function saveCustomer(Request $request)
    {
        $rules = [
            'name' => 'required|max:255',
            'primary_phone' => 'required|max:255',
            'country'   => 'required'
        ];

        $validator = Validator::make($request->all(),$rules);
        if($validator->fails()){

            return response()->json([
                'status' => 400, 
                'error' => $validator->errors()
            ]);

        }else{
            // insert or update
            $customer_profile = !$request->has('customer_profile_id') ? new CustomerProfile : CustomerProfile::findOrFail($request->customer_profile_id);
            $customer_profile->name = trim($request->name);
            $customer_profile->email = trim($request->email);
            $customer_profile->primary_phone = trim($request->primary_phone);
            $customer_profile->alt_phone = trim($request->alt_phone);
            $customer_profile->contact_person_name = trim($request->contact_person_name);
            $customer_profile->contact_person_email = trim($request->contact_person_email);
            $customer_profile->customer_since = date('Y-m-d');
            $customer_profile->currency = trim($request->currency);
            $customer_profile->city = trim($request->city);
            $customer_profile->state = trim($request->state);
            $customer_profile->area = trim($request->area);
            $customer_profile->zip_code = trim($request->zip_code);
            $customer_profile->address = trim($request->address);
            $customer_profile->country = trim($request->country);

            $customer_profile->created_by = $request->user()->id;
            
            if($customer_profile->save()){

                $customers = CustomerProfile::getDropDownList();

                // make options of customer list 
                $options = '';
                $selected = '';
                foreach($customers as $key=>$value) {
                    
                    if($key == $customer_profile->id) {
                        $selected = ' selected="selected"';
                    }

                    $options .= '<option value="'.$key.'"'.$selected.'>'.$value.'</option>';
                }

                return response()->json([
                    'status' => 200, 
                    'type' => 'success', 
                    'options' => $options,
                    'customer_profile_id' => $customer_profile->id,
                ]);
            }else{

                return response()->json([
                    'status' => 404, 
                    'type' => 'error', 
                    'message' => 'Customer has not been added.'
                ]);
            }
        }
    }

    /**
     * Approve invoice
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function approveInvoice(Request $request, $id)
    {
        $invoice = Invoice::find($id);

        if(!$invoice) {
            return response()->json([
                'type'      => 'error',
                'message'   => 'Invoice not found!'
            ]);
        }

        if($invoice->invoice_details->isEmpty()) {
            return response()->json([
                'type'      => 'error',
                'message'   => 'A non-draft invoice must have one or more items',
            ]);
        }

        $invoice->status = 'unsent';
        $invoice_status = $invoice->getStatus();

        if($invoice->save()) {
            return response()->json([
                'type'      => 'success',
                'message'   => 'Invoice has been successfully approved.',
                'invoice_status' => $invoice_status,
            ]);
        }
    }

    /**
     * Set invoice sending status to skipped
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function skipSendingInvoice(Request $request, $id)
    {
        $invoice = Invoice::find($id);

        if(!$invoice) {
            return response()->json([
                'type'      => 'error',
                'message'   => 'Invoice not found!'
            ]);
        }

        $invoice->invoice_sent_status = 'skipped';

        if($invoice->save()) {

            $panel_three_html = $this->getPanelThreeHtml($invoice);

            return response()->json([
                'type'      => 'success',
                'message'   => 'Invoice sending is skipped',
                'panel_three_html'  => $panel_three_html
            ]);
        }
    }

    /**
     * Get panel three html
     *
     * @param obj
     * @return string
     */
    private function getPanelThreeHtml($invoice)
    {
        $amount_due = $invoice->getAmountDue();

        $panel_three_html = '<div class="row">
                                <div class="col-md-6">
                                    <h3 class="text-muted margin-top-0">Get Paid</h3>
                                </div>
                                <div class="col-md-6">
                                    <a href="javascript:openPaymentModal();" class="btn btn-primary">Record a Payment</a>
                                </div>
                            </div>

                            <div class="alert alert-info margin-top-20">
                                <div class="row">
                                    <div class="col-md-8">
                                        <strong>Status: </strong>'.($invoice->total - $amount_due ? 'Your invoice is partially paid.' : 'Your invoice is awaiting payment.').'
                                    </div>
                                    <div class="col-md-4 text-right">
                                        <strong>Amount Due: </strong>৳'. number_format($amount_due, 2) .'
                                    </div>
                                </div>
                            </div>

                            <div class="margin-top-20">
                                <strong>Get paid on time by scheduling payment reminders for your customer:</strong>
                            </div>';

        if($invoice->invoice_sent_status == 'skipped') {
            $panel_three_html .= '<div class="alert alert-warning margin-top-20">
                To schedule payment reminders for your customer, you must first send the invoice or mark it as sent.
            </div>';
        }

        $panel_three_html .= '<div class="remind-area margin-top-20'.($invoice->invoice_sent_status == 'skipped' ? ' form-disabled':'').'">
                <div class="row">

                    <div class="col-sm-4">
                        <div class="remind-before-area">
                            <h5 class="bold">REMIND BEFORE DUE DATE</h5>';

        if(in_array($invoice->invoice_sent_status, ['marked-as-sent', 'sent']) && $invoice->isRemindableDay(-14)){
            
            $panel_three_html .= '<div class="checkbox">
                  <label>'. Form::checkbox('remind_days[]', -14, old('remind_days')).' 14 days before</label>
                </div>';
        }else {
            $panel_three_html .= '<div class="checkbox form-disabled">
                  <label>'. Form::checkbox('remind_days[]', -14, old('remind_days'), ['disabled' => 'disabled']).' 14 days before</label>
                </div>';
        }

        if(in_array($invoice->invoice_sent_status, ['marked-as-sent', 'sent']) && $invoice->isRemindableDay(-7)){
            
            $panel_three_html .= '<div class="checkbox">
                  <label>'. Form::checkbox('remind_days[]', -7, old('remind_days')) .' 7 days before</label>
                </div>';
        }else {
            $panel_three_html .= '<div class="checkbox form-disabled">
                  <label>'. Form::checkbox('remind_days[]', -7, old('remind_days'), ['disabled' => 'disabled']) .' 7 days before</label>
                </div>';
        }

        if(in_array($invoice->invoice_sent_status, ['marked-as-sent', 'sent']) && $invoice->isRemindableDay(-3)){
            
            $panel_three_html .= '<div class="checkbox">
                  <label>'. Form::checkbox('remind_days[]', -3, old('remind_days')) .' 3 days before</label>
                </div>';
        }else {
            $panel_three_html .= '<div class="checkbox form-disabled">
                  <label>'. Form::checkbox('remind_days[]', -3, old('remind_days'), ['disabled' => 'disabled']) .' 3 days before</label>
                </div>';
        }                                
                                            
            $panel_three_html .= '</div>
                    </div>

                    <div class="col-sm-4">
                        <div class="remind-today-area">
                            <h5 class="bold">REMINED ON DUE DATE</h5>';

        if(in_array($invoice->invoice_sent_status, ['marked-as-sent', 'sent']) && $invoice->isRemindableDay(0)) {
                
                $panel_three_html .= '<div class="checkbox">
                      <label>'. Form::checkbox('remind_days[]', 0, old('remind_days')) .' 0 days before</label>
                    </div>';
        }else {
                $panel_three_html .= '<div class="checkbox form-disabled">
                      <label>'. Form::checkbox('remind_days[]', 0, old('remind_days'), ['disabled' => 'disabled']) .' 0 days before</label>
                    </div>';
        }
                                                
        $panel_three_html .=        '</div>
                                </div>

                    <div class="col-sm-4">
                        <div class="remind-after-area">
                            <h5 class="bold">REMIND AFTER DUE DATE</h5>';

        if(in_array($invoice->invoice_sent_status, ['marked-as-sent', 'sent']) && $invoice->isRemindableDay(3)) {
                $panel_three_html .= '<div class="checkbox">
                                          <label>'. Form::checkbox('remind_days[]', 3, old('remind_days')) .' 3 days before</label>
                                        </div>';
        }else {
                $panel_three_html .= '<div class="checkbox form-disabled">
                                          <label>'. Form::checkbox('remind_days[]', 3, old('remind_days'), ['disabled' => 'disabled']) .' 3 days before</label>
                                        </div>';
        }

        if(in_array($invoice->invoice_sent_status, ['marked-as-sent', 'sent']) && $invoice->isRemindableDay(7)) {
                $panel_three_html .= '<div class="checkbox">
                                          <label>'. Form::checkbox('remind_days[]', 7, old('remind_days')) .' 7 days before</label>
                                        </div>';
        }else {
                $panel_three_html .= '<div class="checkbox form-disabled">
                                          <label>'. Form::checkbox('remind_days[]', 7, old('remind_days'), ['disabled' => 'disabled']) .' 7 days before</label>
                                        </div>';
        }

        if(in_array($invoice->invoice_sent_status, ['marked-as-sent', 'sent']) && $invoice->isRemindableDay(14)) {
                $panel_three_html .= '<div class="checkbox">
                                          <label>'. Form::checkbox('remind_days[]', 14, old('remind_days')) .' 14 days before</label>
                                        </div>';
        }else {
                $panel_three_html .= '<div class="checkbox form-disabled">
                                          <label>'. Form::checkbox('remind_days[]', 14, old('remind_days'), ['disabled' => 'disabled']) .' 14 days before</label>
                                        </div>';
        }
                                       
        $panel_three_html .= '</div>
                            </div>

                            </div>
                        </div>';

        if(in_array($invoice->getStatus(false), ['sent', 'partial', 'viewed', 'overdue'])) {
            $panel_three_html .= '<div class="margin-top-20">
                <a href="'. url('invoice-reminder-preview/'.$invoice->identifier) .'" target="_blank">See a preview</a> of the reminder email, or <a href="javascript:openRemindInvoiceModal('. $invoice->id .');">send a reminder</a> now.
            </div>';
        }

        return $panel_three_html;
    }

    /**
     * Set invoice sending status to marked as sent
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function markedAsSent(Request $request, $id)
    {
        $invoice = Invoice::find($id);

        if(!$invoice) {
            return response()->json([
                'type'      => 'error',
                'message'   => 'Invoice not found!'
            ]);
        }

        if(!in_array($invoice->status, ['paid', 'partial'])) {
            $invoice->status = 'sent';
        }
        $invoice->invoice_sent_status = 'marked-as-sent';
        $invoice->invoice_sent_date = date('Y-m-d');

        if($invoice->save()) {

            $panel_three_html = $this->getPanelThreeHtml($invoice);

            if(in_array($invoice->status, ['paid', 'partial'])) {

                $payment_associate_info = $this->getPaymentAssociateInfo($invoice);
                
                $payment_info = $payment_associate_info['payment_info'];

                if($payment_info) {
                    $panel_three_html .= $payment_info;
                }
            }

            $invoice_status = $invoice->getStatus();

            return response()->json([
                'type'              => 'success',
                'message'           => 'Invoice marked as sent',
                'panel_three_html'  => $panel_three_html,
                'invoice_status'    => $invoice_status,
            ]);
        }
    }

    /**
     * Display Invoice email preview
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function showInvoiceEmailPreview(Request $request , $id)
    {
        $invoice = Invoice::findOrFail($id);
        $email_message = trim($request->message);

        return view('invoices.email_preview', compact('invoice', 'email_message'));
    }

    /**
     * Process the email sending of invoice
     * 
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function sendEmail(Request $request, $id)
    {
        $invoice = Invoice::find($id);

        if(!$invoice) {
            return response()->json([
                'status'  => 404,
                'type'    => 'error',
                'message' => 'Invoice not found',
            ]);
        }

        $rules = [
            'from'  => 'required|email:max:255',
            'to'    => 'required|email:max:255',
        ];

        $validator = Validator::make($request->all(), $rules);

        if($validator->fails()) {
            return response()->json([
                'error'    => $validator->errors(),
                'status'    => 400,
            ]);
        }

        $owner = $invoice->getCompanyOwner();
        $company = $owner->companies()->first();

        $data = [
            'blade' => 'invoice_email',
            'body'  =>  [
                'email_message' => trim($request->message),
                'invoice'  => $invoice,
                'owner'  => $owner,
                'company'  => $company,
            ],
            'fromAddress' => trim($request->from),
            'fromName' => $company->name,
            'toUser'    =>  trim($request->to),
            'toUserName' =>  $invoice->customer_profile->name,
            'subject'   =>  'Invoice #'.$invoice->identifier.' from '.$company->name,
        ];

        if($request->has('ccs')) {
            $data['ccs'] = array_filter(explode(',', trim($request->ccs)));
        }  

        if($request->has('bcc'))  {
            $data['bcc_address'] = $invoice->owner->email;
        }

        if($request->has('attachment')) {
            $pdf_folder = 'pdf/';

            // check whether folder already exist if not, create folder
            if(!file_exists($pdf_folder)) {
                mkdir($pdf_folder, 0755, true);
            }
            $file_name = 'invoice_'.$invoice->id.'_'.date('Y-m-d').'.pdf';
            $file_path = $pdf_folder . $file_name;

            if(file_exists($file_path)) {
                $file_name = 'invoice_'.$invoice->id.'_'.date('Y-m-d').'_'.mt_rand(100, 999).'.pdf';
                $file_path = $pdf_folder . $file_name;
            }

            $invoice_details = $invoice->invoice_details;
            $payments = $invoice->payments;
            
            PDF::loadView('pdf.invoice', compact('invoice', 'invoice_details','payments', 'company'))
                ->save($file_path);

            $data['attach_file_path'] = $file_path;
            $data['attach_file_options'] = [
                'as'    =>  $file_name,
                'mime'  =>  'application/pdf'
            ];
        }

        if(EmailSender::send($data)) {

            // Add data into mail_logs table
            $data['invoice_id'] = $invoice->id;
            $data['trigger_by'] = 'manual';
            $data['sent_by'] = $invoice->owner->id;

            MailLogHolder::add($data);

            if(!in_array($invoice->status, ['paid', 'partial'])) {
                $invoice->status = 'sent';
            }
            $invoice->invoice_sent_status = 'sent';
            $invoice->invoice_sent_date = date('Y-m-d');
            $invoice->save();

            $invoice_status = $invoice->getStatus();

            $invoice_sent_time = '<strong>Last Sent: </strong>with '. config('constants.app_name'). ' today' ;
            
            $panel_three_html = $this->getPanelThreeHtml($invoice);

            if(in_array($invoice->status, ['paid', 'partial'])) {
                
                $payment_associate_info = $this->getPaymentAssociateInfo($invoice);
                
                $payment_info = $payment_associate_info['payment_info'];

                if($payment_info) {
                    $panel_three_html .= $payment_info;
                }
            }

            return response()->json([
                'status'    => 200,
                'type'  => 'success',
                'message'   => 'Email has been successfully sent',
                'invoice_status'    => $invoice_status,
                'invoice_sent_time' => $invoice_sent_time,
                'panel_three_html' => $panel_three_html,
            ]);
        }else { 
            
            return response()->json([
                'status'    => 404,
                'type'  => 'error',     
                'message'   => 'Email has not been sent',
            ]);
        }
    }

    /**
     * Record payment for invoice
     *
     * @param \Illimunate\Http\Request $request
     * @param int $id
     * @return \Illimunate\Http\Response
     */
    public function recordPayment(Request $request, $id) 
    {
        $invoice = Invoice::find($id);

        if(!$invoice) {
            return response()->json([
                'type'      => 'error',
                'message'   => 'Invoice not found',
            ]);
        }

        $rules = [
            'payment_date'  => 'required|date|date_format:Y-m-d',
            'amount'    => 'required|numeric|min:0',
            'payment_mood'  => 'required|string|max:40',
        ];

        $validator = Validator::make($request->all(), $rules);

        if($validator->fails()) {

            return response()->json([
                'status'    => 400,
                'error' => $validator->errors(),
            ]);
        }

        // add or update payment record
        $payment = !$request->filled('payment_id') ? new Payment: Payment::find($request->payment_id);
        $payment->invoice_id = $invoice->id;
        $payment->customer_profile_id = $invoice->customer_profile_id;
        $payment->payment_date = trim($request->payment_date);
        $payment->amount = trim($request->amount);
        $payment->payment_mood = trim($request->payment_mood);
        $payment->memo = trim($request->memo);

        if(!$request->filled('payment_id')) {
            $payment->created_by = $request->user()->id;
            $msg = 'added.';
            $action = 'add';
        }else {
            $payment->updated_by = $request->user()->id;
            $msg = 'updated.';
            $action = 'update';
        }

        if($payment->save()) {
            $payment_id = $payment->id;

            $amount_due = $invoice->getAmountDue();

            $get_paid_info = '';
            if($amount_due <= 0) {
                $invoice->status = 'paid';
                $invoice->save();

                $get_paid_info = '<h3 class="text-muted margin-top-0">Get Paid</h3>
                    <div class="row">
                        <div class="col-md-8">
                            <strong>Status: </strong>Your invoice is paid in full.
                        </div>
                        <div class="col-md-4 text-right">
                            <strong>Amount Due: </strong><span class="amount-due">৳'. number_format($amount_due,2).' </span> 
                        </div>
                    </div>';
            }else {

                if(($amount_due != $invoice->total) && (Carbon::now()->diffInDays(Carbon::parse($invoice->payment_due), false) >= 0)) {
                    $invoice->status = 'partial';
                }else {
                    $invoice->status = 'viewed';
                }

                $invoice->save();
                $get_paid_info = $this->getPanelThreeHtml($invoice);
            }

            $payment_associate_info = $this->getPaymentAssociateInfo($invoice);

            $table_row_of_payment = $payment_associate_info['table_row_of_payment'];
            $payment_info = $payment_associate_info['payment_info'];

            if($payment_info) {
                $get_paid_info .= $payment_info;
            }

            $invoice_status = $invoice->getStatus();
            
            $due = $invoice->status == 'paid' ? '__' : Carbon::parse($invoice->payment_due)->diffForHumans(Carbon::now());

            return response()->json([
                'status'        => 200,
                'type'          => 'success',
                'message'       => 'Payment has been successfully '. $msg,
                'amount_due'    => $amount_due,
                'table_row_of_payment'  => $table_row_of_payment,
                'action'  => $action,
                'payment_id'    => $payment_id,
                'invoice_status'  => $invoice_status,
                'get_paid_info' => $get_paid_info,
                'due'   => $due,
            ]);
        }else {

            return response()->json([
                'status'    => 404,
                'type'  => 'error',
                'message'   => 'Payment has not been successfully '.$msg,
            ]);
        }
    }

    /**
     * Get payment associate info
     *
     * @param obj $invoice
     * @return array
     */
    private function getPaymentAssociateInfo($invoice)
    {
        $company = $invoice->getCompanyOwner()->companies()->first();
        $table_row_of_payment = '';
        $payment_info = '';
            
        $payments = $invoice->payments;
        if($payments->isNotEmpty()) {
            
            $payment_info = '<hr>
                    <section id="payment-info">
                        <h4 class="bold">Payment Received:</h4>';
                    
            foreach($payments as $payment) {
                
                $table_row_of_payment .= '<tr class="payment-row">
                    <td colspan="3" align="right">Payment on '. Carbon::parse($payment->payment_date)->format(getOption($company->id, 'invoice_date_format', 'Y-m-d')) .' using '.str_replace('-', ' ', $payment->payment_mood).' :</td>
                    <td class="text-right">৳'. number_format($payment->amount, 2).'</td>
                </tr>';

                $payment_info .= '<div id="payment-item-'.$payment->id.'" class="payment-item details-info margin-bottom-20">
                                <span>'. Carbon::parse($payment->payment_date)->format(getOption($company->id, 'invoice_date_format', 'Y-m-d')) .' - A payment for <strong>৳'. number_format($payment->amount, 2) .'</strong>  ‎was made using '. str_replace('-', ' ', $payment->payment_mood) .'</span>';
                if($payment->memo) {
                    $payment_info .='<br>
                    <small class="text-muted">'. $payment->memo .'</small>';
                }

                    $payment_info .='<br>
                                <div class="action-area">
                                    <a href="javascript:openSendReceipt('. $payment->id .');" class="btn btn-primary btn-xs"><i class="fa fa-send" aria-hidden="true"></i> Send a Receipt</a>

                                    <a href="javascript:openPaymentModal('. $payment->id .');" class="btn btn-info btn-xs"><i class="fa fa-edit" aria-hidden="true"></i> Edit Payment</a>
                                    
                                    <a href="javascript:openRemovePaymentModal('. $payment->id .');" class="btn btn-danger btn-xs btn-delete" data-amount="'. $payment->amount .'"><i class="fa fa-trash" aria-hidden="true"></i> Remove Payment</a>
                                </div>
                            </div>';
            }

            $payment_info .='</section>';
        }

        return [
            'table_row_of_payment'  => $table_row_of_payment,
            'payment_info'          => $payment_info,
        ];
    }

    /**
     * Fetch payment
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return response
     */
    public function fetchPayment(Request $request, $id)
    {
        $payment = Payment::find($id);

        if(!$payment) {
            return response()->json([
                'type'  => 'error',
                'message'   => 'Payment not found'
            ]);
        }
        $invoice = $payment->invoice;
        $payment->invoice_identifier = $invoice->identifier;
        $payment->customer_email = $invoice->customer_profile->email;
        
$payment->message = "
Hi,        

Here's your payment receipt for Invoice #" .strtoupper($invoice->identifier).", for ৳".number_format($payment->amount, 2)." BDT.

You can always view your receipt online, at:
". url('payment-receipt/'.encrypt($payment->id)) ."

If you have any questions, please let us know.

Thanks,
". $invoice->getCompanyOwner()->companies()->first()->name;

        return response()->json($payment);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function removePayment(Request  $request, $id)
    {
        $invoice = Invoice::find($id);

        if(!$invoice) {
            return response()->json([
                'type'      => 'error',
                'message'   => 'Invoice not found',
            ]);
        }
           
        if(Payment::destroy($request->payment_id)) {
            
            $amount_due = $invoice->getAmountDue();
            
            $get_paid_info = '';
            
            if($amount_due <= 0) {
                $invoice->status = 'paid';
                $invoice->save();

                $get_paid_info = '<h3 class="text-muted margin-top-0">Get Paid</h3>
                    <div class="row">
                        <div class="col-md-8">
                            <strong>Status: </strong>Your invoice is paid in full.
                        </div>
                        <div class="col-md-4 text-right">
                            <strong>Amount Due: </strong><span class="amount-due">৳'. number_format($amount_due,2).' </span> 
                        </div>
                    </div>';
            }else {

                if(($amount_due != $invoice->total) && (Carbon::now()->diffInDays(Carbon::parse($invoice->payment_due), false) >= 0)) {
                    $invoice->status = 'partial';
                }else {
                    $invoice->status = 'viewed';
                }

                $invoice->save();
                $get_paid_info = $this->getPanelThreeHtml($invoice);
            }

            
            $payment_associate_info = $this->getPaymentAssociateInfo($invoice);

            $table_row_of_payment = $payment_associate_info['table_row_of_payment'];
            $payment_info = $payment_associate_info['payment_info'];

            if($payment_info) {
                $get_paid_info .= $payment_info;
            }

            $invoice_status = $invoice->getStatus();

            $due = $invoice->status == 'paid' ? '__' : Carbon::parse($invoice->payment_due)->diffForHumans(Carbon::now());

            return response()->json([
                'status'        => 200,
                'type'          => 'success',
                'message'       => 'Payment has been successfully removed',
                'get_paid_info' => $get_paid_info,
                'amount_due'    => $amount_due,
                'table_row_of_payment'  => $table_row_of_payment,
                'invoice_status'    => $invoice_status,
                'due'   => $due
            ]);
        }else {

            return response()->json([
                'status'    => 404,
                'type'  => 'error',
                'message'   => 'Payment has not been removed ',
            ]);
        }
    }

    /**
     * Send Receipt
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function sendReceipt(Request $request) 
    {
        $payment = Payment::find($request->payment_id);

        if(!$payment) {
            return response()->json([
                'status'  => 404,
                'type'    => 'error',
                'message' => 'Payment not found',
            ]);
        }

        $rules = [
            'to'    => 'required|email:max:255',
        ];

        $validator = Validator::make($request->all(), $rules);

        if($validator->fails()) {
            return response()->json([
                'error'    => $validator->errors(),
                'status'    => 400,
            ]);
        }

        $invoice = $payment->invoice;
        $owner = $invoice->owner;
        $company_owner = $invoice->getCompanyOwner();
        $company = $company_owner->companies()->first();

        $data = [
            'blade' => 'sent_receipt_email',
            'body'  =>  [
                'email_message' => $request->message,
                'payment'       => $payment,
                'invoice'       => $invoice,
                'owner'         => $company_owner,
                'company'       => $company,
            ],
            'fromAddress' => $owner->email,
            'fromName' => $company->name,
            'toUser'    =>  trim($request->to),
            'toUserName' =>  $invoice->customer_profile->name,
            'subject'   =>  'Payment Receipt for Invoice#'.$invoice->identifier,
        ];

        if($request->has('ccs')) {
            $data['ccs'] = array_filter(explode(',', trim($request->ccs)));
        }  

        if($request->has('bcc'))  {
            $data['bcc_address'] = $invoice->owner->email;
        }

        if(EmailSender::send($data)) { 

            // Add data into mail_logs table
            $data['invoice_id'] = $invoice->id;
            $data['trigger_by'] = 'manual';
            $data['sent_by'] = $invoice->owner->id;

            MailLogHolder::add($data);
            
            return response()->json([
                'status'    => 200,
                'type'  => 'success',
                'message'  => 'This receipt has been sent.',
            ]);
        }else {
            return response()->json([
                'status'    => 404,
                'type'  => 'error',
                'message'  => 'This receipt has not been sent.',
            ]);
        }
    }        
    
    /**
     * Show the receipt view of payment
     * 
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function showPaymentReceipt($id)
    {
        $payment = Payment::findOrFail($id);

        return view('preview.payment_receipt', compact('payment'));
    }

    /**
     * Send invoice reminder
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function sendReminder(Request $request)
    {
        $invoice = Invoice::find($request->invoice_id);

        if(!$invoice) {
            return response()->json([
                'type'  => 'error',
                'message'  => 'Invoice not found!',
            ]); 
        }

        if(!$invoice->customer_profile->email) {
            return response()->json([
                'type'  => 'error',
                'message'  => 'Customer email not found!',
            ]);
        }

        $companyOwner = $invoice->getCompanyOwner();
        $company = $companyOwner->companies()->first();

        $data = [
            'blade' => 'invoice_reminder',
            'body'  =>  [
                'invoice'       => $invoice,
                'companyOwner'  => $companyOwner,
                'company'       => $company,
            ],
            'toUser'    =>  $invoice->customer_profile->email,
            'toUserName' =>  $invoice->customer_profile->name,
            'subject'   =>  'Reminder for Invoice #'. strtoupper($invoice->identifier),
        ];

        if(EmailSender::send($data)) {
            
            return response()->json([
                'type'  => 'success',
                'message'   => 'Reminder email sent',
            ]);

        }else {
            
            return response()->json([
                'type'      => 'error',
                'message'   => 'Reminder email not sent',
            ]);
            
        }
    }
}
