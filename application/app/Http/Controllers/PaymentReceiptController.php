<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Payment;

class PaymentReceiptController extends Controller
{
    /**
     * Display preview of payment receipt
     *
     * @param string $encrypted_id
     * @return \Illuminate\Http\Response
     */
    public function __invoke($encrypted_id)
    {
    	$id = decrypt($encrypted_id);
    	$payment = Payment::findOrFail($id);
        $invoice = $payment->invoice;
        $owner = $invoice->getCompanyOwner();
        $company = $owner->companies()->first();

    	return view('preview.payment_receipt', compact('payment','invoice', 'owner', 'company'));
    }
}
