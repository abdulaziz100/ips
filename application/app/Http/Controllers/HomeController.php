<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\CustomerProfile;
use App\Invoice;
use App\User;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     * 
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if(!isSuperAdmin()) {

            $query = Invoice::whereType('normal')
                ->where(function($query) {
                $query->whereIn('status', ['unsent', 'sent', 'viewed', 'partial'])
                    ->where('payment_due', '<', date('Y-m-d'));
                });

            if(isOwner() || isAgent()) {
                $query->whereOwnerId($request->user()->id);
            }else if (isCustomer()){
                $query->whereCustomerId($request->user()->id);
            }

            $overdue_invoices = $query->latest()->take('5')->get();

            $query = Invoice::whereType('normal')
                ->where(function($query) {
                $query->whereIn('status', ['partial', 'paid'])
                    ->where('payment_due', '>=', date('Y-m-d'));
                });

            if(isOwner() || isAgent()) {
                $query->whereOwnerId($request->user()->id);
            }else if (isCustomer()){
                $query->whereCustomerId($request->user()->id);
            }

            $paid_invoices = $query->latest()->take('5')->get();

        }

        if(isSuperAdmin()) {

            $owners = User::whereUserType('owner')->latest()->take(5)->get();
        }

        if(!isCustomer()) {

            $customer_profiles = CustomerProfile::where(function($query) use($request) {
                if(!isSuperAdmin()) {
                    $query->whereCreatedBy($request->user()->id);
                }
            })->latest()->take(5)->get();
        }
        
        return view('home', compact('overdue_invoices', 'paid_invoices', 'owners', 'customer_profiles'));
    }
}
