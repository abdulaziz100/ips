<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Auth;
use App\User;

class AccountActivationController extends Controller
{
    public function active(Request $request){

        if(empty($request->token)){
            $message = message('danger','info-circle', 'Account Activation: Invalid Request.');
            return redirect('login')->withErrors(['login-error'=>$message]);
        }

        try {
            $data = \Firebase\JWT\JWT::decode($request->token, env('JWT_SECRET_KEY'), ['HS256']);

            if(empty($data->resource)){
                $message = message('danger','info-circle', 'Account Activation: Invalid Request.');
                return redirect('login')->withErrors(['login-error'=>$message]);
            }

            if(User::whereId($data->resource)->whereActive(true)->count() > 0){
                $message = message('danger','info-circle', 'Account Activation: Your account already active, login using your email and password.');
                return redirect('login')->withErrors(['login-error'=>$message]);
            }

            if(Auth::loginUsingId($data->resource)){

                $last_login = Auth::user ()->last_login != "" ? Auth::user ()->last_login : Auth::user ()->created_at;
                session()->put ( 'last_login', date ( 'd M, Y @ h:i:s a', strtotime ( $last_login ) ) );

                User::whereId($data->resource)->update(['active'=>true, 'last_login' => date ( 'Y-m-d H:i:s' )]);
                
                // User::setLoginSession();

                return redirect('/home');
                
            }else{
                $message = message('danger','info-circle', 'Account Activation: Invalid Request.');
                return redirect('login')->withErrors(['login-error'=>$message]);
            }

        } catch (\Firebase\JWT\ExpiredException $e) {
            $message = message('danger','info-circle', 'Account Activation: Invalid Request.');
            return redirect('login')->withErrors(['login-error'=>'Account Activation: '.$e->getMessage()]);
        } catch (\Exception $e) {
            $message = message('danger','info-circle', 'Account Activation: Invalid Request.');
            return redirect('login')->withErrors(['login-error'=>$message]);
        }
    }
}
