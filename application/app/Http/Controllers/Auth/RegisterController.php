<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

use Illuminate\Http\Request;
use ReCaptcha\ReCaptcha;
use Helpers\Classes\EmailSender;
use Auth, Carbon, Crypt, Mail;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'username' => 'required|alpha_dash|max:100|unique:users|min:3',
            'email' => 'required|string|email|max:100|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'username' => $data['username'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'user_type' => !empty($data['user_type']) ? $data['user_type'] : 'owner',
        ]);
    }

    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $this->validator($request->all())->validate();

        // Check recapthca
        $recaptcha = new ReCaptcha(env('RECAPTCHA_SECRET'));
        $resp = $recaptcha->verify($request->input('g-recaptcha-response'), $_SERVER['REMOTE_ADDR']);
        
        if (!$resp->isSuccess()) {
            
            $message = 'Please fill up the recaptcha';

            return back()->withInput()->with('recaptha-error', $message);
        }

        $user = $this->create($request->all());

        $success = 0;

        if($user) {
            
            //encode    
            $iat = Carbon::now()->timestamp;
            $exp = $iat+3600;  
            $token = [
                "resource" => $user->id,
                "iss" => env('APP_HOST'),
                "iat" => $iat,
                "exp" => $exp
            ];

            $jwt = \Firebase\JWT\JWT::encode($token, env('JWT_SECRET_KEY'));

            $data = [
                'blade' => 'new_account',
                'body'  =>  [
                    'name' => trim($request->name),
                    'username'  => trim($request->username),
                    'password'  => trim($request->password),
                    'jwt'  => $jwt,
                ],
                'toUser'    =>  trim($request->email),
                'toUserName'    =>  trim($request->username),
                'subject'   =>  env('APP_NAME') . ' New Account Confirmation!',
            ];

            EmailSender::send($data);

            $message = toastMessage ( '<strong>Congratulation</strong>, Your account has been successfully created. A confirmation mail has been sent to your email address', 'success' );
            $success = 1;

        }else{
            $message = toastMessage ( " Your account has not been created.", 'error' );
        }

        // redirect
        session()->flash('toast', $message);
        
        if($success) {
            
            return redirect()->back();
        }else {
            return redirect()->back()->withInput();
        }
    }
}
