<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Helpers\Classes\Uploader;
use File, Validator;

class OptionController extends Controller
{
	/**
     * For user access control
     */
    public function __construct()
    {
        $this->middleware('owner');
        $this->middleware('company_owner');
    }

   /**
	* Display diffrent options of settings
	*
	* @param \Illuminate\Http\Request $request
	* @return \Illuminate\Http\Response
	*/
	public function index(Request $request)
	{
		$company_id = $request->user()->companies()->first()->id;
		
		if($request->isMethod('POST')) {

			$rules =[
				'payment_terms'	=> 'required|integer',
				'invoice_title'	=> 'required|max:255',
				'invoice_date_format'	=> 'required',
			];

			if($request->hasFile('logo')) {
	            $rules['logo'] = 'mimes:jpg,jpeg,png|max:1024';
	        }

	        $validator = Validator::make($request->all(), $rules);
        
	        if ($validator->fails()) {
	            
	            return redirect()->back()->withErrors($validator)->withInput();
	        }else{
	            // store
	            /**
	            * Upload logo
	            */
	            $upload_folder = 'uploads/company-logo/';
	            // check whether folder already exist if not, create folder

	            $option_logo = getOption($company_id, 'logo');
	            // Delete logo from upload folder && database if remove button is pressed and do not upload logo
	            if(!empty($option_logo) && $request->file_remove == 'true' && !$request->hasFile('logo')){
	                $uploaded_logo_name = basename($option_logo);

	                if(Uploader::delete($option_logo)) {
	                	$option_logo = null;
	                }
	            }

	            if($request->hasFile('logo')) {

	                // check if logo already exists in database
	                if(!empty($option_logo)) {
	                    if(Uploader::delete($option_logo)) {
	                		$option_logo = null;
	                	}
	                }

	                if($path = Uploader::upload($request->file('logo'), $upload_folder)) {
	                	$option_logo = $path;
	                }
	            }

	            $request_all = array_except($request->all(), [ '_token', 'logo']);

	            if($option_logo && $request_all['file_remove'] == 'false') {
	            	$request_all['logo'] = $option_logo;
	            }else {
	            	$request_all['logo'] = null;
	            }
	            
	            $request_all = array_except($request_all, [ 'file_remove']);

	            foreach($request_all as $key=>$value) {
	            	addOrUpdateOption($company_id, $key, $value);
	            }

	            session()->flash('toast', toastMessage('Settings info has been updated'));

	            return back();
			}
		}

		$options = getAllOption($company_id);

		return view('settings', compact('options'));
	}	
}
