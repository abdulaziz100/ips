<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Helpers\Classes\EmailSender;
use App\User;
use Auth, Carbon, Validator;

class UserController extends Controller
{
	/**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth', ['except' => [ 'getResetPassword']]);

        $this->middleware('super-admin', ['only' => ['getOwnerList']]);

        $this->middleware('owner', ['only' => 'delete', 'changeActive']);
    }

    /**
     * Display a list of owners
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function getOwnerList(Request $request)
    {
        if(isSuperAdmin()) {
            $users = User::whereUserType('owner')->where(function($query) use($request) {
                if($request->has('search_item')) {
                    $query->where('name', 'LIKE', '%' .trim($request->search_item) . '%')
                    ->orWhere('email', 'LIKE', '%'.trim($request->search_item). '%');
                }
            })->latest('id')->paginate(10);
            $users->paginationSummery = getPaginationSummery($users->total(), $users->perPage(), $users->currentPage());

            if($request->has('search_item')) {
                $users->appends(['search_item' => trim($request->search_item)]);
            }

            return view('users.owner_list', compact('users'));
        }else {
            abort(403, 'Unauthorized action.');
        }
    }

    /**
     * Display a list of agents
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function getAgentList(Request $request)
    {
        $users = User::whereUserType('agent')->where(function($query) use($request) {
            if($request->has('search_item')) {
                $query->where('name', 'LIKE', '%' .trim($request->search_item) . '%')
                    ->orWhere('email', 'LIKE', '%'.trim($request->search_item). '%');
            }
            if(!isSuperAdmin()) {
                $query->whereOwnerId($request->user()->id);
            }
        })->latest('id')->paginate(10);
        $users->paginationSummery = getPaginationSummery($users->total(), $users->perPage(), $users->currentPage());

        if($request->has('search_item')) {
            $users->appends(['search_item' => trim($request->search_item)]);
        }

        return view('users.owner_list', compact('users'));
    }

    /**
     * Create a agent
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function createAgent(Request $request)
    {
        return view('users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeAgent(Request $request)
    {
        $rules = [
            'name' => 'required|string|max:255',
            'username' => 'required|alpha_dash|max:100|unique:users|min:3',
            'email' => 'required|string|email|max:100|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ];

        $validator = Validator::make($request->all(), $rules);

        if($validator->fails()) {

            return redirect()->back()->withInput()->withErrors($validator); 

        }else {
            // create or update users
            $user = $request->filled('user_id') ? User::find($request->user_id) : new User;
            $user->name = trim($request->name);
            $user->username = trim($request->username);
            $user->email = trim($request->email);
            $user->password = bcrypt($request->password);
            $user->user_type = 'agent';

            if(!$request->filled('user_id')) {
                $msg = 'added.';
                $user->created_by = $request->user()->id;
            }else {
                $msg = 'updated.';
                $user->updated_by = $request->user()->id;
            }

            if(isOwner()) {
                $user->owner_id = $request->user()->id;
            }

            if($user->save()) {
                $message = toastMessage('User has been successfully '.$msg);

                if(!$request->filled('user_id')) {
                    
                    //encode    
                    $iat = Carbon::now()->timestamp;
                    $exp = $iat+3600;  
                    $token = [
                        "resource" => $user->id,
                        "iss" => env('APP_HOST'),
                        "iat" => $iat,
                        "exp" => $exp
                    ];

                    $jwt = \Firebase\JWT\JWT::encode($token, env('JWT_SECRET_KEY'));

                    $data = [
                        'blade' => 'new_account',
                        'body'  =>  [
                            'name' => trim($request->name),
                            'username'  => trim($request->username),
                            'password'  => trim($request->password),
                            'jwt'  => $jwt,
                        ],
                        'toUser'    =>  trim($request->email),
                        'toUserName'    =>  trim($request->username),
                        'subject'   =>  env('APP_NAME') . ' New Account Confirmation!',
                    ];

                    EmailSender::send($data);
                }
            }else {
                $message = toastMessage('User has not been '.$msg, 'error');
            }

            session()->flash('toast', $message);

            return redirect('agents');
        }
    }

    /**
    * Change user access
    *
    * @param  \Illuminate\Http\Request  $request
    * @param int $id
    * @return \Illuminate\Http\Response
    */
    public function changeActive(Request  $request)
    {
        $user = User::find($request->hdnResource);
        if(!$user) {
            session()->flash('toast', toastMessage('User not found', 'error'));

            return back();
        }

        if($user->active) {
            $user->active = 0;
            $msg = 'deactivated';
        }else {
            $user->active = 1;
            $msg = 'activated';
        }

        if($user->save()) {
            $message = toastMessage(' User has been '.$msg);
        }else {
            $message = toastMessage(' User has not been '.$msg, 'error');
        }

        $request->session()->flash('toast', $message);
        
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
        if(User::destroy($request->hdnResource)){
            $message = toastMessage('Owner has been successfully removed.');
        }else{
            $message = toastMessage('Owner has not been removed.','error');
        }

        // Redirect
        $request->session()->flash('toast',$message);

        return redirect('owners');
    }

    /**
     * Display a profile form and update user info
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function profile(Request $request)
    {
        $user = $request->user();

        if($request->isMethod('POST')) {
            
            $rules = [
                'password'  => 'min:6|confirmed'
            ];

            $validator = Validator::make($request->all(), $rules);

            if($validator->fails()) {
                return redirect()->back()->withInput()->withErrors($validator);
            }else {
                // update user info
                $user->name = trim($request->name);
                if($request->has('password')) {
                    $user->password = bcrypt($request->password);
                }

                if($user->save()) {
                    $message = toastMessage('User has been successfully updated');
                }else {
                    $message = toastMessage('User has been successfully updated', 'error');
                }

                session()->flash('toast', $message);

                return redirect()->back();
            }
        }

        return view('users.profile', compact('user'));
    }

    /**
    * Reset Pasword
    */
    public function getResetPassword(Request $request, $username)
    {
        // $user = User::whereUsername($username)->first();
        // $user->password = bcrypt(123456);
        
        // if($user->save()) {
        //     exit('User password has been reset. Your new password is 123456');
        // }else {
        //     exit('Something wrong!. Please try again.');
        // }
    }
}
