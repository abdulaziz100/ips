<?php

namespace App\Http\Controllers;
ini_set('max_execution_time', 300);
use App\Invoice;
use Illuminate\Http\Request;

use App\Helpers\Classes\InvoiceSender;
use App\Country;
use App\CustomerProfile;
use App\InvoiceDetail;
use App\InvoiceSchedule;
use App\InvoiceSentOption;
use App\Payment;
use App\Service;
use App\User;
use Carbon, Form, PDF, Validator;

class RecurringInvoiceController extends Controller
{
    /**
     * For user access control
     */
    public function __construct()
    {
        $this->middleware('agent');
        $this->middleware('company_owner');
    }

    /**
     * Display a listing of the resource.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function invoiceList(Request $request)
    {
        $query = Invoice::whereType('recurring');

        $data = [];
        
        if(!isSuperAdmin()) {
            $query->whereCreatedBy($request->user()->id);
        }

        if($request->filled('customer_profile_id')) {
            
            $query->whereCustomerProfileId($request->customer_profile_id);
            $data['customer_profile_id'] = $request->customer_profile_id;
        }

        if($request->filled('status') && in_array($request->status, ['draft', 'active', 'end'])) {
            
            $query->whereStatus($request->status);

            $data['status'] = $request->status;
        }

        $invoices = $query->latest('id')->paginate(10);
        $invoices->paginationSummery = getPaginationSummery($invoices->total(), $invoices->perPage(), $invoices->currentPage());
        
        if($data) {
            $invoices->appends($data);
        }

        $customer_profiles = CustomerProfile::getDropDownList();
        $recurring_invoice_statuses = array_prepend(config('constants.recurring_invoice_status'), 'Select status', '');

        return view('recurring_invoices.index', compact('invoices', 'customer_profiles', 'recurring_invoice_statuses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $customers = CustomerProfile::getDropDownList();
        $services = Service::getDropDownList();
        $countries = Country::getDropDownList();

        return view('recurring_invoices.create', compact('customers', 'services', 'countries'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $total_service_descriptions = count($request->service_description);
        $total_quantities = count($request->quantity);
        $total_prices = count($request->rate);
        $total_vats = count($request->vat);

        $except = ['service_description', 'quantity', 'price', 'vat'];

        if($total_service_descriptions == $total_quantities && $total_quantities == $total_prices && $total_prices == $total_vats && $total_vats) {
            
            $rules = [
                'customer_profile_id' => 'required|integer|min:1',
                'payment_due'  => 'required',
            ];

            if($request->has('svc_id') || $request->has('service_description') || $request->has('quantity') || $request->has('rate') || $request->has('vat')) {

                for ($i=0; $i < $total_vats; $i++) { 
                    $rules['svc_id.'.$i] = 'required|integer|min:1';
                    $rules['service_description.'.$i] = 'string|max:500|nullable';
                    $rules['quantity.'.$i] = 'required|integer|min:1';
                    $rules['rate.'.$i] = 'required|numeric|min:0';
                    $rules['vat.'.$i] = 'required|integer|min:0';
                }
            }

            $validator = Validator::make($request->all(), $rules);
            
            if($validator->fails()) {
                
                return back()->withInput($request->except($except))->withErrors($validator);
            }else {
                $total_quantity = $sub_total_price = $total_vat_amount = 0;

                $service_ids = $request->svc_id;
                $service_descriptions = $request->service_description;
                $quantities = $request->quantity;
                $rates = $request->rate;
                $vats = $request->vat;

                if($request->has('svc_id') || $request->has('service_description') || $request->has('quantity') || $request->has('rate') || $request->has('vat')) {
                    
                    for ($i=0; $i < $total_vats; $i++) { 
                        $total_quantity += $quantities[$i];
                        $sub_total_price += $quantities[$i]*$rates[$i];
                        $total_vat_amount += ($quantities[$i]*$rates[$i]*$vats[$i])/100;
                    }
                }

                // insert or update invoice
                $invoice = !$request->has('invoice_id') ? new Invoice : Invoice::findOrFail($request->invoice_id);
                $invoice_status = !empty($invoice->status) && $invoice->status == 'active' ? 'active' : 'draft';
                $invoice->customer_profile_id = trim($request->customer_profile_id);
                $invoice->owner_id = $request->user()->id;
                $invoice->title = trim($request->title);
                $invoice->type = 'recurring';
                $invoice->invoice_summary = trim($request->invoice_summary);
                if(!$request->has('invoice_id')) {
                    $invoice->identifier = Invoice::getIdentifier();
                }
                $invoice->po_number = trim($request->po_number);
                $invoice->sub_total = $sub_total_price;
                $invoice->vat_total = $total_vat_amount;
                $invoice->total = $sub_total_price + $total_vat_amount;
                $invoice->invoice_date = date('Y-m-d');
                ;
                $invoice->payment_due = Carbon::now()->addDays(trim($request->payment_due))->format('Y-m-d');
                $invoice->notes = trim($request->notes);
                $invoice->invoice_footer = trim($request->invoice_footer);
                $invoice->status = $invoice_status;

                if(!$request->has('invoice_id')) {
                    $invoice->created_by = $request->user()->id;
                    $msg = 'added.';
                }else {
                    $invoice->updated_by = $request->user()->id;
                    $msg = 'updated.';
                }
                
                if($invoice->save()) {

                    if($request->has('invoice_id')) {
                        $invoice->invoice_details()->forceDelete();
                    }
                    
                    if($request->has('svc_id') || $request->has('service_description') || $request->has('quantity') || $request->has('rate') || $request->has('vat')) {
                        
                        // add data to invoice details table

                        for ($i=0; $i < $total_vats; $i++) { 
                            $service = Service::findOrFail($service_ids[$i]);
                            $invoice_detail = new InvoiceDetail;
                            $invoice_detail->invoice_id = $invoice->id;
                            $invoice_detail->service_id = $service->id;
                            $invoice_detail->service_name = $service->title;
                            $invoice_detail->service_description = $service_descriptions[$i];
                            $invoice_detail->quantity = $quantities[$i];
                            $invoice_detail->rate = $rates[$i];
                            $invoice_detail->vat = $vats[$i];
                            $invoice_detail->amount = $quantities[$i]*$rates[$i];

                            $invoice_detail->save();
                        }
                    }

                    $message = toastMessage('Invoice has been '.$msg);
                }else{
                    $message = toastMessage('Invoice has not been '.$msg, 'error');
                }

                // redirect 
                $request->session()->flash('toast',$message);

                return redirect('recurring-invoices/list');
            }
        }else {
            session()->flash('toast', toastMessage('<strong>empty items:</strong> Please add items to your recurring invoice', 'error'));

            return back()->withInput($request->except($except));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $invoice = Invoice::findOrFail($id);
        $invoice_details = $invoice->invoice_details;
        $customer_profile = $invoice->customer_profile;
        $customer_profile_html = '<strong>'.$customer_profile->name.'</strong>';
        if($customer_profile->email) {
            $customer_profile_html .= '<br/>'.$customer_profile->email;
        }

        $customer_profile_html .= '<br/><br/><strong>Tel: </strong>'.$customer_profile->primary_phone;
        $owner = $invoice->getCompanyOwner() ;
        $company = $owner->companies()->first();
        $amount_due = $invoice->getAmountDue();
        $total_invoices = $invoice->recurring_invoice_logs()->count();
        $schedule = $invoice->invoice_schedule;
        $invoice_sent_option = $invoice->invoice_sent_option;

         return view('recurring_invoices.show', compact('invoice','invoice_details', 'customer_profile', 'customer_profile_html', 'owner', 'company', 'amount_due', 'total_invoices', 'schedule', 'invoice_sent_option'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $invoice = Invoice::findOrFail($id);
        $invoice_details = $invoice->invoice_details;
        $customers = CustomerProfile::getDropDownList();
        $services = Service::getDropDownList();
        $countries = Country::getDropDownList();
        $invoice_date = Carbon::parse($invoice->invoice_date. ' 00:00:00');
        $payment_due = Carbon::parse($invoice->payment_due. ' 00:00:00');

        $diff = $invoice->getDayDiff();

        return view('recurring_invoices.edit', compact('invoice','invoice_details', 'customers', 'services', 'countries', 'diff'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
        if(Invoice::destroy($request->hdnResource)){
            $message = toastMessage('Invoice has been successfully removed.');
        }else{
            $message = toastMessage('Invoice has not been removed.','error');
        }

        // Redirect
        $request->session()->flash('toast',$message);

        return back();
    }

    /**
     * Get days of specific month
     *
     * @param int $month_no
     * @return \Illuminate\Http\Response
     */
    public function getMonthDays($month_no)
    {
        $arr = getDayOfMonth($month_no);

        $options = '';
        if($arr && is_array($arr)) {
            foreach($arr as $key=>$value) {
                $selected = '';

                if(date('j') == $key) {
                   $selected = ' selected="selected"'; 
                }
                $options .= '<option value="'.$key.'"{$selected}>'.$value.'</option>';
            }
        }

        return $options;
    }

    /**
     * Set Invoice Schedule
     *
     * @param \Illuminate\Http\Request $request
     * @param int $invoice_id
     * @return \Illuminate\Http\Response
     */
    public function setSchedule(Request $request, $invoice_id)
    {
        $invoice = Invoice::find($invoice_id);

        if(!$invoice) {
            return response()->json([
                'type'  => 'error',
                'message'  => 'Invoice not found!',
            ]);
        }

        $rules = [
            'frequency' => 'required',
            'day_of_week'  => 'required_if:frequency,weekly|integer',
            'day_of_month'  => 'required_if:frequency,monthly|integer',
            'month'  => 'required_if:frequency,yearly',
            'yearly_day_of_month'  => 'required_if:frequency,yearly|integer',
            'start' => 'required|date|date_format:Y-m-d',
            'timezone' => 'required|string',
            'trigger_time'  => 'required',
        ] ;

        if($request->filled('until')) {
            $rules['until'] = 'date|date_format:Y-m-d|after_or_equal:start';
        }

        $validator = Validator::make($request->all(), $rules);

        if($validator->fails()) {
            $messages = $validator->messages()->all();
            $validation_error = '';

            foreach($messages as $value) {
                $validation_error .= $value . '<br/>';
            }

            return response()->json([
                'type'  => 'error',
                'message'   => $validation_error,
            ]); 
        }

        if(Carbon::now()->diffInDays(Carbon::parse($request->start), false) < 0 ) {
            
            return response()->json([
                'type'  => 'error',
                'message'   => 'Start must be equal or greater than current date',
            ]);

        }

        // add or update invoice schedule
        $invoice_schedule = !$request->filled('invoice_schedule_id') ? new InvoiceSchedule : InvoiceSchedule::findOrFail(trim($request->invoice_schedule_id));
        $invoice_schedule->invoice_id = $invoice_id;
        
        // Set trigger time
        $trigger_time_arr = explode(' ', trim($request->trigger_time));
        $hour_minutes_arr = explode(':', $trigger_time_arr[0]);
        $hour = $hour_minutes_arr[0];
        $minute = $hour_minutes_arr[1];
        if($trigger_time_arr[1] == 'PM' && $hour!=12) {
            $hour+=12;
        }
        $invoice_schedule->trigger_time = $hour.':'.$minute.':00';
        $invoice_schedule->frequency = trim($request->frequency);
        
        if(trim($request->frequency) == 'weekly') {
            $invoice_schedule->day_of_week = $request->filled('day_of_week') ? trim($request->day_of_week) : null;
        }else if(trim($request->frequency) == 'yearly'){
            $invoice_schedule->month = $request->filled('month') ? trim($request->month) : null;
            $invoice_schedule->day_of_month = $request->filled('yearly_day_of_month') ? trim($request->yearly_day_of_month) : null;
        }else if(trim($request->frequency) == 'monthly'){
            $invoice_schedule->day_of_month = $request->filled('day_of_month') ? trim($request->day_of_month) : null;
        }
        
        $invoice_schedule->start = trim($request->start);
        $invoice_schedule->until = $request->filled('until') ? trim($request->until) : null;
        $invoice_schedule->timezone = trim($request->timezone);

        if(!$request->filled('invoice_schedule_id')) {
            $msg = 'added.';
        }else {
            $msg = 'updated.';
        }

        if($invoice_schedule->save()) {
            
            $schedule_html = '<h3 class="text-muted margin-top-0">Set Schedule</h3>';
            $schedule = $invoice_schedule;

            $schedule_html .= Form::model($schedule, ['url' => '', 'role' => 'form', 'id' => 'invoice-schedule-form', 'class' => 'margin-top-20 hide']) .
                    '<div class="row margin-bottom-20">
                        <div class="col-sm-6">
                            <div class="row">'.
                                Form::label('frequency','Repeat this invoice', ['class' => 'control-label col-sm-6 text-right padding-right-0']) .'
                                <div class="col-sm-6">'.
                                     Form::select('frequency',config('constants.frequency'), $schedule ? $schedule->frequency : 'monthly',['class'=>'form-control chosen-select', 'onchange' => 'setFrequencyAssociates(this.value);']) .'
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="monthly-content">
                                on the '. Form::select('day_of_month',getDayOfMonth(), 1,['class'=>' chosen-select']) .' day of every month
                            </div>
                            <div class="weekly-content hide">
                                every '. Form::select('day_of_week',config('constants.day_of_week'), 2,['class'=>' chosen-select']) .' 
                            </div>
                            <div class="yearly-content hide">
                                every '. Form::select('month',getMonth(), date('n'),['class'=>' chosen-select', 'onchange' => 'getMonthDays(this.value);']) .' on the 
                            </div>
                        </div>
                    </div>

                    <div class="row yearly-content hide margin-bottom-20">
                        <div class="offset-md-3 col-md-9">'
                             .Form::select('yearly_day_of_month',getDayOfMonth(date('n')), date('j'),['class'=>' inline-block chosen-select']) .' day of the month
                        </div>
                    </div>

                    <div class="row margin-bottom-20">
                        <div class="col-md-6">
                            <div class="row margin-bottom-20">'
                                 .Form::label('start','Create first invoice on', ['class' => 'control-label col-sm-6 padding-right-0']) .
                                '<div class="col-sm-6">
                                    <div class="has-feedback">'
                                     .Form::text('start',$schedule ? $schedule->start : date('Y-m-d'), ['class'=>'form-control datepicker', 'placeholder' => 'YYYY-MM-DD']) .'   
                                    <span class="glyphicon glyphicon-calendar form-control-feedback"></span>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="row margin-bottom-20">'
                                . Form::label('until','Until', ['class' => 'control-label col-sm-6 col-md-2 padding-right-0']) .'
                                <div class="col-sm-6">
                                    <div class="has-feedback">'
                                    . Form::text('until',null, ['class'=>'form-control datepicker', 'placeholder' => 'YYYY-MM-DD']) .'   
                                    <span class="glyphicon glyphicon-calendar form-control-feedback"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">'
                        . Form::label('timezone','Create in', ['class' => 'control-label col-md-3 text-right padding-right-0']) .'
                        <div class="col-md-9">'
                            . Form::select('timezone',getTimezones(), 'Asia/Dhaka', ['class'=>'form-control chosen-select']) .' time zone     <br>

                            <small class="text-muted">Set a time zone to ensure invoice delivery in the morning based on the recipient\'s time zone.</small>
                        </div>
                    </div>

                    <div class="row margin-top-20">'
                         .Form::label('trigger_time','Trigger Time', ['class' => 'control-label col-md-3 text-right padding-right-0']) .'
                        <div class="col-md-4">'
                            . Form::text('trigger_time',!empty($schedule->trigger_time) ? $schedule->getTriggerTime() : date('h:i A'),['class'=>'trigger-time form-control', 'data-date-format' => 'LT']) .'
                        </div>
                    </div>' 
                    
                    .Form::hidden('invoice_schedule_id', $schedule->id).
                Form::close() .
            
                '<div class="details-info invoice-schedule-info">
                    <strong>Repeat '. ucfirst($schedule->frequency) .':</strong> '. $schedule->getRepeatInfo() .'<br>
                    <strong>Dates:</strong>  Create first invoice on '. Carbon::parse($schedule->start)->format('F jS Y') .',and end '.($schedule->until ? Carbon::parse($schedule->until)->format('F jS Y') : 'never').'<br>
                    <strong>Time Zone:</strong> '. $schedule->timezone .'
                </div>';

            $panel_three_html = '';

            if(!$invoice->invoice_sent_option) {

            $panel_three_html = '<div class="panel-body">
                        <div class="row">
                            <div class="col-md-1">
                                <span class="step-no badge btn">3</span>
                            </div>
                            <div class="col-md-11">
                                <div class="row sent-option-header">
                                    <div class="col-md-4">
                                        <h3 class="text-muted margin-top-0">Send Option</h3>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="sent-option-button-area text-right"> 
                                            <a href="javascript:openAutomaticArea();" class="btn btn-default btn-automatic hide">Switch to Automatic Sending</a>
                                            <a href="javascript:openManualArea();" class="btn btn-default btn-manual hide">Switch to Manual Sending</a>
                                            <a href="javascript:saveSentOption();" class="btn btn-primary btn-save hide">Next</a>
                                        </div>
                                    </div>
                                </div>';
                        
            $panel_three_html .= '<!-- Sent Media Area -->
                                <div class="sent-media-area margin-top-20">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="sent-media" onclick="openAutomaticArea();">
                                                <i class="fa fa-paper-plane fa-3x"></i>
                                                <h4 class="bold margin-top-0">Send Automatically</h4>
                                                <span>Each invoice will be automatically emailed to your customer.</span>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="sent-media"  onclick="openManualArea();">
                                                <i class="fa fa-envelope fa-3x"></i>
                                                <h4 class="bold margin-top-0">Send Manually</h4>
                                                <span>I will manually send each invoice</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- /Sent Media Area -->
                                
                                <!-- Manual Send Area -->
                                <div class="manual-send-area bg-primary margin-top-20 padding-10 hide">
                                    <div class="row">
                                        <div class="col-sm-2"><i class="fa fa-envelope fa-3x"></i></div>
                                        <div class="col-sm-10">
                                            <h4 class="bold margin-top-0">Send Manually</h4>
                                            <span>I will manually send each invoice to my customer.</span>
                                        </div>
                                    </div>
                                </div>
                                <!-- Manual Send Area -->
                                
                                <!--  Automatic Send Area -->
                                <div class="automatic-send-area  margin-top-20 hide">
                                    <div class="bg-primary padding-10">
                                        <div class="row">
                                            <div class="col-sm-2"><i class="fa fa-paper-plane fa-3x"></i></div>
                                            <div class="col-sm-10">
                                                <h4 class="bold margin-top-0">Send Automatically</h4>
                                                <span>The invoice will be automatically emailed to your customer.</span>
                                            </div>
                                        </div>
                                    </div>'.

                                     Form::open(['url' => '', 'role' => 'form', 'id' => 'invoice-send-form', 'class' => 'form-horizontal margin-top-20']).' 
                                    
                                        <div class="form-group">
                                            <div class="col-sm-2 text-right bold">Send</div>
                                            <div class="col-sm-6">
                                                <strong>Invoice only</strong> <br>
                                                <small class="text-muted">Will be sent when the invoice is generated.</small>
                                            </div>
                                        </div>
                                        <div class="form-group">'.
                                             Form::label('from', 'Send From', ['class' => 'control-label col-sm-2 text-right']).'
                                            <div class="col-sm-6">'.
                                               Form::email('from', $invoice->owner->email, ['class' => 'form-control', 'placeholder' => 'Enter Sender Email']) .'
                                            </div>
                                        </div>

                                        <div class="form-group">'.
                                             Form::label('to', 'Send To', ['class' => 'control-label col-sm-2 text-right']).'
                                            <div class="col-sm-6">'.
                                               Form::email('to', $invoice->customer_profile->email, ['class' => 'form-control', 'placeholder' => 'Enter Recepient Email']) .' 
                                            </div>
                                            <div class="col-sm-4">
                                                <a class="btn btn-primary btn-xs" href="javascript:openCC();" title="Add CC"><i class="fa fa-plus-circle" aria-hidden="true"></i></a>
                                            </div>
                                        </div>

                                        <div class="form-group cc-row hide">'.
                                            Form::label('ccs', 'Send CC', ['class' => 'control-label col-sm-2 text-right']) .'
                                            <div class="col-sm-6">'.
                                               Form::text('ccs',null,['class'=>'form-control','placeholder'=>'Enter CC', 'data-role' => 'tagsinput']).'  
                                            </div>
                                        </div>

                                        <div class="form-group">'.
                                             Form::label('message', 'Custom Message', ['class' => 'control-label col-sm-2 text-right']) .'
                                            <div class="col-sm-6">'.
                                               Form::textarea('message', null, ['class' => 'form-control', 'placeholder' => 'Enter Message to your customer','size'=>'30x3']) .' 
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="offset-sm-2 col-sm-6">
                                                <div class="checkbox">
                                                    <label>'. Form::checkbox('is_attached',1,false) .' Attach PDF of the invoice to the email sent to the customer </label>
                                                </div>
                                                <div class="checkbox">
                                                    <label>'. Form::checkbox('copy_myself',1,false) .' Email a copy of each invoice to myself</label>
                                                </div>
                                            </div>
                                        </div>'.
                                         Form::hidden('sent_media', '') .
                                         Form::hidden('invoice_id', $invoice->id) .
                                        Form::hidden('invoice_sent_option_id', '') .  
                                    Form::close() .'
                                </div>
                                <!-- /Automatic Send Area --!>

                            </div>
                        </div>
                    </div>';
            }

            return response()->json([
                'type'  => 'success',
                'message'   => 'Invoice schedule has been '.$msg,
                'schedule_html'  => $schedule_html,
                'frequency' => $schedule->frequency,
                'panel_three_html'  => $panel_three_html,
            ]);
        }else {
            return response()->json([
                'type'  => 'error',
                'message'   => 'Invoice schedule has not been '.$msg,
            ]);
        }
    }

    /**
     * Store or update invoice sentr option
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function saveSentOption(Request $request)
    {
        $rules = [
            'invoice_id'    => 'required|integer',
            'sent_media' =>  'required|string|max:50|in:automatic,manual',
            'from'    => 'required_if:sent_media,automatic',
            'to'    => 'required_if:sent_media,automatic',
        ];

        $validator = Validator::make($request->all(), $rules);

        if($validator->fails()) {
            $messages = $validator->messages()->all();
            $validation_error = '';

            foreach($messages as $key=>$value)  {
                $validation_error .= $value .'<br/>';
            }

            return response()->json([
                'type'      => 'error',
                'message'   =>  $validation_error,
            ]);
        }else {
            // add schedule sent option
            $invoice = Invoice::find(trim($request->invoice_id));
            if(!$invoice) {
                return response()->json([
                    'type'      => 'error',
                    'message'   =>  'Invoice not found',
                ]);
            }

            if($invoice && $invoice->type !='recurring') {
                return response()->json([
                    'type'      => 'error',
                    'message'   =>  'This is not recurring invoice',
                ]);
            }

            $invoice_sent_option_exist = $invoice->invoice_sent_option;

            $invoice_sent_option = !$invoice_sent_option_exist ? new InvoiceSentOption : $invoice_sent_option_exist;
            $invoice_sent_option->invoice_id = $invoice->id;
            $invoice_sent_option->owner_id = $invoice->owner_id;
            $invoice_sent_option->customer_profile_id = $invoice->customer_profile_id;
            $invoice_sent_option->sent_media = trim($request->sent_media);

            if(trim($request->sent_media) == 'automatic') {
                $invoice_sent_option->from = $request->has('from') ? $request->from : $invoice->owner->email;
                $invoice_sent_option->to = $request->has('to') ? $request->to : $invoice->customer_profile->email;
                $invoice_sent_option->ccs = $request->filled('ccs') ? trim($request->ccs) : null;
                $invoice_sent_option->message = $request->filled('message') ? trim($request->message) : null;
                $invoice_sent_option->is_attached = $request->filled('is_attached') ? true : false;
                $invoice_sent_option->copy_myself = $request->filled('copy_myself') ? true : false;
            }else {
                $invoice_sent_option->from = null;
                $invoice_sent_option->to = null;
                $invoice_sent_option->ccs = null;
                $invoice_sent_option->message = null;
                $invoice_sent_option->is_attached = false;
                $invoice_sent_option->copy_myself = false;
            }

            if(!$invoice_sent_option_exist) {
                $msg = 'added.';
            }else {
                $msg = 'updated.';
            }

            if($invoice_sent_option->save()) {

                $panel_three_html = '<div class="panel-body">
                        <div class="row">
                            <div class="col-md-1">
                                <span class="step-no badge btn"><i class="fa fa-check" aria-hidden="true"></i></span>
                            </div>
                            <div class="col-md-11">
                                <div class="row sent-option-header">
                                    <div class="col-md-4">
                                        <h3 class="text-muted margin-top-0">Send Option</h3>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="sent-option-button-area text-right">
                                            <a href="javascript:cancelEditSentOption();" class="btn btn-danger btn-cancel hide">Cancel</a> 
                                            <a href="javascript:openAutomaticArea('.$invoice_sent_option->id.');" class="btn btn-default btn-automatic hide">Switch to Automatic Sending</a>
                                            <a href="javascript:openManualArea();" class="btn btn-default btn-manual hide">Switch to Manual Sending</a>
                                                <a href="javascript:saveSentOption();" class="btn btn-primary btn-save hide">Save</a>
                                            <a href="javascript:'.($invoice_sent_option->sent_media == 'manual' ? 'openManualArea('.$invoice_sent_option->id.')' : 'openAutomaticArea('.$invoice_sent_option->id.');').'" class="btn btn-default btn-edit"><i class="fa fa-edit"></i> Edit</a>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="sent-option-info margin-top-20">';

                        if($invoice_sent_option->sent_media == 'manual') {

                            $panel_three_html .= '<div class="manual-sending-info">
                                        <strong>Manual Sending:</strong> I will send each invoice to my customer.
                                    </div>';
                        }else if($invoice_sent_option->sent_media == 'automatic') {
                                        
                            $panel_three_html .= '<strong>Automatic Sending:</strong> Email the invoice automatically to '.($invoice_sent_option->getEmails()).' when the invoice is generated.';
                        }

                        $sent_media = $invoice_sent_option->sent_media;
                        $from = $invoice_sent_option->from;
                        $to = $invoice_sent_option->to;
                        $ccs = $invoice_sent_option->ccs;
                        $message = $invoice_sent_option->message;
                        $message = $invoice_sent_option->message;
                        $is_attached = $invoice_sent_option->is_attached;
                        $copy_myself = $invoice_sent_option->copy_myself;
                        
                        $panel_three_html .= '</div>
                                
                                <!-- Manual Send Area -->
                                <div class="manual-send-area bg-primary margin-top-20 well-sm hide">
                                    <div class="row">
                                        <div class="col-sm-2"><i class="fa fa-envelope fa-3x"></i></div>
                                        <div class="col-sm-10">
                                            <h4 class="bold margin-top-0">Send Manually</h4>
                                            <span>I will manually send each invoice to my customer.</span>
                                        </div>
                                    </div>
                                </div>
                                <!-- Manual Send Area -->
                                
                                <!--  Automatic Send Area -->
                                <div class="automatic-send-area  margin-top-20 hide">
                                    <div class="bg-primary well-sm">
                                        <div class="row">
                                            <div class="col-sm-2"><i class="fa fa-paper-plane fa-3x"></i></div>
                                            <div class="col-sm-10">
                                                <h4 class="bold margin-top-0">Send Automatically</h4>
                                                <span>The invoice will be automatically emailed to your customer.</span>
                                            </div>
                                        </div>
                                    </div>'.

                                     Form::open(['url' => '', 'role' => 'form', 'id' => 'invoice-send-form', 'class' => 'form-horizontal margin-top-20']).' 
                                    
                                        <div class="form-group">
                                            <div class="col-sm-2 text-right bold">Send</div>
                                            <div class="col-sm-6">
                                                <strong>Invoice only</strong> <br>
                                                <small class="text-muted">Will be sent when the invoice is generated.</small>
                                            </div>
                                        </div>
                                        <div class="form-group">'.
                                             Form::label('from', 'Send From', ['class' => 'control-label col-sm-2 text-right']).'
                                            <div class="col-sm-6">'.
                                               Form::email('from', $from, ['class' => 'form-control', 'placeholder' => 'Enter Sender Email']) .'
                                            </div>
                                        </div>

                                        <div class="form-group">'.
                                             Form::label('to', 'Send To', ['class' => 'control-label col-sm-2 text-right']).'
                                            <div class="col-sm-6">'.
                                               Form::email('to', $to, ['class' => 'form-control', 'placeholder' => 'Enter Recepient Email']) .' 
                                            </div>
                                            <div class="col-sm-4">
                                                <a class="btn btn-primary btn-xs" href="javascript:openCC();" title="Add CC"><i class="fa fa-plus-circle" aria-hidden="true"></i></a>
                                            </div>
                                        </div>

                                        <div class="form-group cc-row hide">'.
                                            Form::label('ccs', 'Send CC', ['class' => 'control-label col-sm-2 text-right']) .'
                                            <div class="col-sm-6">'.
                                               Form::text('ccs',$ccs,['class'=>'form-control','placeholder'=>'Enter CC', 'data-role' => 'tagsinput']).'  
                                            </div>
                                        </div>

                                        <div class="form-group">'.
                                             Form::label('message', 'Custom Message', ['class' => 'control-label col-sm-2 text-right']) .'
                                            <div class="col-sm-6">'.
                                               Form::textarea('message', $message, ['class' => 'form-control', 'placeholder' => 'Enter Message to your customer','size'=>'30x3']) .' 
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="offset-sm-2 col-sm-6">
                                                <div class="checkbox">
                                                    <label>'. Form::checkbox('is_attached',1,$is_attached) .' Attach PDF of the invoice to the email sent to the customer </label>
                                                </div>
                                                <div class="checkbox">
                                                    <label>'. Form::checkbox('copy_myself',1,$copy_myself) .' Email a copy of each invoice to myself</label>
                                                </div>
                                            </div>
                                        </div>'.
                                         Form::hidden('sent_media', $sent_media) .
                                         Form::hidden('invoice_id', $invoice->id) .
                                        Form::hidden('invoice_sent_option_id', $invoice_sent_option->id) .  
                                    Form::close() .'
                                </div>
                                <!-- /Automatic Send Area --!>
                            </div>
                        </div>
                    </div>';

                $panel_four_html = '';

                if($invoice->status != 'active') {
                    
                    $panel_four_html = '<div class="panel panel-primary panel-four">
                        <div class="panel-body text-center">
                            <h2>You\'re almost set!</h2>
                            <h3 class="text-muted">Review the details of your recurring invoice above and approve it when you\'re ready.</h3>
                            <h4 class="bold">Once approved, your first invoice will be created and sent on '. Carbon::parse($invoice_sent_option->start_date)->format('F dS, Y') .'</h4>
                            <a href="javascript:approveInvoice('. $invoice->id .');" class="btn btn-primary"><i class="fa fa-thumbs-up"></i> Approve and Start Recurring Invoice</a>
                        </div>
                    </div>';
                }

                return response()->json([
                    'type'  => 'success',
                    'message'  => 'Invoice sent option has been successfully '.$msg,
                    'panel_three_html'  => $panel_three_html,
                    'panel_four_html'  => $panel_four_html,
                ]);
            }else {
                return response()->json([
                    'type'  => 'error',
                    'message'  => 'Invoice sent option has not been '.$msg,
                ]);
            }
        }
    }

    /**
     * Fetch Invoice Send option info
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illiminate\Http\Response 
     */
    public function fetchInvoiceSendOption(Request $request, $id)
    {
        $invoice_sent_option = InvoiceSentOption::find($id);

        if(!$invoice_sent_option) {
            return response()->json([
                'type'  => 'error',
                'message'  => 'Invoice send option not found!',
            ]);
        }else {
            return response()->json($invoice_sent_option);
        }
    }

    /**
     * Approve invoice
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function approve(Request $request, $id)
    {
        $invoice = Invoice::find($id);

        if(!$invoice) {
            return response()->json([
                'type'      => 'error',
                'message'   => 'Invoice not found'
            ]);
        }

        if($invoice->type != 'recurring') {
            return response()->json([
                'type'      => 'error',
                'message'   => 'This is not recurring invoice'
            ]);
        }

        $invoice_sent_option = $invoice->invoice_sent_option;

        if(!$invoice_sent_option) {
            return response()->json([
                'type'      => 'error',
                'message'   => 'Invoice send option not found!',
            ]);
        }

        $invoice->status = 'active';
        $invoice->save();

        $invoice_schedule = $invoice->invoice_schedule;

        if($invoice_schedule->start == date('Y-m-d')) {
            InvoiceSender::send($invoice, 'manual');
        }

        $invoice_status = '<span class="label label-'. (config('constants.invoice_status_class.'.$invoice->status)).' invoice-status"> '. strtoupper($invoice->status).'</span>';
        $total_invoices = $invoice->recurring_invoice_logs()->count();

        if($total_invoices == 1) {
            $total_invoices = $total_invoices . ' Invoice';
        }else {
            $total_invoices = $total_invoices . ' Invoices';
        }

        return response()->json([
            'type'  => 'success',
            'message'   => 'You have activated a recurring invoice. The first invoice will be generated on '.Carbon::parse($invoice_sent_option->start)->format('F dS, Y').'.',
            'invoice_status' => $invoice_status,
            'total_invoices' => $total_invoices,
        ]);
    }

    /**
     * Make status to end
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function terminate(Request $request)
    {
        $invoice = Invoice::findOrFail($request->hdnResource);

        $invoice->status = 'end';

        if($invoice->save()) {
            $message = toastMessage('You have ended this recurring invoice. No more invoices will be generated.');
        }else {
            $message = toastMessage('Invoice status not updated', 'error');
        }

        session()->flash('toast', $message);

        return back();
    }
}
