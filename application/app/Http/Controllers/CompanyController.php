<?php

namespace App\Http\Controllers;

use App\Company;
use Illuminate\Http\Request;

use App\Country;
use Carbon, Validator;

class CompanyController extends Controller
{
    /**
     * For user access control
     */
    public function __construct()
    {
        $this->middleware('owner');
    }

    /**
     * Display a listing of the resource.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $companies = Company::where(function($query) use($request) {
            if(isOwner()) {
                $query->whereCreatedBy($request->user()->id);
            }
        })->latest('id')->paginate(10);
        $companies->paginationSummery = getPaginationSummery($companies->total(), $companies->perPage(), $companies->currentPage());

        return view('companies.index', compact('companies'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $countries = Country::getDropDownList();

        return view('companies.create', compact('countries'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'name' => 'required|max:255',
            'country' => 'required',
        ];

        $validator = Validator::make($request->all(),$rules);
        if($validator->fails()){

            return redirect()->back()->withInput($request->all())->withErrors($validator);
        }else{
            // insert or update
            $company = !$request->has('company_id') ? new Company : Company::findOrFail($request->company_id);
            $company->name = trim($request->name);
            $company->type_of_business = trim($request->type_of_business);
            $company->country = trim($request->country);
            $company->currency = trim($request->currency);
            $company->type_of_organization = trim($request->type_of_organization);
            $company->address = trim($request->address);
            $company->timezone = trim($request->timezone);
            $company->city = trim($request->city);
            $company->state = trim($request->state);
            $company->zip_code = trim($request->zip_code);
            $company->primary_phone = trim($request->primary_phone);
            $company->alt_phone = trim($request->alt_phone);
            $company->website = trim($request->website);
            $company->owner_id = isSuperAdmin() ? $request->owner_id : $request->user()->id;
            if(!$request->has('company_id')) {
                $company->created_by = $request->user()->id;
                $msg = 'added.';
            }else {
                $company->updated_by = $request->user()->id;
                $msg = 'updated.';
            }
            
            if($company->save()){

                // setup default settings
                if(!$request->has('company_id')) {
                    setupDefaultSettings($company->id);
                }
                
                $message = toastMessage('company has been '.$msg);
            }else{
                $message = toastMessage('company has not been '.$msg, 'error');
            }

            // redirect 
            $request->session()->flash('toast',$message);

            return redirect('companies');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function show(Company $company)
    {
        return view('companies.show', compact('company'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function edit(Company $company)
    {
        $countries = Country::getDropDownList();

        return view('companies.edit', compact('countries', 'company'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
        if(Company::destroy($request->hdnResource)){
            $message = toastMessage('Company has been successfully removed.');
        }else{
            $message = toastMessage('Company has not been removed.','error');
        }

        // Redirect
        $request->session()->flash('toast',$message);

        return back();
    }
}
