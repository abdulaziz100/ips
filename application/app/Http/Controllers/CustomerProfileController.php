<?php

namespace App\Http\Controllers;

use App\CustomerProfile;
use Illuminate\Http\Request;

use Helpers\Classes\EmailSender;
use App\Country;
use App\User;
use Carbon, Validator;

class CustomerProfileController extends Controller
{
    /**
     * For user access control
     */
    public function __construct()
    {
        $this->middleware('agent');
    }

    /**
     * Display a listing of the resource.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function customerProfileList(Request $request)
    {
        $customer_profiles = CustomerProfile::where(function($query) use($request) {
            if($request->has('search_item')) {
                $query->where('name', 'LIKE', '%' .trim($request->search_item) . '%')
                    ->orWhere('email', 'LIKE', '%' .trim($request->search_item) . '%')
                    ->orWhere('primary_phone', 'LIKE', '%'.trim($request->search_item). '%');
            }
            if(!isSuperAdmin()) {
                $query->whereCreatedBy($request->user()->id);
            }
        })->latest('id')->paginate(10);
        $customer_profiles->paginationSummery = getPaginationSummery($customer_profiles->total(), $customer_profiles->perPage(), $customer_profiles->currentPage());

        if($request->has('search_item')) {
            $customer_profiles->appends(['search_item' => trim($request->search_item)]);
        }

        return view('customer_profiles.index', compact('customer_profiles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $countries = Country::getDropDownList();

        return view('customer_profiles.create', compact('countries'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'name' => 'required|max:255',
            'primary_phone' => 'required|max:255',
            'country'   => 'required'
        ];

        if($request->filled('email')) {

            $email_rule = 'string|email|max:100';

        }

        if($request->has('enable_web_access')) {
                
            $email_rule = 'string|email|max:100|unique:users,email';
            
            if($request->has('customer_profile_id')) {
                $customerProfile = CustomerProfile::find($request->customer_profile_id);

                if($user = $customerProfile->user) {
                    $email_rule .= ','. $user->id;
                }
            }
        }

        if(!empty($email_rule)) {
            $rules = $rules + [
                'email'    => $email_rule,
            ];
        }

        if($request->has('enable_web_access')) {
            $rules = $rules + [
                'password' => 'required|string|min:6',
            ];
        }

        $validator = Validator::make($request->all(),$rules);
        if($validator->fails()){

            return redirect()->back()->withInput($request->all())->withErrors($validator);

        }else{
            // insert or update
            $customer_profile = !$request->has('customer_profile_id') ? new CustomerProfile : CustomerProfile::findOrFail($request->customer_profile_id);
            $customer_profile->name = trim($request->name);
            $customer_profile->email = trim($request->email);
            $customer_profile->primary_phone = trim($request->primary_phone);
            $customer_profile->alt_phone = trim($request->alt_phone);
            $customer_profile->contact_person_name = trim($request->contact_person_name);
            $customer_profile->contact_person_email = trim($request->contact_person_email);
            $customer_profile->customer_since = date('Y-m-d');
            $customer_profile->currency = trim($request->currency);
            $customer_profile->city = trim($request->city);
            $customer_profile->state = trim($request->state);
            $customer_profile->area = trim($request->area);
            $customer_profile->zip_code = trim($request->zip_code);
            $customer_profile->address = trim($request->address);
            $customer_profile->country = trim($request->country);

            if(!$request->has('customer_profile_id')) {
                $customer_profile->created_by = $request->user()->id;
                $msg = 'added.';
            }else {
                $customer_profile->updated_by = $request->user()->id;
                $msg = 'updated.';
            }
            
            if($customer_profile->save()){

                // Check whether enable web access button is checked. If checked then add new user
                if($request->has('enable_web_access') && $request->has('email')) {
                    // inert user data
                    $username = array_first(explode('@', trim($request->email)));
                    $user_exists = User::whereUsername($username)->first();
                    if($user_exists) {
                        $username = $username . '_' . str_random(6);
                    }
                    $user = new User;
                    $user->name = trim($request->name);
                    $user->username = $username;
                    $user->email = trim($request->email);
                    $user->password = bcrypt($request->password);
                    $user->user_type = 'customer';
                    
                    $user->active = 0;
                    if($user->save()) {
                        
                        //encode    
                        $iat = Carbon::now()->timestamp;
                        $exp = $iat+3600;  
                        $token = [
                            "resource" => $user->id,
                            "iss" => env('APP_HOST'),
                            "iat" => $iat,
                            "exp" => $exp
                        ];

                        $jwt = \Firebase\JWT\JWT::encode($token, env('JWT_SECRET_KEY'));

                        $data = [
                            'blade' => 'new_account',
                            'body'  =>  [
                                'name' => trim($request->name),
                                'username'  => $username,
                                'password'  => trim($request->password),
                                'jwt'  => $jwt,
                            ],
                            'toUser'    =>  trim($request->email),
                            'toUserName'    =>  trim($request->name),
                            'subject'   =>  env('APP_NAME') . ' New Account Confirmation!',
                        ];

                        EmailSender::send($data);
                    }
                }

                $message = toastMessage('Customer has been '.$msg);
            }else{
                $message = toastMessage('Customer has not been '.$msg, 'error');
            }

            // redirect 
            $request->session()->flash('toast',$message);

            return redirect('customer-profiles/list');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CustomerProfile  $customerProfile
     * @return \Illuminate\Http\Response
     */
    public function edit(CustomerProfile $customerProfile)
    {
        $countries = Country::getDropDownList();

        return view('customer_profiles.edit', compact('countries', 'customerProfile'));
    }

    /**
    * Change user access
    *
    * @param  \Illuminate\Http\Request  $request
    * @param int $id
    * @return \Illuminate\Http\Response
    */
    public function changeActive(Request  $request)
    {
        $customer_profile = CustomerProfile::find($request->hdnResource);

        if(!$customer_profile) {
            session()->flash('toast', toastMessage('Customer profile not found', 'error'));

            return back();
        }

        if($customer_profile->active) {
            $customer_profile->active = 0;
            $msg = 'inactive';
        }else {
            $customer_profile->active = 1;
            $msg = 'active';
        }

        if($customer_profile->save()) {
            $message = toastMessage(' Customer status has become '.$msg);
        }else {
            $message = toastMessage(' Customer status has not become '.$msg, 'error');
        }

        $request->session()->flash('toast', $message);
        
        return back();
    }

    /**
     * Display customer profile related invoice info
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function getDeleteConfirmation(Request $request, $id)
    {
        $customer_profile = CustomerProfile::findOrFail($id);

        $invoices = $customer_profile->invoices;

        return view('customer_profiles.delete_confirm', compact('customer_profile', 'invoices'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
        if(CustomerProfile::destroy($request->hdnResource)){
            $message = toastMessage('Customer Profile has been successfully removed.');
        }else{
            $message = toastMessage('Customer Profile has not been removed.','error');
        }

        // Redirect
        $request->session()->flash('toast',$message);

        return redirect('customer-profiles/list');
    }
}
