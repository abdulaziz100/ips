<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Category;
use Validator;

class CategoryController extends Controller
{
    /**
     * For user access control
     */
    public function __construct()
    {
        $this->middleware('agent');
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function categoryList(Request $request)
    {
        $categories = Category::where(function($query) use($request) {
            if(!isSuperAdmin()) {
                $query->whereCreatedBy($request->user()->id);
            }
            if($request->has('title')) {
                $query->where('title', 'LIKE', '%'. trim($request->title). '%');
            }
        })->latest('id')->paginate(10);
        
        if($request->has('title')) {
            $categories->appends(['title' => trim($request->title)]);
        }
        $categories->paginationSummery = getPaginationSummery($categories->total(), $categories->perPage(), $categories->currentPage());
        $parent_categories = Category::getCategoryList();

        return view('categories.index', compact('categories', 'parent_categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'title' => 'required|max:255',
        ];

        $validator = Validator::make($request->all(),$rules);
        if($validator->fails()){

            return redirect()->back()->withInput($request->all())->withErrors($validator);
        }else{
            // insert or update
            $category = !$request->has('category_id') ? new Category : Category::findOrFail($request->category_id);
            $category->title = trim($request->title);
            $category->slug = str_slug($request->title);
            $category->description = trim($request->description);
            $category->parent_id = $request->parent_id ? trim($request->parent_id) : 0;
            if(!$request->has('category_id')) {
                $category->created_by = $request->user()->id;
                $msg = 'added.';
            }else {
                $category->updated_by = $request->user()->id;
                $msg = 'updated.';
            }
            
            if($category->save()){
                $category->slug = $category->id . '-'.str_slug($category->title);
                $category->save();
                $message = toastMessage('Category has been '.$msg);
            }else{
                $message = toastMessage('Category has not been '.$msg, 'error');
            }

            // redirect 
            $request->session()->flash('toast',$message);

            return redirect('categories/list');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $category = Category::findOrFail($id);
        $categories = Category::where(function($query) use($request) {
            if(!isSuperAdmin()) {
                $query->whereCreatedBy($request->user()->id);
            }
            if($request->has('title')) {
                $query->where('title', 'LIKE', '%'. trim($request->title). '%');
            }
        })->latest('id')->paginate(10);
        
        if($request->has('title')) {
            $categories->appends(['title' => trim($request->title)]);
        }
        $categories->paginationSummery = getPaginationSummery($categories->total(), $categories->perPage(), $categories->currentPage());
        
        $parent_categories = Category::getCategoryList(true, $id);

        return view('categories.index', compact('category', 'categories', 'parent_categories'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
        if(Category::destroy($request->hdnResource)){
            $message = toastMessage('Category has been successfully removed.');
        }else{
            $message = toastMessage('Category has not been removed.','error');
        }

        // Redirect
        $request->session()->flash('toast',$message);

        return redirect('categories/list');
    }
}
