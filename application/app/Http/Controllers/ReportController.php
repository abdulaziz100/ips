<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\CustomerProfile;
use App\Invoice;
use App\RecurringInvoice;
use App\Service;
use DB, PDF;

class ReportController extends Controller
{
    /**
     * Display a report of invoice
     *
     * @param \Illuminate\Http\Request $request
     * @param string 
     * @return \Illuminate\Http\Response
     */
    public function invoice(Request $request, $action=null)
    {
    	$query = Invoice::query(); 

		if($request->has('date_range')) {
            $date_range  = explode(' - ',$request->date_range);
            $from_date = \Carbon::parse($date_range[0])->format('Y-m-d');
            $to_date = \Carbon::parse($date_range[1])->format('Y-m-d');
            $query->whereRaw('invoice_date >= "'.$from_date.'" AND invoice_date <="'.$to_date.'"');
    	}else {
    		$from_date = date('Y-m-d', strtotime('-7 days'));
    		$to_date = date('Y-m-d');
    		$query->whereRaw('invoice_date >= "'.date('Y-m-d', strtotime('-7 days')).'" AND invoice_date <="'.date('Y-m-d').'"');
    	}

    	if(isOwner() || isAgent()) {

    		$query->whereOwnerId($request->user()->id);

    	}else if(isCustomer()) {
            
    		$query->whereCustomerProfileId($request->user()->id);
    	}

    	$invoices = $query->whereType('normal')->latest('id')->get();

    	if($action == 'print') {

    		return view('prints.report_invoice', compact('invoices', 'from_date', 'to_date'));

    	}else if($action == 'pdf') {

    		$file_name = 'invoice-report-'.date('Y-m-d').'.pdf';

    		return PDF::loadView('pdf.report_invoice', compact('invoices', 'from_date', 'to_date'))->stream($file_name);
    	}

    	return view('reports.invoice', compact('invoices', 'from_date', 'to_date'));
    }

    /**
     * Display a report of income
     *
     * @param \Illuminate\Http\Request $request
     * @param string 
     * @return \Illuminate\Http\Response
     */
  	public function income(Request $request, $action=null)
    {
    	$query = Invoice::query(); 

		if($request->has('date_range')) {
            $date_range  = explode(' - ',$request->date_range);
            $from_date = \Carbon::parse($date_range[0])->format('Y-m-d');
            $to_date = \Carbon::parse($date_range[1])->format('Y-m-d');
            $query->whereRaw('invoice_date >= "'.$from_date.'" AND invoice_date <="'.$to_date.'"');
    	}else {
    		$from_date = date('Y-m-d', strtotime('-7 days'));
    		$to_date = date('Y-m-d');
    		$query->whereRaw('invoice_date >= "'.date('Y-m-d', strtotime('-7 days')).'" AND invoice_date <="'.date('Y-m-d').'"');
    	}

    	if(isOwner() || isAgent()) {
    		$query->whereOwnerId($request->user()->id);
    	}else if(isCustomer()) {
    		$query->whereCustomerProfileId($request->user()->id);
    	}

    	if($request->filled('customer_profile_id')) {
			$query->whereCustomerProfileId($request->customer_profile_id);
		}

    	$invoices = $query->whereType('normal')->whereIn('status', ['partial', 'paid'])->latest('id')->get();

    	if($action == 'print') {

    		return view('prints.report_income', compact('invoices', 'from_date', 'to_date'));

    	}else if($action == 'pdf') {

    		$file_name = 'income-report-'.date('Y-m-d').'.pdf';

    		return PDF::loadView('pdf.report_income', compact('invoices', 'from_date', 'to_date'))->stream($file_name);
    	}

    	$customer_profiles = CustomerProfile::getDropDownList();

    	return view('reports.income', compact('invoices', 'from_date', 'to_date', 'customer_profiles'));
    }

    /**
     * Display a report of customer wise income
     *
     * @param \Illuminate\Http\Request $request
     * @param string 
     * @return \Illuminate\Http\Response
     */
  	public function customerwiseIncome(Request $request, $action=null)
    {
    	$query = DB::table('invoices as i')
    		->join('payments as p', 'i.id', '=', 'p.invoice_id')
    		->join('customer_profiles as cp','i.customer_profile_id', '=', 'cp.id'); 

		if($request->has('date_range')) {
            $date_range  = explode(' - ',$request->date_range);
            $from_date = \Carbon::parse($date_range[0])->format('Y-m-d');
            $to_date = \Carbon::parse($date_range[1])->format('Y-m-d');
            $query->whereRaw('i.invoice_date >= "'.$from_date.'" AND i.invoice_date <="'.$to_date.'"');
    	}else {
    		$from_date = date('Y-m-d', strtotime('-7 days'));
    		$to_date = date('Y-m-d');
    		$query->whereRaw('i.invoice_date >= "'.date('Y-m-d', strtotime('-7 days')).'" AND i.invoice_date <="'.date('Y-m-d').'"');
    	}

    	if(isOwner() || isAgent()) {
    		$query->where('i.owner_id', $request->user()->id);
    	}else if(isCustomer()) {
    		$query->where('i.customer_profile_id', $request->user()->id);
    	}

    	if($request->filled('customer_profile_id')) {
			$query->where('i.customer_profile_id', $request->customer_profile_id);
		}

    	$invoices = $query->where('i.type', 'normal')
			->select('cp.id','cp.name','cp.email','cp.created_at', 
				DB::raw('count(i.id) as total_invoice'),
				DB::raw('SUM(i.total) as total_amount'),
				DB::raw('SUM(p.amount) as total_paid_amount')
			)->groupBy('cp.id')->latest('cp.id')->get();

    	if($action == 'print') {

    		return view('prints.report_customerwise_income', compact('invoices', 'from_date', 'to_date'));

    	}else if($action == 'pdf') {

    		$file_name = 'customerwise-income-report-'.date('Y-m-d').'.pdf';

    		return PDF::loadView('pdf.report_customerwise_income', compact('invoices', 'from_date', 'to_date'))->stream($file_name);
    	}

    	$customer_profiles = CustomerProfile::getDropDownList();

    	return view('reports.customerwise_income', compact('invoices', 'from_date', 'to_date', 'customer_profiles'));
    }

    /**
     * Display report of polular services
     *
     * @param \Illuminate\Http\Request $request
     * @param string $action
     * @return \Illuminate\Http\Response
     */
    public function popularServices(Request $request, $action=null)
    {
        $services = DB::table('invoices as i')
            ->join('invoice_details as id', 'i.id', '=', 'id.invoice_id')
            ->join('services as s', 'id.service_id', '=', 's.id')
            ->select(
                's.id',
                's.title', 
                's.promotional_price',
                DB::raw('COUNT(id.service_id) as total_products')
            )
            ->where('i.type', 'recurring')
            ->where(function($query) use($request) {
                if(!isSuperAdmin()) {
                    $query->whereOwnerId($request->user()->id);
                }
            })
            ->groupBy('id.service_id')
            ->orderBy('total_products', 'DESC')
            ->get();

        if($action == 'print') {

            return view('prints.report_popular_services', compact('services'));

        }else if($action == 'pdf') {

            $file_name = 'popular-services-report-'.date('Y-m-d').'.pdf';

            return PDF::loadView('pdf.report_popular_services', compact('services'))->stream($file_name);
        }

        return view('reports.popular_services', compact('services'));
    }
}
