<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Invoice;
use PDF;

class InvoiceDetailsController extends Controller
{
    /**
     * Display invoice information
     *
     * @param \Illuminate\Http\Request $request
     * @param string $identifier
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request, $identifier)
    {
    	$invoice = Invoice::whereIdentifier($identifier)->first();

    	if(!$invoice) {

    		session()->flash('toast', toastMessage('Invoice not found', 'error'));

    		return redirect('/');
    	}
    	
        $invoice_details = $invoice->invoice_details;
        $payments = $invoice->payments;
        $company = $invoice->getCompanyOwner()->companies()->first();

        if($request->has('view') && $request->view == 'print') {
            
            return view('prints.invoice', compact('invoice', 'invoice_details', 'payments', 'company'));

        }else if($request->has('view') && $request->view == 'pdf') {
            
            return \PDF::loadView('pdf.invoice', compact('invoice', 'invoice_details', 'payments', 'company'))->download('invoice_'.$invoice->identifier.'_'.date('Y').'_'.date('m').'_'.date('d').'.pdf');
        }

        return view('preview.invoice', compact('invoice', 'invoice_details', 'payments', 'company'));
    }	
}
