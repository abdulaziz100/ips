<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Invoice;

class InvoiceReminderController extends Controller
{
    /**
     * Display invoice reminder info
     *
     * @param \Illuminate\Http\Request $request
     * @param string $identifier
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request, $identifier)
    {
    	$invoice = Invoice::whereIdentifier($identifier)->first();

    	if(!$invoice) {

    		session()->flash('toast', toastMessage('Invoice not found', 'error'));

    		return redirect('/');
    	}
    	
        $owner = $invoice->owner;
		$companyOwner = $invoice->getCompanyOwner();
		$company = $companyOwner->companies()->first();

		return view('preview.invoice_reminder', compact('invoice', 'owner','companyOwner', 'company'));
    }
}
