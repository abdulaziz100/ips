<?php

namespace App\Http\Controllers;

use App\Service;
use Illuminate\Http\Request;

use App\Category;
use Validator;

class ServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = Service::query();

        if(!isSuperAdmin()) {
            $query->whereCreatedBy($request->user()->id);
        }
        $services = $query->latest('id')->paginate(10);
        $services->paginationSummery = getPaginationSummery($services->total(), $services->perPage(), $services->currentPage());

        return view('services.index', compact('services'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::getCategoryList();

        return view('services.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'title' => 'required|max:255',
            'category_id' => 'required|integer',
        ];

        $validator = Validator::make($request->all(),$rules);
        if($validator->fails()){

            return redirect()->back()->withInput($request->all())->withErrors($validator);
        }else{
            // insert or update
            $service = !$request->has('service_id') ? new Service : Service::findOrFail($request->service_id);
            $service->title = trim($request->title);
            $service->description = trim($request->description);
            $service->category_id = trim($request->category_id);
            $service->regular_price = trim($request->regular_price);
            $service->promotional_price = trim($request->promotional_price);
            if(!$request->has('service_id')) {
                $service->created_by = $request->user()->id;
                $msg = 'added.';
            }else {
                $service->updated_by = $request->user()->id;
                $msg = 'updated.';
            }
            
            if($service->save()){
                $message = toastMessage('Service has been '.$msg);
            }else{
                $message = toastMessage('Service has not been '.$msg, 'error');
            }

            // redirect 
            $request->session()->flash('toast',$message);

            return redirect('services');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function edit(Service $service)
    {
        $categories = Category::getCategoryList();

        return view('services.edit', compact('service', 'categories'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
        if(Service::destroy($request->hdnResource)){
            $message = toastMessage('Service has been successfully removed.');
        }else{
            $message = toastMessage('Service has not been removed.','error');
        }

        // Redirect
        $request->session()->flash('toast',$message);

        return back();
    }
}
