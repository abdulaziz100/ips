<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InvoiceSentOption extends Model
{
    public function getEmails()
    {
    	$emails = $this->to;

    	if($this->ccs) {
    		$emails .= ','. $this->ccs;
    	}

    	return $emails;
    }
}
