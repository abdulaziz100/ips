<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    use SoftDeletes;

	protected $dates = ['deleted_at'];

	public function parent()
    {
        return $this->belongsTo('App\Category', 'parent_id', 'id');
    }

    public function children()
    {
        return $this->hasMany('App\Category', 'parent_id', 'id');
    }

    public static function getCategoryList($prepend = true, $except_id = null)
    {
        $query = Category::query();

        if(!isSuperAdmin()) {
            $query->whereCreatedBy(\Auth::id());
        }

        $categories = $query->orderBy('title')->pluck('title', 'id');

        if($prepend) {
            $categories->prepend('Select a category', '');
        }

        if($except_id) {
            $categories = $categories->except($except_id);
        }

        return $categories->all();
    }
}
