<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use Carbon;

class Invoice extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];


    /**
     * Define an inverse one-to-one or many relationship.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function customer_profile()
    {
    	return $this->belongsTo(CustomerProfile::class);
    }

    public function owner()
    {
        return $this->belongsTo('App\User', 'owner_id');
    }

    /**
     * Define a one-to-one relationship.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function invoice_schedule()
    {
        return $this->hasOne(InvoiceSchedule::class);
    } 

    public function invoice_sent_option()
    {
        return $this->hasOne(InvoiceSentOption::class);
    }

    public function recurring_invoice_log()
    {
        return $this->hasOne('App\RecurringInvoiceLog','generated_invoice_id');
    }  

    /**
     * Define a one-to-many relationship.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function invoice_details()
    {
    	return $this->hasMany(InvoiceDetail::class);
    }

    public function payments()
    {
        return $this->hasMany(Payment::class);
    }

    public function recurring_invoice_logs()
    {
        return $this->hasMany('App\RecurringInvoiceLog', 'recurring_invoice_id');
    }

    public function mail_logs()
    {
        return $this->hasMany(MailLog::class);
    }

    protected static function boot()
    {
    	parent::boot();

    	static::deleting(function($invoice) {
            $invoice->invoice_details()->delete();
            $invoice->payments()->delete();
            $invoice->recurring_invoice_logs()->delete();
    		$invoice->mail_logs()->delete();
            $invoice->invoice_schedule()->delete();
            $invoice->invoice_sent_option()->delete();
            $invoice->recurring_invoice_log()->delete();
    	});
    }

    /**
     * Get Unique Indetifier
     */
    public static function getIdentifier()
    {
        $current_datetime = strtotime(date('Y-m-d h:i:s'));
        $identifier = substr(md5($current_datetime), 0, 6);

        $invoice = Invoice::whereIdentifier($identifier)->first(['identifier']);

        if($invoice) {
            Self::getIdentifier();
        }else {
            $identifier = $identifier;
        }

        return $identifier;
    }

    /**
     * Get Invoice Owner
     */
    public function getCompanyOwner()
    {
        $owner = $this->owner;
        
        if($owner->owner) {
            $owner = $owner->owner;
        }

        return $owner;
    }

    /**
     * check current date is remindable day
     *
     * @param int $date_no
     * @return boolean 
     */
    public function isRemindableDay($day_no)
    {
        $is_remindable_day = false;

        $payment_due = Carbon::parse($this->payment_due);
        $day_difference = Carbon::now()->diffInDays($payment_due, false);

        if($day_difference >= 0) {
            if($day_no <= 0) {
                $day_no = abs($day_no);

                if($day_difference > $day_no) {
                    $is_remindable_day = true;
                }
            }
        }else {
            $day_difference = abs($day_difference);
            if($day_no) {
                if($day_no > $day_difference) {
                    $is_remindable_day = true;
                }
            }
        }
        
        return $is_remindable_day;
    }

    /**
     * Show amount due of invoice
     */
    public function getAmountDue()
    {
        $total_amount = $this->total;

        $total_payment = $this->payments()->sum('amount');

        return round(($total_amount - $total_payment), 2);
    }

    /**
     * Get payment due
     */
    public function getDayDiff()
    {
        $invoice_date = Carbon::parse($this->invoice_date. ' 00:00:00');
        $payment_due = Carbon::parse($this->payment_due. ' 00:00:00');

        return $invoice_date->diffInDays($payment_due);
    }

    /**
     * Get Invoice Status
     */
    public function getStatus($with_label_class=true) 
    {  
        $status = $this->status;

        if(in_array($status, ['unsent', 'sent', 'viewed', 'partial']) && (Carbon::now()->diffInDays(Carbon::parse($this->payment_due), false) < 0)) {

            $status = 'overdue';

            $status_with_label_class = '<span class="label label-danger invoice-status">'.strtoupper($status).'</span>';
        }else {
            
            $status_with_label_class = '<span class="label label-'. config('constants.invoice_status_class.'.$status).' invoice-status"> '. strtoupper($status) .'</span>';
        }

        if($with_label_class) {
            return $status_with_label_class;
        }

        return $status;
    }

    /**
     * Get Schedule info
     */
    public function getScheduleInfo()
    {
        $schedule_info = '–';

        if($invoice_schedule = $this->invoice_schedule) {

            $frequency = $invoice_schedule->frequency;
            $start_date = $invoice_schedule->start;
            $end_date = $invoice_schedule->until ? $invoice_schedule->until : 'Never';

            if($frequency == 'daily') {
                
                $schedule_info = '<span>Repeat daily</span><br/>';

            }else if($frequency == 'weekly') {
                
                $schedule_info = '<span>Repeat weekly every '.config('constants.day_of_week.'.$invoice_schedule->day_of_week).'</span><br/>';

            }else if($frequency == 'monthly') {

                $schedule_info = '<span>Repeat monthly on the '.getDayOfMonth(1, $invoice_schedule->day_of_month).'</span><br/>';

            }else if($frequency == 'yearly') {

                $schedule_info = '<span>Repeat yearly on '.getMonth($invoice_schedule->month).' '.getDayOfMonth(1, $invoice_schedule->day_of_month).'</span><br/>';
            }

            $schedule_info .= '<small class="text-muted">First Invoice: '.$start_date.', Ends:'.$end_date.'</small>';
        }

        return $schedule_info;
    }

    /**
     * Get Previous invoice date
     */
    public function getPreviousInvoiceDate()
    {
        $previous_invoice_date = '–';
        $recurring_invoice_log = $this->recurring_invoice_logs()->latest()->first() ;

        if($recurring_invoice_log) {
            $previous_invoice_date = $recurring_invoice_log->generated_date;
        }
        
        return $previous_invoice_date;
    }

    /**
     * Get next invoice date
     */
    public function getNextInvoiceDate()
    {
        $next_invoice_date = '–';
        
        if($this->status == 'active') {

            $previous_invoice_date = $this->getPreviousInvoiceDate();

            $invoice_schedule = $this->invoice_schedule;
            $startDate = Carbon::parse($invoice_schedule->start);
            $untilDate = Carbon::parse($invoice_schedule->until);
            $toDate = Carbon::now(); 

            $year = $toDate->year;
            $month = $toDate->month;
            $day = $toDate->day;
            $dayOfWeek = $toDate->dayOfWeek;           

            if($toDate->diffInDays($startDate, false) >=0) {
                $next_invoice_date = $invoice_schedule->start;
            }else {
                
                $frequency = $invoice_schedule->frequency;

                if($previous_invoice_date != '–') {
                    $previous_invoice_date_obj = Carbon::parse($previous_invoice_date);
                }

                $day_of_month = $invoice_schedule->day_of_month;
                $day_of_week = $invoice_schedule->day_of_week;
                $month_of_year = $invoice_schedule->month;

                if($frequency == 'daily') {

                    if(!empty($previous_invoice_date_obj)) {
                        if($previous_invoice_date_obj->diffInDays($toDate, false) > 0) {
                            $next_possible_invoice_date = $toDate;
                        }else {
                            $next_possible_invoice_date = $previous_invoice_date_obj->addDay();
                        }
                    }else {
                        $next_possible_invoice_date = $toDate;
                    } 

                }else if($frequency == 'weekly') {
                    
                    if($dayOfWeek ==  $day_of_week) {
                        $next_possible_invoice_date = $toDate;
                    }else {
                        $week_day_name = config('constants.day_of_week.'.$day_of_week);
                        $next_possible_invoice_date = $toDate->modify("next $week_day_name");
                    }

                }else if($frequency == 'monthly') {

                    if($day == $day_of_month) {
                        $next_possible_invoice_date = $toDate;
                    }else {
                        $next_possible_invoice_date = $toDate->modify("next month")->startOfMonth()->addDays($day_of_month - 1);
                    }

                }else if($frequency == 'yearly') {

                    if(($month == $month_of_year) && ($day == $day_of_month)) {
                        
                        $next_possible_invoice_date = $toDate;
                    }else {
                        $next_possible_invoice_date = $toDate->modify('next year')->startOfYear()->addMonths($month_of_year - 1)->addDays($day_of_month - 1);
                    }
                    
                }
                
                $isValidDate = false;

                if($invoice_schedule->until) {
                    $isValidDate = $next_possible_invoice_date->between($startDate, $untilDate);
                }else {
                    $isValidDate = $startDate->diffInDays($next_possible_invoice_date, false) >= 0 ? true:false;
                }

                if($isValidDate) {
                    $next_invoice_date = $next_possible_invoice_date->format('Y-m-d');
                }

            }
        }

        return $next_invoice_date;
    }
}
