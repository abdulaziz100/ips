<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Service extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    /**
     * Define an inverse one-to-one or many relationship.
	 *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
    	return $this->belongsTo('App\Category');
    }

    /**
     * Display a dropdown list of services
     *
     * @return array 
     */
    public static function getDropDownList($prepend=true)
    {
        $services = Service::whereCreatedBy(\Auth::id())->orderBy('title')->pluck('title', 'id');

        if($prepend) {
            $services->prepend('Select a service', '');
        }

        return $services->all();
    }
}
