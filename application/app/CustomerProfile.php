<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use Auth;

class CustomerProfile extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    public function invoices()
    {
        return $this->hasMany(Invoice::class);
    }

    public function country_obj()
    {
    	return $this->belongsTo('App\Country', 'country', 'country_code');
    }

    public function user()
    {
    	return $this->belongsTo('App\User', 'email', 'email');
    }

    protected static function boot()
    {
    	parent::boot();

    	static::deleting(function($customerProfile) {
            $invoices = $customerProfile->invoices;
            if($invoices->isNotEmpty()) {
                foreach($invoices as $invoice) {
                    $invoice->invoice_details()->forceDelete();
                    $invoice->payments()->delete();
                    $invoice->recurring_invoice_logs()->delete();
                    $invoice->mail_logs()->delete();
                    $invoice->invoice_schedule()->delete();
                    $invoice->invoice_sent_option()->delete();
                    $invoice->recurring_invoice_log()->delete();
                }
            }
            $customerProfile->invoices()->forceDelete();
            $customerProfile->user()->delete();
    	});
    }

    /**
     * Display a dropdown list of customers
     *
     * @return array 
     */
    public static function getDropDownList($prepend=true)
    {
        $query = CustomerProfile::query();

        if(!isSuperAdmin()) {
            $query->whereCreatedBy(Auth::id());
        }

        $customers = $query->pluck('name', 'id');

        if($prepend) {
            $customers->prepend('Select a customer', '');
        }

        return $customers->all();
    }
}
