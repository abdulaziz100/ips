<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    /**
     * Define an inverse one-to-one or many relationship.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function invoice()
    {
    	return $this->belongsTo(Invoice::class);
    }

    public function customer_profile()
    {
    	return $this->belongsTo(CustomerProfile::class);
    }
}
