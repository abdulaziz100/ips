<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InvoiceSchedule extends Model
{
	/**
     * Define an inverse one-to-one or many relationship.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function invoice()
    {
    	return $this->belongsTo(Invoice::class);
    }

    public function getRepeatInfo()
    {
    	$message = '';
    	$frequency = $this->frequency;
    	$day_of_month = $this->day_of_month;

    	if($frequency == 'daily') {
    		$message = 'Every day of the week';
    	}else if($frequency == 'weekly') {
    		$message = 'Every '.config('constants.day_of_week.'.$this->day_of_week);
    	}else if($frequency == 'monthly') {
            // $message = 'On the '.Carbon::now()->startOfMonth()->addDays($day_of_month)->format('jS').' day of each month';
    		$message = 'On the '.getDayOfMonth(1,$day_of_month).' day of each month';
    	}else if($frequency == 'yearly') {
    		$message = 'Every '.getMonth($this->month).' on the '.getDayOfMonth(1,$day_of_month).' day of the month';
    	}

    	return $message;
    }

    public function getTriggerTime($hour=null, $minute=null, $second=null, $format = 'h:i A')
    {
        if(!$hour && !$minute && !$second) {
            $time_arr = explode(':', $this->trigger_time);
            $hour   = $time_arr[0];
            $minute = $time_arr[1];
            $second = $time_arr[2];
        }
        
        return \Carbon::createFromTime($hour, $minute, $second, $this->timezone)->format($format);
    }
}
