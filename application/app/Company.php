<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use Auth;

class Company extends Model
{
	use SoftDeletes;

	protected $dates = ['deleted_at'];
	
    public function country_obj()
    {
    	return $this->belongsTo('App\Country', 'country', 'country_code');
    }

    /**
     * Get company owner
     */
    public static function getOwner()
    {
    	if(!Auth::check()) {
			return false;
		}

		$company_owner = isOwner() ? Auth::user() : Auth::user()->owner;

		return $company_owner;
    }
}
