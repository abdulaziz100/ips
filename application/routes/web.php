<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('login');
});

Auth::routes();

Route::any('login', 'Auth\LoginController@login');
Route::get('logout', 'Auth\LoginController@logout');

Route::get('/home', 'HomeController@index')->name('home');

Route::get('account/active/{token}', 'Auth\AccountActivationController@active');

Route::middleware('auth')->group(function() {

	// Category Routes...
	Route::match(['GET','POST'],'categories/list', 'CategoryController@categoryList');
	Route::post('categories/delete', 'CategoryController@delete');
	Route::resource('categories', 'CategoryController');

	// Service Routes...
	Route::post('services/delete', 'ServiceController@delete');
	Route::resource('services', 'ServiceController');

	// Customer Routes...
	Route::post('customer-profiles/change-active', 'CustomerProfileController@changeActive');
	Route::get('customer-profiles/delete-confirm/{id}', 'CustomerProfileController@getDeleteConfirmation');
	Route::post('customer-profiles/delete', 'CustomerProfileController@delete');
	Route::any('customer-profiles/list', 'CustomerProfileController@customerProfileList');
	Route::resource('customer-profiles', 'CustomerProfileController');

	// Company Routes...
	Route::post('companies/delete', 'CompanyController@delete');
	Route::resource('companies', 'CompanyController');

	// Subscription Routes...
	Route::resource('subscriptions', 'SubscriptionController');

	// Invoice Routes...
	Route::any('invoices/list', 'InvoiceController@invoiceList');
	Route::post('invoices/fetch-service/{service_id}', 'InvoiceController@fetchService');
	Route::post('invoices/save-customer', 'InvoiceController@saveCustomer');
	Route::post('invoices/delete', 'InvoiceController@delete');
	Route::post('invoices/approve/{id}', 'InvoiceController@approveInvoice');
	Route::post('invoices/skip-sending-invoice/{id}', 'InvoiceController@skipSendingInvoice');
	Route::post('invoices/marked-as-sent/{id}', 'InvoiceController@markedAsSent');
	Route::any('invoices/email-preview/{id}', 'InvoiceController@showInvoiceEmailPreview');
	Route::post('invoices/fetch-invoice/{id}','InvoiceController@fetchInvoice');
	Route::post('invoices/send-email/{id}', 'InvoiceController@sendEmail');
	Route::post('invoices/record-payment/{id}', 'InvoiceController@recordPayment');
	Route::post('invoices/fetch-payment/{id}', 'InvoiceController@fetchPayment');
	Route::post('invoices/remove-payment/{id}', 'InvoiceController@removePayment');
	Route::get('invoices/payment-receipt/{id}', 'InvoiceController@showPaymentReceipt');
	Route::post('invoices/send-receipt', 'InvoiceController@sendReceipt');
	Route::post('invoices/send-reminder', 'InvoiceController@sendReminder');
	Route::resource('invoices', 'InvoiceController');

	//Recurring Invoice Routes...
	Route::any('recurring-invoices/list', 'RecurringInvoiceController@invoiceList');
	Route::post('recurring-invoices/delete', 'RecurringInvoiceController@delete');
	Route::get('recurring-invoices/fetch-month-days/{month_no}', 'RecurringInvoiceController@getMonthDays');
	Route::post('recurring-invoices/set-schedule/{invoice_id}', 'RecurringInvoiceController@setSchedule');
	Route::post('recurring-invoices/save-sent-option', 'RecurringInvoiceController@saveSentOption');
	Route::post('recurring-invoices/fetch-invoice-sent-option/{id}', 'RecurringInvoiceController@fetchInvoiceSendOption');
	Route::post('recurring-invoices/approve/{id}', 'RecurringInvoiceController@approve');
	Route::post('recurring-invoices/terminate', 'RecurringInvoiceController@terminate');
	Route::resource('recurring-invoices', 'RecurringInvoiceController');

	// Report Routes...
	Route::any('reports/invoice/{action?}', 'ReportController@invoice');
	Route::any('reports/income/{action?}', 'ReportController@income');
	Route::any('reports/customerwise-income/{action?}', 'ReportController@customerwiseIncome');
	Route::any('reports/popular-services/{action?}', 'ReportController@popularServices');

	// Settings
	Route::any('settings', 'OptionController@index');
});

// invioce details route for frontend
Route::get('invoice-details/{indentifier}', 'InvoiceDetailsController');
// payment receipt route for frontend
Route::get('payment-receipt/{encrypted_id}', 'PaymentReceiptController');
// Invoice reminder route
Route::get('invoice-reminder-preview/{identifier}', 'InvoiceReminderController');

// user routes...
Route::any('owners', 'UserController@getOwnerList');
Route::any('profile', 'UserController@profile');
Route::post('users/change-active', 'UserController@changeActive');
Route::post('users/delete', 'UserController@delete');
Route::any('agents', 'UserController@getAgentList');
Route::get('create-agent', 'UserController@createAgent');
Route::post('store-agent', 'UserController@storeAgent');
Route::get('users/reset-password/{username}', 'UserController@getResetPassword');

// Run invoice send command
Route::get('/invoice-send', function() {
	\Artisan::call('invoice:send');
});

Route::get('/update-app', function() {
	\Artisan::call('config:clear');
	\Artisan::call('config:cache');
});

Route::get('invoice-reminder', function() {
	
	$invoice = \App\Invoice::findOrFail(1);
	$owner = $invoice->owner;
	$companyOwner = $invoice->getCompanyOwner();
	$company = $companyOwner->companies()->first();

	return view('preview.invoice_reminder', compact('invoice', 'owner','companyOwner', 'company'));

});

Route::get('update-users', function() {
	
	// $users = \DB::table('users')->update([
	// 	'password'	=> bcrypt('123456&*')
	// ]);

	// if($users) {
	// 	dd('Users updated');
	// }

});

